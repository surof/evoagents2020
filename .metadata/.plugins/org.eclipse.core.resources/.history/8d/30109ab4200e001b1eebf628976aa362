/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import org.encog.engine.network.activation.ActivationLinear;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.neat.NEATPopulation;
import org.encog.neural.neat.PersistNEATPopulation;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.neural.pattern.FeedForwardPattern;
import org.encog.persist.EncogDirectoryPersistence;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.modules.NeuralNetworkModule;
import evoagentmindelements.modules.SkillModule;

public class EATSingleSim2dTransfer extends EATaskDemoSimSingleBot2D {
	protected String refSkill = "";
	protected boolean recordTrainingSet = true;
	BasicMLDataSet trainSet = new BasicMLDataSet();	
	int numberOfNetworks = 10;
	ArrayList<BasicNetwork> transferedNetwork;
	NeuralNetworkModule transferedModule;
	SkillModule refModule;
	int hiddenLayerCount = 2;
	
	HashMap<String,Double> actuatorValues = null;
	HashMap<String,Double> sensorValues = null;
	
	boolean isMLP = true;
	ArrayList<ResilientPropagation> bProp = null;
	
	
	public EATSingleSim2dTransfer(String root, String botmodel, String botname, String masterS, String driveN, String learnS, String envirName)
	{
		super(root,botmodel, botname, masterS, driveN,envirName);
		refSkill = learnS;
		refModule = mind.getSkill(refSkill);
	}

	protected void initTask() {
		if(isMLP)
		{
			transferedNetwork = new ArrayList<>();
			for(int i = 0 ; i < numberOfNetworks; i++)
				transferedNetwork.add(makeBasicNetwork(mindTemplate.getSkillInputCount(refSkill),mindTemplate.getSkillOutputCount(refSkill),hiddenLayerCount,2));
			transferedModule = new NeuralNetworkModule(mind, refSkill,refModule.getSkillInputs(),refModule.getSkillOutputs(),null,transferedNetwork.get(0));
			transferedModule.setMaster(refModule.isMaster);
		}	
		else
		{
			//NEATPopulation neatPop = makeNEATPop();// stuff
		}
	}
	
	protected void preRepetition() {
		if(recordTrainingSet)
			throttleDelay = 0;
		else
		{
//			throttleDelay = 4;
			throttleDelay = 0;			
		}
	}
	
	protected void simStep() {
		if(recordTrainingSet)
		{
			sensorValues = environment.doStep(actuatorValues, scoreCounter, interruptFlag);
			actuatorValues = mind.doStep(sensorValues);
			trainSet.add(refModule.copyInputMLData(),refModule.copyOutputMLData());
		}
		else
		{
			actuatorValues = mind.doStep(environment.doStep(actuatorValues, scoreCounter, interruptFlag));
		}
	}
	
	protected void viewerStep() {
		if(!recordTrainingSet)
			simPanel.repaint();
	}

	protected void postRepetition() {
		if(recordTrainingSet)
		{	
			if(isMLP)
			{
				
				if(bProp == null)
				{
					bProp = new ArrayList<>();

					for(int i = 0 ; i < numberOfNetworks; i++)
						bProp.add(new ResilientPropagation(transferedNetwork.get(i), trainSet));
				}
				else
					for(int i = 0 ; i < numberOfNetworks; i++)
						bProp.get(i).setTraining(trainSet);
					
				//bProp = new ResilientPropagation(transferedNetwork, trainSet);
				System.out.println(trainSet.size());
					
				for(int i = 0 ; i< 150 ; i++)
				{
					for(int j = 0 ; j < numberOfNetworks; j++)
						bProp.get(j).iteration();
					
				}
				for(int j = 0 ; j < numberOfNetworks; j++)
					bProp.get(j).finishTraining();
				//trainSet = new BasicMLDataSet();
				mind.swapSkill(transferedModule);
				scoreCounter.reset();
			}
			else
			{
				/*
				//neat
				NEATGAFactory fac = new NEATGAFactory();
				MLTrain train = fac.create(neatPop, trainSet, null);
				for(int i = 0 ; i< 20 ; i++)
					train.iteration();
				transferedModule.setNetwork((MLRegression) train.getMethod());
				mind.swapSkill(transferedModule);
				scoreCounter.reset();
				*/
			}
		}
		else
		{
			if(isMLP)
			{
				//save ann
				File f = new File(AgentRootFolder+"/"+EvoAgentAppDefines.skillFolder+"/"+refSkill+"/"+refSkill+"_TRANSFER_"+scoreCounter.getCurrentScore()+EvoAgentAppDefines.neuralNetworkFileExtension);
				EncogDirectoryPersistence.saveObject(f, transferedNetwork);
				//trainSet = new BasicMLDataSet();
				mind.swapSkill(refModule);
				scoreCounter.reset();
			}
			else
			{
				/*
 				//save neat
				File f = new File(AgentRootFolder+"/"+EvoAgentAppDefines.skillFolder+"/"+refSkill+"/"+refSkill+"_TRANSFER_NEAT_"+scoreCounter.getCurrentScore()+EvoAgentAppDefines.neuralNetworkFileExtension);
				EncogDirectoryPersistence.saveObject(f, transferedModule.getNetwork());
				//trainSet = new BasicMLDataSet();
				mind.swapSkill(refModule);
				scoreCounter.reset();
				 */
			}
		}
		recordTrainingSet = !recordTrainingSet;
	}

	private BasicNetwork makeBasicNetwork(int inSize, int outSize, int numLayers, int addedHiddenNeurons)
	{
		FeedForwardPattern pattern = new FeedForwardPattern();
		BasicNetwork network;
		pattern.setInputNeurons(inSize);
		for(int i = 0 ; i < numLayers ; i++)
			pattern.addHiddenLayer(Math.max(inSize, outSize)+addedHiddenNeurons);
		pattern.setOutputNeurons(outSize);
		pattern.setActivationFunction(new ActivationSigmoid());
		pattern.setActivationOutput(new ActivationLinear());
		network = (BasicNetwork)pattern.generate();
		network.reset();
		return network;
	}
	
	@SuppressWarnings("unused")
	private NEATPopulation makeNEATPop()
	{
		int populationSize = 200;
		NEATPopulation pop= null;
		Path path = Paths.get(AgentRootFolder+"/"+EvoAgentAppDefines.skillFolder+"/"+refSkill+"/"+refSkill+"_TRANSFER_NEAT_TRAINPERSIST"+EvoAgentAppDefines.trainingPersistenceFileExtension);
		if (Files.exists(path)) {
			System.out.println("resume NEAT training from file");			
			try {
				byte[] data = Files.readAllBytes(path);
				pop = (NEATPopulation)new PersistNEATPopulation().read(new ByteArrayInputStream(data));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(pop == null)
		{
			System.out.println("start NEAT training from scratch");			
			pop = new NEATPopulation(mindTemplate.getSkillInputCount(refSkill),mindTemplate.getSkillOutputCount(refSkill), populationSize);
			pop.reset();
		}
		return pop;
	}
	
	/*

	private String environmentName = "";
	private SimulationEnvironment2D environment;
	EvoAgentMind mind;
	protected String refSkill = "";
	BasicNetwork transferedNetwork;
	NeuralNetworkModule transferedModule;
	SkillModule refModule;
	
	int hiddenLayerCount = 2;
	
	public EATSingleSim2dTransfer(String root, String botmodel, String botname, String masterS, String learnS, String envirName)
	{
		super(root,botmodel, botname, masterS);
		environmentName = envirName;
		mindTemplate = EvoAgentMindFactory.loadMindTemplate(MindRootFolder,botname, masterSkill);
		mind = EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
		
		refSkill = learnS;
	}
	
	public void runTask() 
	{
		BasicMLDataSet trainSet = new BasicMLDataSet();
		refModule = mind.getSkill(refSkill);
		transferedNetwork = makeBasicNetwork(mindTemplate.getSkillInputCount(refSkill),mindTemplate.getSkillOutputCount(refSkill),hiddenLayerCount,2);
		((MLResettable)transferedNetwork).reset();
		transferedModule = new NeuralNetworkModule(mind, 
				refSkill, 
				mind.getSkill(refSkill).getSensorInput(),
				mind.getSkill(refSkill).getDerivativeInput(),
				mind.getSkill(refSkill).getVariableInput(),
				mind.getSkill(refSkill).getVariableDerivativeInput(),
				mind.getSkill(refSkill).getMotorOutput(),
				mind.getSkill(refSkill).getSkillOutput(),
				mind.getSkill(refSkill).getVariableOutput(),
				2,
				transferedNetwork);
		
		transferedModule.setMaster(refModule.isMaster);
		
		NEATPopulation neatPop = makeNEATPop();
		
		ResilientPropagation bProp = null;
		boolean flip = false;
		long timePrev = 0;
		float throttleDelay = 4;
//		float throttleDelay = 0;
		
		makeEnvironement();
		environment.setBotModel(botModel);
		environment.init();
		
		SimulationInterruptFlag interruptFlag = new SimulationInterruptFlag();
		ScoreCounter scoreCounter = new ScoreCounter();
		ArrayList<ArrayList<evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D.ObstaclePos>> obstaclesData = new ArrayList<>();
		HashMap<String,Double> actuatorValues = null;
		HashMap<String,Double> sensorValues = null;
		SimEnv2DViewer simPanel = new SimEnv2DViewer(environment,scoreCounter); 
		JFrame frame = new JFrame(environmentName);
		frame.add(simPanel);
		frame.setSize(800, 600);
		if(environment.getHasObstacles())
		{
			environment.generateAllObstaclesParameters(obstaclesData, 1);
			environment.loadObstacles(obstaclesData.get(0));
		}
		frame.setVisible(true);
		environment.preRunOps();
		while(frame.isVisible())
		{
			interruptFlag.resetState();
			environment.reset();
			mind.reset();
			environment.preRepetitionOps();
			//mind.printSkillList();
			while(!interruptFlag.interrupted() && frame.isVisible())
			{
				if (flip)
				{
					if(false)
					{
						if(System.currentTimeMillis()-timePrev>throttleDelay)
						{
							environment.preStepOps();
							environment.doWorldStep();
							environment.preMindOps();
							actuatorValues = mind.doStep(((SimulationEnvironment2DSingleBot)environment).doStep(actuatorValues, scoreCounter, interruptFlag));
							simPanel.repaint();
							environment.postStepOps();		
							timePrev = System.currentTimeMillis();			
						}
						else
						{
							try {
								Thread.sleep(5);
							} catch (InterruptedException e1) {
								
								e1.printStackTrace();
							}					
						}															
					}
					else
					{
						environment.preStepOps();
						environment.doWorldStep();
						environment.preMindOps();
						actuatorValues = mind.doStep(((SimulationEnvironment2DSingleBot)environment).doStep(actuatorValues, scoreCounter, interruptFlag));
						simPanel.repaint();
						environment.postStepOps();		
					}
				}
				else
				{
					environment.preStepOps();
					environment.doWorldStep();
					environment.preMindOps();
					sensorValues = ((SimulationEnvironment2DSingleBot)environment).doStep(actuatorValues, scoreCounter, interruptFlag);
					actuatorValues = mind.doStep(sensorValues);
					trainSet.add(refModule.copyInputMLData(),refModule.copyOutputMLData());
					environment.postStepOps();		
				}
			}
			if(flip)
			{
				//save neat

				File f = new File(AgentRootFolder+"/"+EvoAgentAppDefines.skillFolder+"/"+refSkill+"/"+refSkill+"_TRANSFER_NEAT_"+scoreCounter.getCurrentScore()+EvoAgentAppDefines.neuralNetworkFileExtension);
				EncogDirectoryPersistence.saveObject(f, transferedModule.getNetwork());
				//trainSet = new BasicMLDataSet();
				mind.swapSkill(refModule);
				scoreCounter.reset();
				
				//save ann
				/*	
				File f = new File(AgentRootFolder+"/"+EvoAgentAppDefines.skillFolder+"/"+refSkill+"/"+refSkill+"_TRANSFER_"+scoreCounter.getCurrentScore()+EvoAgentAppDefines.neuralNetworkFileExtension);
				EncogDirectoryPersistence.saveObject(f, transferedNetwork);
				//trainSet = new BasicMLDataSet();
				mind.swapSkill(refModule);
				scoreCounter.reset();
				
			}
			else
			{
				//neat
				NEATGAFactory fac = new NEATGAFactory();
				MLTrain train = fac.create(neatPop, trainSet, null);
				for(int i = 0 ; i< 20 ; i++)
					train.iteration();
				transferedModule.setNetwork((MLRegression) train.getMethod());
				mind.swapSkill(transferedModule);
				scoreCounter.reset();
				
				
				//train ann
				/*
				if(bProp == null)
					 bProp = new ResilientPropagation(transferedNetwork, trainSet);
				else
					bProp.setTraining(trainSet);
				for(int i = 0 ; i< 150 ; i++)
					bProp.iteration();
				bProp.finishTraining();
				mind.swapSkill(transferedModule);
				scoreCounter.reset();
				///
			}
			
			flip = !flip;
			environment.postRepetitionOps(scoreCounter,interruptFlag);
		}
		environment.postRunOps(scoreCounter,interruptFlag);
		System.out.println("Demo ended");
		//Encog.getInstance().shutdown();
	}
		
	private boolean makeEnvironement()
	{
			Class<?> clazz;
			try {
				System.out.println("making env : evoagent2dsimulator.experiments."+environmentName);
				clazz = Class.forName("evoagent2dsimulator.experiments."+environmentName);
				Constructor<?> constructor = clazz.getConstructor();
				environment = (SimulationEnvironment2D) constructor.newInstance();
				System.out.println("created environment : "+ environmentName);
			} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				System.out.println("environement class not found : "+ environmentName);
				System.out.println(e.getMessage());
				environment = null;
				return false;
			}
			return true;
	}


	private BasicNetwork makeBasicNetwork(int inSize, int outSize, int numLayers, int addedHiddenNeurons)
	{
		FeedForwardPattern pattern = new FeedForwardPattern();
		BasicNetwork network;
		pattern.setInputNeurons(inSize);
		for(int i = 0 ; i < numLayers ; i++)
			pattern.addHiddenLayer(Math.max(inSize, outSize)+addedHiddenNeurons);
		pattern.setOutputNeurons(outSize);
		pattern.setActivationFunction(new ActivationSigmoid());
		pattern.setActivationOutput(new ActivationRamp());
		network = (BasicNetwork)pattern.generate();
		network.reset();
		return network;
	}
	
	private NEATPopulation makeNEATPop()
	{
		int populationSize = 200;
		NEATPopulation pop= null;
		Path path = Paths.get(AgentRootFolder+"/"+EvoAgentAppDefines.skillFolder+"/"+refSkill+"/"+refSkill+"_TRANSFER_NEAT_TRAINPERSIST"+EvoAgentAppDefines.trainingPersistenceFileExtension);
		if (Files.exists(path)) {
			System.out.println("resume NEAT training from file");			
			try {
				byte[] data = Files.readAllBytes(path);
				pop = (NEATPopulation)new PersistNEATPopulation().read(new ByteArrayInputStream(data));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(pop == null)
		{
			System.out.println("start NEAT training from scratch");			
			pop = new NEATPopulation(mindTemplate.getSkillInputCount(refSkill),mindTemplate.getSkillOutputCount(refSkill), populationSize);
			pop.reset();
		}
		return pop;
	}
	*/
}