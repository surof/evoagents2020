/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.multiagentexperiments;

import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectDetector;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLZoneActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PointListenerDistance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;
import evoagentsimulation.evoagent2dsimulator.worldElements.WorldElement;

public class EXP_Forage extends SimulationEnvironment2DMultiBot{

	RW_ManualReward manualReward;
	private TriggerZone dz ;
	private ArrayList<WorldElement> targetObjectList ;
	private int objectGenerateCount = 8;
	private float objectSize = 1.0f;
	private float objectSpread = 40.0f;
	private int botCount = 4;
	private int formationInitX = 0;
	private int formationInitY = 0;
	private int WORLD_SIZE = 250;

	private double botPerceptionDistance = 60;
	
	//Reward Values

	int tickLimit = 10000000;
	
	public EXP_Forage() {
		super();
		this.name = "forage";
		hasObstacles = true;
		makeCircleWorld = false;
		minObstacleSize = 3.2;
	    maxObstacleSizeVariability = 10.0;
	    maxObstacleSpacingVariability = 15.0;
	    obstacleSpacing = 55.0;
	    
	    setSquareBorders(WORLD_SIZE);
	    
		getCorners()[0] = new Vec2(-WORLD_SIZE , WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
	}
	
	@Override
	public void init() 
	{
		super.init();
		
		manualReward = new RW_ManualReward(null, 1);
		rewardFunctions.add(manualReward);
		//controlFunctions.add(new CF_NextOnTimeout(null,this, 80000));
		startAngle = new ArrayList<ArrayList<Double>>();
		startAngle.add(new ArrayList<Double>());
		startPos = new ArrayList<ArrayList<Vec2>>();
		startPos.add(new ArrayList<Vec2>());
		for(int i = 0; i < botCount; i++)
		{
			startAngle.get(0).add((double) 0);
			startPos.get(0).add(new Vec2(formationInitX + (i * 10), formationInitY + (i * 10)));			
		}
				
		bots = new ArrayList<ArrayList<BotBody2D>>();
		bots.add(makeBots(botModel.get(0), botCount, SimulationEnvironment2DMultiBot.TEAM_1_LABEL, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		makeBotMinds(mindTemplates);
		
		//dz = new TriggerZone(new Vec2(-150,-150), 0,10);
		dz = new TriggerZone(new Vec2(0,0), 0,10);
		getWorldElements().add(dz);
		
		targetObjectList = new ArrayList<WorldElement>();
		
		for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				((S_ConstantSensor)bots.get(i).get(j).sensors.get("ID")).setValue(((double)(j))/((double)(bots.get(i).size() - 1)));
				((S_ZonePresence)bots.get(i).get(j).sensors.get("SENSDZ")).setTarget(dz);
				((S_PLZoneActive)bots.get(i).get(j).sensors.get("ACTDZ")).setTarget(dz);
				((S_ObjectDetector)bots.get(i).get(j).sensors.get("SENSOBJ")).setTargetList(targetObjectList);
				((S_PLObjectListActive)bots.get(i).get(j).sensors.get("ACTOBJA")).setList(targetObjectList);
				((S_PLObjectListActive)bots.get(i).get(j).sensors.get("ACTOBJA")).setDistance(botPerceptionDistance);

				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearFriendlyActive")).setIDToTrack(i);
				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearFriendlyActive")).setDistance(200);
				((S_PointListenerDistance)bots.get(i).get(j).sensors.get("NearFriendlyDistance")).setDistance(200);
			}
		}
		
		/*
		for(int i=0;i<bots.get(0).size();i++)
		{
			controlFunctions.add(new CF_NextOnTimeout(bots.get(0).get(i),this, tickLimit));
			rewardFunctions.add(new RW_Speed(bots.get(0).get(i),speedReward));
			rewardFunctions.add(new RW_BelowDistanceFromGroup(bots.get(0).get(i),nearAllyGroup,bots.get(0),maxDistFromGroup));
			for(int j=0;j<bots.size();j++)
			{
				if(i!=j)
				rewardFunctions.add(new RW_BelowDistanceFromTarget( bots.get(0).get(i), avoidingAllies, bots.get(0).get(j), minDistFromAllies));
			}
			rewardFunctions.add(new RW_ClosingOnTarget(bots.get(0).get(i),goingToFlag,flags.get(0)));
			rewardFunctions.add(new RW_PunishOnCollision(bots.get(0).get(i),collisionPenalty));
		}*/
		makeWorld();
		registerBotsToWorld();
		generateObjects();
	}
	protected Vec2 generatePositionInBoundaries(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[0].y - margin - 150f))+150f
				);
	}
	/*
	@Override
	protected Vec2 generatePositionInBoundaries(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[0].y - margin - 150f))+150f
				);
	}
	*/
	protected Vec2 generateLocalPosition(Vec2 ref, float spread) {
		return new Vec2(
				(float)((Math.random() * spread * 2) + ref.x - spread),
				(float)((Math.random() * spread * 2) + ref.y - spread)
				);
	}
	
	private void generateObjects() {	
		float relax = 0.0f;
		TargetObject to;
		Vec2 posObj;
		Vec2 pos = new Vec2(generatePositionInBoundaries(20.0f));
		while(!checkElementPositionConficts(pos,objectSize) || !checkObstaclePositionConficts(pos,objectSize+8.0f))
			pos.set(generatePositionInBoundaries(20.0f));
		for(int i = 0; i < objectGenerateCount; i++)
		{
			posObj = new Vec2(generateLocalPosition(pos,objectSpread));
			while(!checkElementPositionConficts(posObj,objectSize+8.0f) || !checkObstaclePositionConficts(posObj,objectSize+18.0f) || !checkInBoundaries(posObj,objectSize+18.0f))
			{
				posObj = new Vec2(generateLocalPosition(pos,objectSpread+relax));
				relax += 1.0f;
				//System.out.println("gen " + relax);
			}
			to = new TargetObject(posObj, 1.0f, objectSize);
			getWorldElements().add(to);
			targetObjectList.add(to);	
			to.registerToWorld(getWorld());
		}		
	}

	@Override
	public void postStepOps() {
		super.postStepOps();
		int i = 0;
		while(i < targetObjectList.size())
		{
			if(dz.isPointInZone(targetObjectList.get(i).getWorldPosition()) && !((TargetObject)targetObjectList.get(i)).lock)
			{
				targetObjectList.get(i).removeFromWorld();
				getWorldElements().remove(targetObjectList.get(i));
				targetObjectList.remove(i);
				//rew lad
				manualReward.addReward();
			}
			else
				i++;
		}
		if(targetObjectList.size() <= 0)
			generateObjects();
	}
	
	@Override
	public void reset()
	{
		super.reset();
		clearTargetObjectList();
		for(RewardFunction r: rewardFunctions)
			r.reset();
		generateObjects();
	}
	
	public void clearTargetObjectList()
	{
		for(WorldElement tObj : targetObjectList)
		{
			tObj.removeFromWorld();
			getWorldElements().remove(tObj);
		}
		targetObjectList.clear();
	}
}
