/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.multiagentexperiments;

import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBotCTF;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.bot.WarBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentGroupActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLVirtualActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLZoneActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Distance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_FlagCarry;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_FlagState;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_TeamScores;
import evoagentsimulation.evoagent2dsimulator.worldElements.Flag;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

public class EW_CTF  extends SimulationEnvironment2DMultiBotCTF {


	RW_TeamScores rewardTeamScore;
	RW_ManualReward rewardManual;

	//Reward Values
	double killReward = 0.001;
	double killFlagCarry = 0.10;
	double FlagCaptureReward = 15.0;
	double FlagCapturedReward = -10.0;
	double FlagReturned = 8.0;
	double FlagCarryTick = 0.01;

	int tickLimit = 100000;
	float prevDist = -1;
	
	public EW_CTF()
	{
		super();
		this.name = "CTF";
		hasObstacles = true;
		getCorners()[0] = new Vec2(-100.0f, 100.0f);
		getCorners()[1] = new Vec2(100.0f, 100.0f);
		getCorners()[2] = new Vec2(100.0f, -100.0f);
		getCorners()[3] = new Vec2(-100.0f, -100.0f);
		
		obstacleSpacing = 50.0;
		minObstacleSize = 2;
		maxObstacleSizeVariability = 10;
		maxObstacleSpacingVariability = 10;
	}
	
	@Override
	public void init() 
	{
		// set starting angle for each team (0 - 1)d
		startAngle = new ArrayList<ArrayList<Double>>();
		startAngle.add(new ArrayList<Double>());
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);

		startAngle.add(new ArrayList<Double>());
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		
		// set starting positions for each team (0 - 1)
		startPos = new ArrayList<ArrayList<Vec2>>();
		startPos.add(new ArrayList<Vec2>());
		startPos.get(0).add(new Vec2(80,-80));
		startPos.get(0).add(new Vec2(80,-85));
		startPos.get(0).add(new Vec2(80,-90));
		startPos.get(0).add(new Vec2(90,-80));
		startPos.get(0).add(new Vec2(90,-85));
		startPos.get(0).add(new Vec2(90,-90));
		startPos.get(0).add(new Vec2(70,-80));
		startPos.get(0).add(new Vec2(70,-85));
		startPos.get(0).add(new Vec2(70,-90));

		startPos.add(new ArrayList<Vec2>());
		startPos.get(1).add(new Vec2(-80,80));
		startPos.get(1).add(new Vec2(-80,85));
		startPos.get(1).add(new Vec2(-80,90));
		startPos.get(1).add(new Vec2(-90,80));
		startPos.get(1).add(new Vec2(-90,85));
		startPos.get(1).add(new Vec2(-90,90));
		startPos.get(1).add(new Vec2(-70,80));
		startPos.get(1).add(new Vec2(-70,85));
		startPos.get(1).add(new Vec2(-70,90));

		// create the bots for each team
		bots = new ArrayList<ArrayList<BotBody2D>>();
		bots.add(makeBots(botModel.get(0), 6, SimulationEnvironment2DMultiBot.TEAM_1_LABEL, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		bots.add(makeBots(botModel.get(1), 6, SimulationEnvironment2DMultiBot.TEAM_2_LABEL, SimulationEnvironment2DMultiBot.TEAM_2_ID));
		// create an individual MIND instance for each bot, using the team template
		makeBotMinds(mindTemplates);

		// obectives
		teamBasesPositions.add(new Vec2(70,70));
		teamBasesPositions.add(new Vec2(-70,-70));
		
		flags.add(new Flag(teamBasesPositions.get(0), 0, 5, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		flags.add(new Flag(teamBasesPositions.get(1), 0, 5, SimulationEnvironment2DMultiBot.TEAM_2_ID));
		
		teamZones.add(new TriggerZone(teamBasesPositions.get(0), 0, 10,SimulationEnvironment2DMultiBot.TEAM_1_LABEL));
		teamZones.add(new TriggerZone(teamBasesPositions.get(1), 0, 10,SimulationEnvironment2DMultiBot.TEAM_2_LABEL));
		
		// add objective to renderer
		for(Flag f : flags)
			worldElements.add(f);
		for(TriggerZone t : teamZones)
			worldElements.add(t);
		
		rewardManual = new RW_ManualReward(null, FlagReturned);

		// set objectives and targets for each team
		for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				//System.out.println(teamZones.get(i).name);
				try {
					((S_ConstantSensor)bots.get(i).get(j).sensors.get("ID")).setValue(((double)(j))/((double)(bots.get(i).size() - 1)));
					((S_PLZoneActive)bots.get(i).get(j).sensors.get("FBaseActive")).setTarget(teamZones.get(i));
					((S_PLVirtualActive)bots.get(i).get(j).sensors.get("FFlagActive")).setTarget(flags.get(i));
					((S_FlagState)bots.get(i).get(j).sensors.get("FFlagState")).setTarget(flags.get(i));
					((S_FlagCarry)bots.get(i).get(j).sensors.get("FFlagCarry")).setFlag(flags.get(i));
					((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearFriendlyActive")).setIDToTrack(i);
					((S_PLAgentGroupActive)bots.get(i).get(j).sensors.get("NearFriendlyGroupActive")).setIDToTrack(i);

					((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearEnemyActive")).setIDToTrack(Math.abs(-1 + i));
					((S_PLAgentGroupActive)bots.get(i).get(j).sensors.get("NearEnemyGroupActive")).setIDToTrack(Math.abs(-1 + i));
					((S_PLZoneActive)bots.get(i).get(j).sensors.get("EBaseActive")).setTarget(teamZones.get(Math.abs(-1 + i)));
					((S_PLVirtualActive)bots.get(i).get(j).sensors.get("EFlagActive")).setTarget(flags.get(Math.abs(-1 + i)));	
					((S_FlagState)bots.get(i).get(j).sensors.get("EFlagState")).setTarget(flags.get(Math.abs(-1 + i)));			
					((S_FlagCarry)bots.get(i).get(j).sensors.get("EFlagCarry")).setFlag(flags.get(Math.abs(-1 + i)));	
					if(i == SimulationEnvironment2DMultiBot.TEAM_1_ID)
						((S_FlagCarry)bots.get(i).get(j).sensors.get("FlagCarry")).setRewardFunction(rewardManual);					
				}
				catch (Exception e) {
					System.out.println("sensor not found");
				}
			}
		}
		// init score 
		for(int i = 0 ; i < bots.size() ; i++)
			teamScores.add(0);
		
		rewardTeamScore = new RW_TeamScores(FlagCaptureReward, FlagCapturedReward);
		controlFunctions.add(new CF_NextOnTimeout(null,this, tickLimit));
		rewardFunctions.add(rewardTeamScore);
		rewardFunctions.add(rewardManual);
		
		makeWorld();
		registerBotsToWorld();
	}
	
   @Override
	public void postStepOps() {
	   super.postStepOps();
	}
   
	@Override
	public void botToPenalty(WarBot warBot) {
		super.botToPenalty(warBot);
		if(warBot.ID == SimulationEnvironment2DMultiBot.TEAM_2_ID)
		{
			rewardManual.addToCurrentValue(killReward);
			if(((S_FlagCarry)warBot.sensors.get("EFlagCarry")).normalizedValue == 1.0)
			{			
				rewardManual.addToCurrentValue(killFlagCarry);
			}
		}
	}
}
