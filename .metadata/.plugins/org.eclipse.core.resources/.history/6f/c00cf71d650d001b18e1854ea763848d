/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.bot.elements;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBotCTF;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.Projectile;

public class A_GunTrigger extends Actuator {
	double startOffset = 2.0;
	A_GunTraverse traverse;
	A_GunCharge charge;
	SimulationEnvironment2DMultiBotCTF simEnvironment;
	RewardFunction rw = null;

	public A_GunTrigger(Vec2 lp, float la, BotBody2D b, A_GunTraverse traverseIn, A_GunCharge chargeIn,
			SimulationEnvironment2DMultiBot simEnv) {
		super(lp, la, b);
		traverse = traverseIn;
		charge = chargeIn;
		simEnvironment = simEnv;
	}

	@Override
	public void step() {
		computeWorldPosAndAngle();
		if (normalizedValue > 0.5 && charge.isCharged()) {
			fire();
			charge.empty();
		}
	}

	private void fire() {
		if (rw != null)
			rw.addReward();
		if (simEnvironment != null) {
			float angle = (float) (worldAngle + ((1.0 - (traverse.normalizedValue * 2)) * Math.PI));
			Projectile p = new Projectile(new Vec2(worldPosition.x + ((float) (Math.cos(angle) * startOffset)),
					worldPosition.y + ((float) (Math.sin(angle) * startOffset))), angle, bot, simEnvironment);
			p.launch();
		}
	}

	public void setRewardFunction(RewardFunction rf) {
		rw = rf;
	}

	@Override
	public void reset() {
	}

}
