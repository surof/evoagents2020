/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments.deprecated;

import java.util.concurrent.ThreadLocalRandom;


import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Distance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollisionAndTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnMovingBackwardTooMuch;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.ControlFunction;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosingOnTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_FixedDistanceFromTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ForwardMotion;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_FreezeWhenTargetStop;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_NotMovingBackward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_SameOrientationAsTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_SameSpeedAsTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_SensorOverThreshold;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Speed;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_StraightMovement;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.DynamicWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.VirtualWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.Waypoint;
import evoagentsimulation.simulationlearning.ScoreCounter;
import evoagentsimulation.simulationlearning.SimulationInterruptFlag;

@SuppressWarnings all
@Deprecated
public class EXP_KeepSameSpeedAsTarget extends SimulationEnvironment2DSingleBot {
	
	// Constantes
	
	private final float MAX_BOT_SPEED = 0.07f;
	private final float WORLD_SIZE = 50;
	private final float TARGET_SIZE = 1;
	
	private final int TRAINING_TICK_LIMIT = 2000; // Durée d'un entrainement
	private final int TRAINING_FROM = 0;
	private final int TRAINING_TO = 9;
	
	// Variables
	
	private int training = TRAINING_FROM; // Entrainement courant
	private int trainingTickCount = 0;

	private Waypoint target;
	private Vec2 targetHeading;
	private Vec2 targetOffset;

	private double targetSpeed = 0;
	private double targetSpeedModif = 0;
	private double targetSpeedModifIncrement = 0;
	private double targetOrient = 1;
	// Methodes
	
	public EXP_KeepSameSpeedAsTarget(String botMod) {
		super(botMod);
		this.name = "KeepSameSpeedAsTarget";
		hasObstacles = false;
	}
	
	public void init() {
		super.init();
		
		getCorners()[0] = new Vec2(-WORLD_SIZE, WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
		
		botStartPos = new Vec2(0, 0);
		botStartAngle = 0;
		targetOffset = new Vec2(10, 0);
		
		makeBot();

		target = new Waypoint(botStartPos.add(targetOffset), 0, TARGET_SIZE);
		getWorldElements().add(target);

		((S_Radar)getBot().sensors.get("TargetOrient")).setTarget(target);       // orient
		((S_Distance)getBot().sensors.get("TargetDistance")).setTarget(target);  // distance

		controlFunctions.add(new CF_NextOnMovingBackwardTooMuch(getBot(), this, 4));
		
		rewardFunctions.add(new RW_SameSpeedAsTarget(getBot(), target, 1000));
		rewardFunctions.add(new RW_NotMovingBackward(getBot(), 1));
		
		// le meilleur score théorique possible en combinant toutes ces fonctions de récompense est 0
		
		initTraining(training);

		makeWorld();

		getBot().registerBotToWorld();

		getBot().body.getFixtureList().m_isSensor = true; // Disable bot collisions	
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();		

		if (trainingTickCount >= TRAINING_TICK_LIMIT) {
			reset();
			training = (training + 1 > TRAINING_TO) ? TRAINING_FROM : training + 1;
			trainingTickCount = 0;
			initTraining(training);
		}
		else {
			runTrainingStep(training);
			trainingTickCount++;
		}
	}
	
	@Override
	public void reset() {
		super.reset();

		for(RewardFunction r: rewardFunctions)
			r.reset();
		
		for(ControlFunction c : controlFunctions)
			c.reset();
	}

	private Vec2 angleToVec2(double angle, double length) {
		return new Vec2((float)(length * Math.cos(angle)), (float)(length * Math.sin(angle)));
	}
	
	private double vec2ToAngle(Vec2 v) {
		return Math.atan2(v.y, v.x);
	}
	
	private Vec2 randomCirclePosition(Vec2 center, double radius) {
		double theta = Math.random() * 2 * Math.PI;
		return angleToVec2(theta, radius).addLocal(center);
	}
	
	private Vec2 randomWorldPosition() {
		double r = WORLD_SIZE * Math.sqrt(Math.random()); // Sqrt permet d'avoir un distribution uniforme
		return randomCirclePosition(new Vec2(0, 0), r); // Renvoie une position a l'interieur du cercle du monde
	}
	
	private Vec2 randomDirection() {
		double theta = Math.random() * 2 * Math.PI;
		return angleToVec2(theta, 1); // Renvoie une direction normalisée au hasard
	}
	
	private void initTraining(int training) {
		
		// Orientation au hasard
		
		if (Math.random() < 0.5)
			targetOrient = -1;
		else 
			targetOrient = 1;
		
		// Positionnement et Direction
		
		switch (training) {
			// Cible devant le bot, dans la même orientation.
			case 0:
			case 1:
				target.worldPosition = botStartPos.add(targetOffset);
				targetHeading = new Vec2(1, 0);
				break;
			// Cible à la même position que le bot, orienté aléatoirement.
			case 2:
			case 3:
				target.worldPosition = new Vec2(botStartPos);
				targetHeading = randomDirection(); // Direction au hasard normalisé
				break;
			// Cible dans un certain rayon du bot en direction de celui-ci.
			case 4:
			case 5:
				target.worldPosition = randomCirclePosition(botStartPos, WORLD_SIZE); // Position sur le rayon du monde
				targetHeading = botStartPos.sub(target.worldPosition); // En se déplaçant vers le centre du monde
				targetHeading.normalize();
				break;
			// Cible à la position et orientation aléatoire.
			case 6:
			case 7:
			case 8:
				target.worldPosition = randomWorldPosition(); // Position à l'interieur du rayon du monde
				targetHeading = randomDirection(); // Direction au hasard normalisé
				break;
			case 9:
				break;
			default:
				System.out.println("Entrainement indisponible !");
		}
		
		// Vitesse
		
		switch (training) {
			case 0:
			case 2:
			case 4:
			case 6:
				targetSpeed = (Math.sin(ThreadLocalRandom.current().nextDouble()) + 1) * MAX_BOT_SPEED / 2; // Vitesse dans [0, MAX_BOT_SPEED]
				break;
			case 1:
			case 3:
			case 5:
			case 7:
				double maxSin = 0.5 * Math.PI;
				targetSpeedModif = ThreadLocalRandom.current().nextDouble();
				targetSpeedModifIncrement =  maxSin / (200 + Math.random() * 400);
				break;
			case 8:
			case 9:
				break;
			default:
				System.out.println("Entrainement indisponible !");
		}
	}
	
	private void runTrainingStep(int training) {
		switch (training) {
			// Vitesse cible constante prise au hasard dans [0, MAX_BOT_SPEED]
			case 0: // Cible devant bot
			case 2: // Cible sur le rayon du monde
			case 4:
				target.worldPosition.addLocal(targetHeading.mul((float)targetSpeed));
				break;
			// Vitesse cible variable dans [0, MAX_BOT_SPEED]
			case 1: // Cible devant bot
			case 3: // Cible sur le rayon du monde
			case 5:
				targetSpeed = (Math.sin(targetSpeedModif) + 1) * MAX_BOT_SPEED / 2;
				target.worldPosition.addLocal(targetHeading.mul((float)targetSpeed));	
				targetSpeedModif += targetSpeedModifIncrement;
				break;
			// Vitesse cible constante prise au hasard dans [0, MAX_BOT_SPEED]
			case 6: // Deplacement aléatoire
				target.worldPosition.addLocal(targetHeading.mul((float)targetSpeed));
				double angle = vec2ToAngle(targetHeading);
				angle += Math.random() * 0.2 - 0.1; // angle - n dans [-1, 1]
				targetHeading = angleToVec2(angle, 2);
				targetHeading.normalize();
				break;
			// Vitesse cible variable dans [0, MAX_BOT_SPEED]
			case 7: // Deplacement aléatoire
				targetSpeed = (Math.sin(targetSpeedModif) + 1) * MAX_BOT_SPEED / 2;
				target.worldPosition.addLocal(targetHeading.mul((float)targetSpeed));	
				targetSpeedModif += targetSpeedModifIncrement;
				angle = vec2ToAngle(targetHeading);
				angle += Math.random() * 0.2 - 0.1; // angle - n dans [-1, 1]
				targetHeading = angleToVec2(angle, 2);
				targetHeading.normalize();
				break;
			// Vitesse cible constante prise au hasard dans [0, MAX_BOT_SPEED]
			case 8: // Deplacement en orbite autour du centre du monde
				double toBotAngle = vec2ToAngle(target.worldPosition);
				double moveAngle = 0.001 * targetOrient;
				Vec2 nextPos = angleToVec2(toBotAngle + moveAngle, WORLD_SIZE);
				Vec2 translation = nextPos.sub(target.worldPosition);
				target.worldPosition.addLocal(translation);	
				break;
			// Vitesse cible nulle
			case 9: // Position aléatoire
				break;
			default:
				System.out.println("Entrainement indisponible !");
		}
	}
}
