/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.worldElements;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.CollisionDefines;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;

public class Projectile extends DynamicWorldElement {

	public String label = "";
	private float force = 2000;
	SimulationEnvironment2DMultiBot simEnvironment;
	BotBody2D bot;

	public Projectile(Vec2 worldPos, float worldAng, BotBody2D ibot, SimulationEnvironment2DMultiBot simEnv)
	{
		super(worldPos, worldAng,0.5f);
		sd.userData = this;
		sd.shape = new CircleShape();
		sd.shape.m_radius = 0.5f;
		//sd.friction = 1.0f;
		sd.density = 2.0f;
		sd.filter.categoryBits = CollisionDefines.CDProjectile;
		sd.filter.maskBits = CollisionDefines.CDAllMask;

		bd.angularDamping = 0.0f;
		bd.linearDamping = 0.0f;
		bd.allowSleep = false;
		simEnvironment = simEnv;
		bot = ibot;
		label = bot.label;
	}


	public void launch()
	{
		simEnvironment.getProjectiles().get(bot.ID).add(this);
		simEnvironment.getWorldElements().add(this);
		world = simEnvironment.getWorld();
		body = world.createBody(bd);
		body.createFixture(sd);
		Vec2 f = new Vec2((float)Math.cos(body.getAngle())*force,(float)Math.sin(body.getAngle())*force);
		body.applyForce(f, body.getPosition());
	}

	public void initProjectilePosition(Vec2 startPos,double startAngle)
	{
		bd.position.set(startPos.x,startPos.y);
		bd.angle = (float)startAngle;
	}

	public void removeProjectileFromWorld() 
	{
		simEnvironment.getProjectiles().get(bot.ID).remove(this);
		simEnvironment.getWorldElements().remove(this);
		removeFromWorld();
		world = null;
	}
}
