/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments.deprecated;

import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.A_GunTrigger;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_AgentGroupActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Distance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_FlagCarry;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_FlagState;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollisionAndTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosingOnTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ForwardMotion;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_PunishOnDeath;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_SensorOverThreshold;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Speed;
import evoagentsimulation.evoagent2dsimulator.multiagentexperiments.SimulationEnvironment2DMultiBotCTF;
import evoagentsimulation.evoagent2dsimulator.worldElements.Projectile;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

public class EXP_DodgeBullet extends SimulationEnvironment2DMultiBot{

	private Projectile bullet;
	private float bulletDistance = 10.0f;
	
	public EXP_DodgeBullet(ArrayList<String> botMod) {
		super(botMod);
		this.name = "DodgeBullet";
		hasObstacles = false;
		getCorners()[0] = new Vec2(-50.0f, 50.0f);
		getCorners()[1] = new Vec2(50.0f, 50.0f);
		getCorners()[2] = new Vec2(50.0f, -50.0f);
		getCorners()[3] = new Vec2(-50.0f, -50.0f);
	}
	
	@Override
	public void init() 
	{
		super.init();
		
		startAngle = new ArrayList<ArrayList<Double>>();
		
		startAngle.add(new ArrayList<Double>());
		startAngle.get(0).add((double) 180);
		
		startAngle.add(new ArrayList<Double>());
		startAngle.get(1).add((double) 180);

		
		startPos = new ArrayList<ArrayList<Vec2>>();
		startPos.add(new ArrayList<Vec2>());
		startPos.get(0).add(new Vec2(0,0));

		startPos.add(new ArrayList<Vec2>());
		startPos.get(1).add(new Vec2(-150,-150));
		
		
		
		bots.add(makeBots(botModel.get(0), 1, SimulationEnvironment2DMultiBot.TEAM_1_LABEL, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		bots.add(makeBots(botModel.get(1), 1, SimulationEnvironment2DMultiBot.TEAM_2_LABEL, SimulationEnvironment2DMultiBot.TEAM_2_ID));
		
		
		for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				((S_ConstantSensor)bots.get(i).get(j).sensors.get("ID")).setValue(((double)(j))/((double)(bots.get(i).size() - 1)));
				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearFriendlyActive")).setIDToTrack(i);
				((S_AgentGroupActive)bots.get(i).get(j).sensors.get("NearFriendlyGroupActive")).setIDToTrack(i);

				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearEnemyActive")).setIDToTrack(Math.abs(-1 + i));
				((S_AgentGroupActive)bots.get(i).get(j).sensors.get("NearEnemyGroupActive")).setIDToTrack(Math.abs(-1 + i));
				
				//sensor bullet  ( S_projectileactive?)
				
			}
		}
		
		
		double rndAngle = Math.random() * 360;
		Vec2 bulletPos = new Vec2((float)Math.cos(rndAngle)*bulletDistance,(float)Math.sin(rndAngle)*bulletDistance);
		float angleToCenter = 0.0f;
		
		bullet = new Projectile(bulletPos,angleToCenter,bots.get(1).get(0),this);
		
		controlFunctions.add(new CF_NextOnTimeout(bots.get(0).get(0),this, 100000));
		rewardFunctions.add( new RW_PunishOnDeath(bots.get(0).get(0),1000));
		//rewardFunctions.add(new RW_Speed(bots.get(0).get(0),2));
		
		makeWorld();
		registerBotsToWorld();

	}

}
