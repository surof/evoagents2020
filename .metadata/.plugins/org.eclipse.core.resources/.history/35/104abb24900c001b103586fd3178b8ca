/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.bot;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

import evoagentsimulation.evoagent2dsimulator.CollisionDefines;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.A_AutoClaw;
import evoagentsimulation.evoagent2dsimulator.bot.elements.A_SignalEmitter;
import evoagentsimulation.evoagent2dsimulator.bot.elements.A_Wheel;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_AgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Distance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Manual;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_MovementSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectDetector;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PointListenerDistance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PointListenerOrient;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ProximityArcSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PointListenerSignalActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;

public class RoleForageBot extends BotBody2D{
	SimulationEnvironment2DMultiBot multEnv = null;
	SimulationEnvironment2DSingleBot singEnv = null;

	public RoleForageBot(SimulationEnvironment2D env)
	{
		super(env);

		if(env instanceof SimulationEnvironment2DMultiBot)
			multEnv = (SimulationEnvironment2DMultiBot) env;
		if(env instanceof SimulationEnvironment2DSingleBot)
			singEnv = (SimulationEnvironment2DSingleBot) env;
		sd = new FixtureDef();
		sd.shape = new CircleShape(); 
		sd.shape.m_radius = (float)size;
		sd.friction = 0.0f;
		sd.density = 2.0f;
		sd.filter.categoryBits = CollisionDefines.CDBot;
		sd.filter.maskBits = CollisionDefines.CDAllMask;

		bd = new BodyDef();
		bd.type = BodyType.DYNAMIC;
		bd.angularDamping = 20.0f;
		bd.linearDamping = 5.0f;
		
		bd.allowSleep = false;

		//Sensors
		sensors.put("ID",new S_ConstantSensor(new Vec2((float)size,0.0f),0, this, 0));
		sensors.put("PAYOFF",new S_Manual(new Vec2((float)size,0.0f),0, this, 0.5));
		
		sensors.put("S1",new S_ProximityArcSensor(new Vec2((float)size,0.0f),0, this, 12.0));
		sensors.put("S2",new S_ProximityArcSensor(new Vec2((float)(size*Math.cos(((double)1.0*(Math.PI/8)))),(float)(size*Math.sin(((double)1.0*(Math.PI/8))))),(float)((double)1.0*(Math.PI/8)), this, 12.0));
		for(int i = 1 ; i < 8 ;i++)
			sensors.put("S" + (2+i),new S_ProximityArcSensor(new Vec2((float)(size*Math.cos((((double)i)*(Math.PI/4)))),(float)(size*Math.sin((((double)i)*(Math.PI/4))))),(float)(((double)i)*(Math.PI/4)), this, 12.0));
		sensors.put("S10",new S_ProximityArcSensor(new Vec2((float)(size*Math.cos(((double)15.0*(Math.PI/8)))),(float)(size*Math.sin(((double)15.0*(Math.PI/8))))),(float)((double)15.0*(Math.PI/8)), this, 12.0));
		sensors.put("DOF",new S_MovementSensor(new Vec2(0,0),0, this,20));	

		sensors.put("RADOBJA",new S_Radar(new Vec2(0,0),0, this,null));
		sensors.put("RADOBJB",new S_Radar(new Vec2(0,0),0, this,null));
		sensors.put("DISTOBJA",new S_Distance(new Vec2(0,0),0, this, (S_Radar)sensors.get("RADOBJA"),150));
		sensors.put("DISTOBJB",new S_Distance(new Vec2(0,0),0, this, (S_Radar)sensors.get("RADOBJB"),150));
		sensors.put("SENSOBJ",new S_ObjectDetector(new Vec2((float)size,0),0, this,2.0,0.2));
		sensors.put("RADDZ",new S_Radar(new Vec2(0,0),0, this,null));
		sensors.put("SENSDZ",new S_ZonePresence(new Vec2(0,0),0, this,null));
		
		//Actuators
		actuators.put("MotL",new A_Wheel(new Vec2(0.0f,-(float)size),0, this,80.0f));
		actuators.put("MotR",new A_Wheel(new Vec2(0.0f,(float)size),0, this,80.0f));
		actuators.put("EMAG",new A_AutoClaw(new Vec2((float)size,0f),0, this,1.5f));
	}
}

