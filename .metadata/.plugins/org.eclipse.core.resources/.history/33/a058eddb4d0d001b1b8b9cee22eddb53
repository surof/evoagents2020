/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.HashMap;

import javax.swing.JFrame;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D;
import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import ui.SimEnv2DViewer;
import ui.SimEnv3DViewer;

public class EATaskDemoSimSingleBot3D extends EATaskDemoSimSingleBot {
	Thread viewerThread;
	HashMap<String,Double> actuatorValues = null;
	SimEnv3DViewer simPanel;
	JFrame frame;
	
	public EATaskDemoSimSingleBot3D(String root, String botmodel, String botname, String masterS, String driveN,String envirName)
	{
		super(root, botmodel, botname, masterS, driveN, envirName);
		envClasspath = EvoAgentAppDefines.classpath3D;
		mindTemplate = EvoAgentMindFactory.loadMindTemplate(MindRootFolder,botname, masterSkill, mindDriveClassName);
		mind = EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
	}

	protected void viewerStep() {
		simPanel.repaint();
	}

	protected void initViewer() {	
		simPanel = new SimEnv3DViewer((SimulationEnvironment3D) environment, scoreCounter); 
		frame = new JFrame(environmentName);
		frame.add(simPanel);
		frame.setSize(800, 600);
		frame.setVisible(true);

		/*
		JFrame frame = new JFrame("Swing and JavaFX");
        final JFXPanel fxPanel = new JFXPanel();
        frame.add(fxPanel);
        frame.setSize(300, 200);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initFX(fxPanel);
            }
       });
       */
		/*
		SimEnv3DViewer.world = (SimulationEnvironment3D) environment;
		viewerThread = new Thread() {
			public void run() {
					SimEnv3DViewer.launch(SimEnv3DViewer.class);					
			}
		};
		viewerThread.start();	
		*/
	}
	
	  private static void initFX(JFXPanel fxPanel) {
	        // This method is invoked on the JavaFX thread
	        Scene scene = createScene();
	        fxPanel.setScene(scene);
	    }

	    private static Scene createScene() {
	        Group  root  =  new  Group();
	        Scene  scene  =  new  Scene(root, Color.ALICEBLUE);
	        Text  text  =  new  Text();
	        
	        text.setX(40);
	        text.setY(100);
	        text.setFont(new Font(25));
	        text.setText("Welcome JavaFX!");

	        root.getChildren().add(text);

	        return (scene);
	    }

	protected boolean checkContinue() {
		return viewerThread.isAlive();
	}
	
	protected boolean checkRepetitionContinue() {
		return viewerThread.isAlive();
	}
	
	protected void closeViewer() {
		SimEnv3DViewer.stopPlatform();
	}
	
	protected void initEnvironement()
	{
		System.out.println("env : " + environment);
		environment.setBotModel(botModel);
		((SimulationEnvironment3D)environment).setBotMind(mind);
		environment.init();
	}
}
