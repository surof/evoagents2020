/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.multiagentexperiments;

import java.util.ArrayList;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectDetector;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ForwardMotion;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.DynamicWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

public class EXP_Search extends SimulationEnvironment2DMultiBot{

	private RW_ManualReward manualReward;
	private TriggerZone dz ;
	private int botCount = 6;
	private int WORLD_SIZE = 400;
	private int formationInitX = -WORLD_SIZE + 20;
	private int formationInitY = -WORLD_SIZE + 20;
	private TargetObject to ;
	

	int tickLimit = 100000;
	
	public EXP_Search() {
		super();
		this.name = "search";
		hasObstacles = false;
		//obstacleSpacing =80;
		//minObstacleSize = 5;
		getCorners()[0] = new Vec2(-WORLD_SIZE , WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
	}
	
	@Override
	public void init() 
	{
		super.init();

		manualReward = new RW_ManualReward(null, 1.0);
		controlFunctions.add(new CF_NextOnTimeout(null, this, tickLimit));
		rewardFunctions.add(manualReward);
		
		startAngle = new ArrayList<ArrayList<Double>>();
		startAngle.add(new ArrayList<Double>());
		startPos = new ArrayList<ArrayList<Vec2>>();
		startPos.add(new ArrayList<Vec2>());
		for(int i = 0; i < botCount; i++)
		{
			startAngle.get(0).add((double) 0);
			startPos.get(0).add(new Vec2(formationInitX + (i * 100), formationInitY + (i * 100)));			
		}
				
		bots = new ArrayList<ArrayList<BotBody2D>>();
		bots.add(makeBots(botModel.get(0), botCount, SimulationEnvironment2DMultiBot.TEAM_1_LABEL, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		makeBotMinds(mindTemplates);

		to = new TargetObject(new Vec2(-20,-20), (float)(Math.PI/4), 1);
		getWorldElements().add(to);
		
		for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				((S_ConstantSensor)bots.get(i).get(j).sensors.get("ID")).setValue(((double)(j))/((double)(bots.get(i).size() - 1)));
				//((S_ZonePresence)bots.get(i).get(j).sensors.get("SENSDZ")).setTarget(dz);
				//((S_Radar)bots.get(i).get(j).sensors.get("RADDZ")).setTarget(dz);
				//((S_ObjectDetector)bots.get(i).get(j).sensors.get("SENSOBJ")).setTarget(to);;
				//((S_ObjectListActive)bots.get(i).get(j).sensors.get("ACTOBJA")).setList(new ArrayList<>());;
				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearFriendlyActive")).setIDToTrack(i);
				rewardFunctions.add(new RW_ForwardMotion(bots.get(i).get(j), 0.0001));
			}
		}

		makeWorld();
		registerBotsToWorld();
		to.registerToWorld(getWorld());
		posTargetObject(to);
	}

	private void posTargetObject(DynamicWorldElement obj) {
		Vec2 pos = new Vec2(generatePositionInBoundaries(10f));
		float space = 200;
		
		while(!checkElementPositionConficts(pos,space) || !checkObstaclePositionConficts(pos,1f+3.0f))
		{
			pos.set(generatePositionInBoundaries(10f));
			if(space > 80)
				space -= 5f;
		}
		obj.setWorldPosition(pos.x,pos.y);
		obj.body.setLinearVelocity(new Vec2(0,0));
		obj.body.setAngularVelocity(0);
	}

	@Override
	public void postStepOps() {
		super.postStepOps();
		//System.out.println("step");
		for(BotBody2D b : bots.get(0))
		{
			if(MathUtils.distance(to.getWorldPosition(),b.body.getPosition())<40)
			{
				/*
				System.out.println(to.getWorldPosition().x + " - "+to.getWorldPosition().y);
				System.out.println(b.body.getPosition().x + " - "+b.body.getPosition().y);
				System.out.println(MathUtils.distance(to.getWorldPosition(),b.body.getPosition()));*/
				manualReward.addToCurrentValue(1);
				posTargetObject(to);
				return;
			}
		}
	}
	
	@Override
	public void reset()
	{
		super.reset();
		posTargetObject(to);
		for(RewardFunction r: rewardFunctions)
			r.reset();
	}
}
