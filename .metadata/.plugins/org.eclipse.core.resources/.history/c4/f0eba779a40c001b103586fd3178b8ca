/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.multiagentexperiments;

import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBotCTF;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.bot.WarBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.A_GunTrigger;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentGroupActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Distance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_FlagCarry;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_FlagState;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Kills;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_StepReward;
import evoagentsimulation.evoagent2dsimulator.worldElements.Flag;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

public class EW_CTF_shootTrainingTest  extends SimulationEnvironment2DMultiBotCTF {


	RW_Kills rewardKills;
	RW_StepReward shootCost = new RW_StepReward(null, -0.001);
	
	public EW_CTF_shootTrainingTest(ArrayList<String> botMod)
	{
		super(botMod);
		penaltyTime = 1000;
		this.name = "CTF";
		hasObstacles = true;
		getCorners()[0] = new Vec2(-150.0f, 150.0f);
		getCorners()[1] = new Vec2(150.0f, 150.0f);
		getCorners()[2] = new Vec2(150.0f, -150.0f);
		getCorners()[3] = new Vec2(-150.0f, -150.0f);
		
		obstacleSpacing = 50.0;
		minObstacleSize = 2;
		maxObstacleVariability = 10;
	}
	
	@Override
	public void init() 
	{
		// set starting angle for each team (0 - 1)
		startAngle = new ArrayList<ArrayList<Double>>();
		startAngle.add(new ArrayList<Double>());
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);
		startAngle.get(0).add((double) 180);

		startAngle.add(new ArrayList<Double>());
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		startAngle.get(1).add((double) 0);
		
		// set starting positions for each team (0 - 1)
		startPos = new ArrayList<ArrayList<Vec2>>();
		startPos.add(new ArrayList<Vec2>());
		startPos.get(0).add(new Vec2(130,-130));
		startPos.get(0).add(new Vec2(130,-135));
		startPos.get(0).add(new Vec2(130,-140));
		startPos.get(0).add(new Vec2(140,-130));
		startPos.get(0).add(new Vec2(140,-135));
		startPos.get(0).add(new Vec2(140,-140));
		startPos.get(0).add(new Vec2(120,-130));
		startPos.get(0).add(new Vec2(120,-135));
		startPos.get(0).add(new Vec2(120,-140));

		startPos.add(new ArrayList<Vec2>());
		startPos.get(1).add(new Vec2(-130,130));
		startPos.get(1).add(new Vec2(-130,135));
		startPos.get(1).add(new Vec2(-130,140));
		startPos.get(1).add(new Vec2(-140,130));
		startPos.get(1).add(new Vec2(-140,135));
		startPos.get(1).add(new Vec2(-120,140));
		startPos.get(1).add(new Vec2(-120,130));
		startPos.get(1).add(new Vec2(-120,135));
		startPos.get(1).add(new Vec2(-120,140));

		// create the bots for each team
		bots = new ArrayList<ArrayList<BotBody2D>>();
		bots.add(makeBots(botModel.get(0), 3, SimulationEnvironment2DMultiBot.TEAM_1_LABEL, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		bots.add(makeBots(botModel.get(1), 3, SimulationEnvironment2DMultiBot.TEAM_2_LABEL, SimulationEnvironment2DMultiBot.TEAM_2_ID));
		// create an individual MIND instance for each bot, using the team template
		makeBotMinds(mindTemplates);

		// obectives
		teamBasesPositions.add(new Vec2(130,130));
		teamBasesPositions.add(new Vec2(-130,-130));
		
		flags.add(new Flag(teamBasesPositions.get(0), 0, 5, 0));
		flags.add(new Flag(teamBasesPositions.get(1), 0, 5, 1));
		
		teamZones.add(new TriggerZone(teamBasesPositions.get(0), 0, 10,SimulationEnvironment2DMultiBot.TEAM_1_LABEL));
		teamZones.add(new TriggerZone(teamBasesPositions.get(1), 0, 10,SimulationEnvironment2DMultiBot.TEAM_2_LABEL));
		
		// add objective to renderer
		for(Flag f : flags)
			worldElements.add(f);
		for(TriggerZone t : teamZones)
			worldElements.add(t);
		
		// set objectives and targets for each team
		for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				((S_ConstantSensor)bots.get(i).get(j).sensors.get("ID")).setValue(((double)(j))/((double)(bots.get(i).size() - 1)));
				((S_Radar)bots.get(i).get(j).sensors.get("FBaseOrient")).setTarget(teamZones.get(i));
				((S_Distance)bots.get(i).get(j).sensors.get("FBaseDistance")).setTarget(teamZones.get(i));
				((S_Radar)bots.get(i).get(j).sensors.get("FFlagOrient")).setTarget(flags.get(i));
				((S_Distance)bots.get(i).get(j).sensors.get("FFlagDistance")).setTarget(flags.get(i));
				((S_FlagState)bots.get(i).get(j).sensors.get("FFlagState")).setTarget(flags.get(i));
				((S_FlagCarry)bots.get(i).get(j).sensors.get("FFlagCarry")).setFlag(flags.get(i));
				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearFriendlyActive")).setIDToTrack(i);
				((S_PLAgentGroupActive)bots.get(i).get(j).sensors.get("NearFriendlyGroupActive")).setIDToTrack(i);

				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearEnemyActive")).setIDToTrack(Math.abs(-1 + i));
				((S_PLAgentGroupActive)bots.get(i).get(j).sensors.get("NearEnemyGroupActive")).setIDToTrack(Math.abs(-1 + i));
				((S_Radar)bots.get(i).get(j).sensors.get("EBaseOrient")).setTarget(teamZones.get(Math.abs(-1 + i)));
				((S_Distance)bots.get(i).get(j).sensors.get("EBaseDistance")).setTarget(teamZones.get(Math.abs(-1 + i)));
				((S_Radar)bots.get(i).get(j).sensors.get("EFlagOrient")).setTarget(flags.get(Math.abs(-1 + i)));
				((S_Distance)bots.get(i).get(j).sensors.get("EFlagDistance")).setTarget(flags.get(Math.abs(-1 + i)));	
				((S_FlagState)bots.get(i).get(j).sensors.get("EFlagState")).setTarget(flags.get(Math.abs(-1 + i)));			
				((S_FlagCarry)bots.get(i).get(j).sensors.get("EFlagCarry")).setFlag(flags.get(Math.abs(-1 + i)));
				if(i == SimulationEnvironment2DMultiBot.TEAM_1_ID)
					((A_GunTrigger)bots.get(i).get(j).actuators.get("GunTrig")).setRewardFunction(shootCost);
			}
		}
		// init score 
		for(int i = 0 ; i < bots.size() ; i++)
			teamScores.add(0);
		
		rewardKills = new RW_Kills(1.0);
		controlFunctions.add(new CF_NextOnTimeout(null,this, 100000));
		rewardFunctions.add(rewardKills);
		rewardFunctions.add(shootCost);
		
		makeWorld();
		registerBotsToWorld();
	}
	
	  @Override
	  public void postStepOps() {
		  super.postStepOps();
		}
 
		@Override
		public void botToPenalty(WarBot warBot) {
			super.botToPenalty(warBot);
			if(warBot.ID == SimulationEnvironment2DMultiBot.TEAM_2_ID)
				rewardKills.scoreFor();
		}
}
