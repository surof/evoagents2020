/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectDetector;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosingOnTargetVariableReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.DynamicWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

public class EXP_CollectLarge extends SimulationEnvironment2DSingleBot {

	private TriggerZone dz ;
	private TargetObject to ;
	private RW_ManualReward manualReward;
	private float targSize = 3f;
	private float initTargSize = 3f;
	
	public EXP_CollectLarge()
	{
		super();
		this.name = "Collect";
		hasObstacles = true;
	}
	
	public void init() 
	{
		super.init();

		int WORLD_SIZE = 220;
		getCorners()[0] = new Vec2(-WORLD_SIZE , WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
	    minObstacleSize = 5.0;
	    maxObstacleSizeVariability = 4.0;
	    maxObstacleSpacingVariability = 4.0;
	    obstacleSpacing = 50.0;
		
		botStartPos = new Vec2(-00.5f,-0.0f);		
		makeBot();

		manualReward = new RW_ManualReward( getBot(), 0.0);
		rewardFunctions.add(manualReward);
		//controlFunctions.add(new CF_NextOnCollisionAndTimeout(bot,this, 50000));
		dz = new TriggerZone(new Vec2(-150,-150), 0,10);
		((S_ZonePresence)getBot().sensors.get("SENSDZ")).setTarget(dz);
		((S_Radar)getBot().sensors.get("RADDZ")).setTarget(dz);
		getWorldElements().add(dz);
		to = new TargetObject(new Vec2(-20,-20), (float)(Math.PI/4), initTargSize);
		((S_ObjectDetector)getBot().sensors.get("SENSOBJ")).setTarget(to);
		((S_PLObjectListActive)getBot().sensors.get("ACTOBJA")).setTarget(to);
		getWorldElements().add(to);
		rewardFunctions.add(new RW_ClosingOnTargetVariableReward( getBot(), 0.0005, 1.5,to));
		rewardFunctions.add(new RW_ClosingOnTargetVariableReward( getBot(), 0.0005, 1.5,dz));
		makeWorld();
		to.registerToWorld(getWorld());
		getBot().registerBotToWorld();
		posTargetObject(to);
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();
		if(dz.isPointInZone(to.getWorldPosition())&&getBot().actuators.get("EMAG").normalizedValue<0.5)
		{
			posTargetObject(to);
			for(RewardFunction r: rewardFunctions)
				r.reset();
			manualReward.addToCurrentValue(5);
		}
	}
		
	private void posTargetObject(DynamicWorldElement obj) {
		Vec2 pos = new Vec2(generatePositionInBoundaries(10f));
		
		while(!checkElementPositionConficts(pos,targSize*2) || !checkObstaclePositionConficts(pos,targSize+8.0f))
			pos.set(generatePositionInBoundaries(10f));
		obj.setWorldPosition(pos.x,pos.y);
		obj.body.setLinearVelocity(new Vec2(0,0));
		obj.body.setAngularVelocity(0);
		
		obj.size = targSize;
		obj.body.getFixtureList().getShape().m_radius = targSize;
		targSize = Math.max(targSize -0.5f, 1.0f);
	}
	
	@Override
	protected Vec2 generatePositionInBoundaries(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[0].y - margin))
				);
	}
	

	@Override
	public void reset()
	{
		super.reset();
		targSize = initTargSize;
		posTargetObject(to);
		for(RewardFunction r: rewardFunctions)
			r.reset();
	}
}
