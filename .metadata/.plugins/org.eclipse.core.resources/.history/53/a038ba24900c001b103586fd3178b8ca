/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments;

import java.awt.Color;
import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentmindelements.modules.drive.DM_CollectSelectObject;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.A_AutoClaw;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectListActive;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.WorldElement;

public class EXP_SelectObjectCollect extends SimulationEnvironment2DSingleBot {

	private RW_ManualReward manualReward;
	private ArrayList<WorldElement> targetObjectListA ;
	private ArrayList<WorldElement> targetObjectListB ;
	private int objectGenerateCount = 3;
	private float objectSize = 1.0f;
	DM_CollectSelectObject drive;
	double roleValue = 0.5; 
	
	public EXP_SelectObjectCollect()
	{
		super();
		this.name = "Select";
		hasObstacles = false;
		makeCircleWorld = true;
	}
	
	public void init() 
	{
		super.init();
		botStartPos = new Vec2(-00.5f,-100.0f);		
		makeBot();
		int WORLD_SIZE = 250;
		getCorners()[0] = new Vec2(-WORLD_SIZE , WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
	    minObstacleSize = 2.2;
	    maxObstacleSizeVariability = 4.0;
	    maxObstacleSpacingVariability = 12.0;
	    obstacleSpacing = 48.0;

		manualReward = new RW_ManualReward( getBot(), 0.0);
		rewardFunctions.add(manualReward);
		controlFunctions.add(new CF_NextOnTimeout(getBot(),this, 20000));
		targetObjectListA = new ArrayList<WorldElement>();
		targetObjectListB = new ArrayList<WorldElement>();
		((S_ObjectListActive)getBot().sensors.get("ACTOBJA")).setList(targetObjectListA);
		((S_ObjectListActive)getBot().sensors.get("ACTOBJB")).setList(targetObjectListB);

		makeWorld();
		getBot().registerBotToWorld();		
		generateObjects();
	}
	
	@Override
	public void preRunOps() {
		super.preRunOps();
		drive = new DM_CollectSelectObject(mind);
		mind.setDriveModule(drive);
		mind.checkInputOutputUse();
		roleValue = Math.random();
		drive.setRoleValue(roleValue);
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();
		if(((A_AutoClaw)getBot().actuators.get("EMAG")).hasObject())
		{
			boolean err = false;
			TargetObject pick = ((A_AutoClaw)getBot().actuators.get("EMAG")).getObject();
			if (pick.getName().equals("A"))
			{
				targetObjectListA.remove(pick);
				if(roleValue < 0.4)
					manualReward.addToCurrentValue(1.0);
				else if(roleValue < 0.5)
					manualReward.addToCurrentValue(0.8);
				else if(roleValue < 0.6)
					manualReward.addToCurrentValue(0.2);
				else 
				{
					manualReward.addToCurrentValue(-1.0);
					err = true;
				}
			}
			else if (pick.getName().equals("B"))
			{
				targetObjectListB.remove(pick);
				if(roleValue > 0.6)
					manualReward.addToCurrentValue(1.0);
				else if(roleValue > 0.5)
					manualReward.addToCurrentValue(0.8);
				else if(roleValue > 0.4)
					manualReward.addToCurrentValue(0.2);
				else 
				{
					manualReward.addToCurrentValue(-1.0);				
					err = true;					
				}
			}
			pick.removeFromWorld();
			getWorldElements().remove(pick);
			((A_AutoClaw)getBot().actuators.get("EMAG")).reset();
			generateObjects();
			if(!err)
			{
				roleValue = Math.random();
				drive.setRoleValue(roleValue);				
			}
		}
	}

	@Override
	protected Vec2 generatePositionInBoundaries(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[0].y - margin))
				);
	}
	
	protected Vec2 generatePositionInBoundariesA(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - (margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[0].y - margin))
				);
	}
	
	protected Vec2 generatePositionInBoundariesB(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x + (margin))),
				(float)(Math.random() * (getCorners()[0].y - margin))
				);
	}
	
	protected Vec2 generateLocalPosition(Vec2 ref, float spread) {
		return new Vec2(
				(float)((Math.random() * spread * 2) + ref.x - spread),
				(float)((Math.random() * spread * 2) + ref.y - spread)
				);
	}
	
	private void generateObjectA(float margin) {	
		TargetObject to;
		Vec2 posObj;
		posObj = new Vec2(generatePositionInBoundariesA(margin));
		while(!checkElementPositionConficts(posObj,objectSize+8.0f) || !checkObstaclePositionConficts(posObj,objectSize+18.0f) || !checkInBoundaries(posObj,objectSize+18.0f))
		{
			posObj = new Vec2(generatePositionInBoundariesA(margin));
		}
		to = new TargetObject(posObj, 1.0f, objectSize,"A");
		getWorldElements().add(to);
		targetObjectListA.add(to);	
		to.registerToWorld(getWorld());
	}
	
	private void generateObjectB(float margin) {	
		TargetObject to;
		Vec2 posObj;
		posObj = new Vec2(generatePositionInBoundariesB(margin));
		while(!checkElementPositionConficts(posObj,objectSize+8.0f) || !checkObstaclePositionConficts(posObj,objectSize+18.0f) || !checkInBoundaries(posObj,objectSize+18.0f))
		{
			posObj = new Vec2(generatePositionInBoundariesB(margin));
		}
		to = new TargetObject(posObj, 1.0f, objectSize,"B");
		to.setColor(Color.magenta);
		getWorldElements().add(to);
		targetObjectListB.add(to);	
		to.registerToWorld(getWorld());
	}
	
	private void generateObjects() {	
		float margin = 20.0f;
		if(targetObjectListA.size() == 0)
		{
			for(int i = 0; i < objectGenerateCount; i++)
			{
				generateObjectA(margin);
			}				
		}
		if(targetObjectListB.size() == 0)
		{	
			for(int i = 0; i < objectGenerateCount; i++)
			{
				generateObjectB(margin);
			}	
		}
	}
	@Override
	public void reset()
	{
		super.reset();
		clearTargetObjectList();
		for(RewardFunction r: rewardFunctions)
			r.reset();
		generateObjects();
		roleValue = Math.random();
		drive.setRoleValue(roleValue);	
	}
	
	public void clearTargetObjectList()
	{
		for(WorldElement tObj : targetObjectListA)
		{
			tObj.removeFromWorld();
			getWorldElements().remove(tObj);
		}
		targetObjectListA.clear();
		for(WorldElement tObj : targetObjectListB)
		{
			tObj.removeFromWorld();
			getWorldElements().remove(tObj);
		}
		targetObjectListB.clear();
	}
}
