/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintStream;
import java.util.List;

import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;

import Utils.StreamToJText;
import evoagentapp.EvoAgentApp;
import evoagentapp.tasks.EATaskDemoSimMultiBot;
import evoagentapp.tasks.EATaskDemoSimSingleBot;
import ui.mindviewer.MINDViewer;

public class EvoAgentAppFrame {
	static JMenuItem MI_GEN_OPEN = new JMenuItem("Open");
	static JMenuItem MI_GEN_END = new JMenuItem("End Task");
	static JMenuItem MI_GEN_EXIT = new JMenuItem("Exit");
	static JRadioButtonMenuItem MI_VIW_SIM = new JRadioButtonMenuItem("Simulation Panel");
	static JRadioButtonMenuItem MI_VIW_MIND = new JRadioButtonMenuItem("Mind Panel");
	static JMenuItem MI_HLP_ABOUT = new JMenuItem("About");
	static JLabel PROCSTITLE = new JLabel("Ressources");
	public static JSlider PROCS = new JSlider();
	static JFrame frame = null;
	static JPanel menuPanel;
	MenuListener listener;

	JFrame mindView = null;
	MINDViewer mindPanel = null;
	
	@SuppressWarnings("serial")
	public EvoAgentAppFrame()
	{
	       	frame = new JFrame("EvoAgentMindServer");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.setPreferredSize(new Dimension(1024,600));
			JMenuBar mb = new JMenuBar();
			frame.setJMenuBar(mb);
			frame.setLayout(new BorderLayout());
			MenuListener listener = new MenuListener(frame);

			menuPanel = new JPanel();
			PROCS.setName("Ressources");
			PROCS.setModel(new DefaultBoundedRangeModel(Runtime.getRuntime().availableProcessors(), 0, 1, Runtime.getRuntime().availableProcessors()));
			menuPanel.add(PROCSTITLE);			
			menuPanel.add(PROCS);
					
			JMenu gen = new JMenu("General");
			mb.add(gen);

			MI_GEN_END.addActionListener(listener);
			gen.add(MI_GEN_END);
			
			MI_GEN_OPEN.addActionListener(listener);
			gen.add(MI_GEN_OPEN);
			
			MI_GEN_EXIT.addActionListener(listener);
			gen.add(MI_GEN_EXIT);
			
			JMenu view = new JMenu("View");
			mb.add(view);

			MI_VIW_SIM.addActionListener(listener);
			view.add(MI_VIW_SIM);
			
			MI_VIW_MIND.addActionListener(listener);
			view.add(MI_VIW_MIND);
			
			JMenu hlp = new JMenu("Help");
			mb.add(hlp);

			MI_HLP_ABOUT.addActionListener(listener);
			hlp.add(MI_HLP_ABOUT);
	      
	        
	        JTextArea textArea = new JTextArea();
	        JScrollPane scrollPane = new JScrollPane(textArea);
	        
	        //SmartScroller scrl = new SmartScroller(scrollPane, SmartScroller.VERTICAL, SmartScroller.END);
	        
	        DropTarget dt = new DropTarget() {
				@Override
				public void drop(DropTargetDropEvent evt) {
					 try {
				            evt.acceptDrop(DnDConstants.ACTION_COPY);
				            @SuppressWarnings("unchecked")
							List<File> droppedFiles = (List<File>)
				                evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
				            for (File file : droppedFiles) {
				                System.out.println(file.getName());
				                if(EvoAgentApp.currentTask == null)
						    		EvoAgentApp.currentTask = EvoAgentApp.makeTaskFromFile(file.getAbsolutePath());
				            }
				        } catch (Exception ex) {ex.printStackTrace();}}};
			dt.setComponent(textArea);
	        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
			frame.getContentPane().add(menuPanel, BorderLayout.NORTH);	
	        PrintStream printStream = new PrintStream(new StreamToJText(textArea));
	        System.setOut(printStream);
	        System.setErr(printStream);
	        frame.pack();
	        frame.setVisible(true);
	}

	public int getAvailableThreads() {
		return PROCS.getValue();
	}
	
	private class MenuListener implements ActionListener {
		public MenuListener(JFrame inwin) {}
		@Override
		public void actionPerformed(ActionEvent e) {
			JMenuItem source = (JMenuItem) e.getSource();

			if (source.equals(MI_GEN_OPEN)) {
			    JFileChooser chooser = new JFileChooser();			    
			    int returnVal = chooser.showOpenDialog(frame);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	if(EvoAgentApp.currentTask == null)
			    	{
			    		EvoAgentApp.currentTask = EvoAgentApp.makeTaskFromFile(chooser.getSelectedFile().getAbsolutePath());
			    	}
			    }
			}else if(source.equals(MI_GEN_END)){
				if(mindView != null)
					mindView.setVisible(false);
				mindView = null;
				MI_VIW_MIND.setSelected(false);
				EvoAgentApp.endTask();
				
			}else if(source.equals(MI_GEN_EXIT)){
				EvoAgentApp.stop = true;
				System.exit(0);
			}else if(source.equals(MI_VIW_SIM)){
				
			}else if(source.equals(MI_VIW_MIND)){
				if(EvoAgentApp.currentTask != null)
				{
				if(mindView == null)
				{
					if(MI_VIW_MIND.isSelected())
					{
						if(EvoAgentApp.currentTask instanceof EATaskDemoSimSingleBot)
							mindPanel = new MINDViewer(EvoAgentApp.currentTask.getMind());
						else if(EvoAgentApp.currentTask instanceof EATaskDemoSimMultiBot)
							mindPanel = new MINDViewer(EvoAgentApp.currentTask.getMinds());			
						mindPanel.displayMIND();
						mindView = new JFrame("MIND Viewer");
						mindView.add(mindPanel);
						mindView.setSize(800, 600);
						//mindView.setJMenuBar(mindPanel.mb);
						mindView.setVisible(true);
					}
				}
				else
				{
					if(MI_VIW_MIND.isSelected())
					{
						mindView.setVisible(true);
					}
					else
					{
						mindView.setVisible(false);						
					}
				}
				}
			}else if(source.equals(MI_HLP_ABOUT)){
				JLabel lab1 = new JLabel("EvoAgent");
				JLabel lab2 = new JLabel("Copyright (C) 2019  SURO François");
				JLabel lab3 = new JLabel("GNU GENERAL PUBLIC LICENSE");
				JLabel lab4 = new JLabel("Version 3, 29 June 2007");
				JLabel lab5 = new JLabel("");
				JLabel lab6 = new JLabel("This program comes with ABSOLUTELY NO WARRANTY");
				JLabel lab7 = new JLabel("This is free software, and you are welcome to redistribute it");
				JLabel lab8 = new JLabel("under certain conditions");
		
				Object[] array = {lab1,lab2,lab3,lab4,lab5,lab5,lab6,lab7,lab8};
				JOptionPane.showMessageDialog(frame, array,"About",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

}
