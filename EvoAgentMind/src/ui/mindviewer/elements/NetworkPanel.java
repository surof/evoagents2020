/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package ui.mindviewer.elements;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;
import javax.swing.Timer;

import org.encog.ml.MLEncodable;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.modules.SkillModule;

@SuppressWarnings("serial")
public class NetworkPanel extends JPanel{

	EvoAgentMind mind;
	NetworkPanel panelAdress = this;
	protected double neuronSize = 20;
	protected double neuronSpacing = 120;
	protected double layerSpacing = 180;
	protected double moduleSpacing = 70;
	int compteur = 0;
	int initXTrigger = 12;
	public double offsetX;
	public double offsetY = 0;
	double zoomLevel = 1.0;

	public MLEncodable currentNetwork = null;
	protected ArrayList<GNode> gNodes = new ArrayList<>();
	protected ArrayList<GNode> gSensor = new ArrayList<>();
	protected ArrayList<GNode> gActuator = new ArrayList<>();
	protected ArrayList<GNode> gVariable = new ArrayList<>();
	protected ArrayList<GLink> gLiens = new ArrayList<>();
	protected ArrayList<GLabel> gLabels = new ArrayList<>();
	
	protected HashMap<String, GNode> gNodesHash = new HashMap<String, GNode>();
	
	public JToolBar toolBar;
	private double mPrevX =0;
	private double mPrevY =0;
	boolean rightClick = false;
	boolean leftClick = false;

	public JComponent selectedElement = null;
	
	boolean displayCustomColor = true;
	boolean mouseIn = false;
	Timer timer;
	public final static int INTERVAL = 200;
	long prevUpdateTime = 0;
	long prevUpdateTimer = 0;
	
	public NetworkPanel()
	{
		super();
		this.setSize(800, 600);
		toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.setRollover(true);
        toolBar.add(Box.createHorizontalGlue());
        MouseInputListener mouseListener = new MouseInputListener() {
        	@Override
			public void mouseMoved(MouseEvent e) {}			
			@Override
			public void mouseDragged(MouseEvent e) {
				if(leftClick && System.currentTimeMillis() - prevUpdateTime > 60)
				{
						prevUpdateTime = System.currentTimeMillis();
						offsetX +=(e.getX()-mPrevX)/zoomLevel;
						offsetY +=(e.getY()-mPrevY)/zoomLevel;
						mPrevX = e.getX();
						mPrevY = e.getY();
						panelAdress.repaint();
						panelAdress.revalidate();	
				}				
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e) || (SwingUtilities.isLeftMouseButton(e)&&e.isControlDown()))
					rightClick = false;
				else if(SwingUtilities.isLeftMouseButton(e))
					leftClick = false;
			}			
			@Override
			public void mousePressed(MouseEvent e) {
				mPrevX = e.getX();
				mPrevY = e.getY();		
				if(SwingUtilities.isRightMouseButton(e) || (SwingUtilities.isLeftMouseButton(e)&&e.isControlDown()))
					rightClick = true;
				else if(SwingUtilities.isLeftMouseButton(e))
					leftClick = true;
			}			
			@Override
			public void mouseExited(MouseEvent e) {
				mouseIn = false;
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				mouseIn = true;
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e) || (SwingUtilities.isLeftMouseButton(e)&&e.isControlDown()))
					displayGeneralPopup(e);
				else
					switchSelectedElement(null);
			}
		};
		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);
		
		this.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				zoomLevel += (e.getPreciseWheelRotation()*-(0.02 + (zoomLevel*0.1)));
				
				if(zoomLevel < 0.1)
					zoomLevel = 0.1;
				else if(zoomLevel > 3)
					zoomLevel = 3;
				panelAdress.repaint();
				panelAdress.revalidate();		
			}
		}); 
	}
	
	public NetworkPanel(EvoAgentMind inmind) {
		this();
		mind = inmind;
		GNode temp;
		int n = 0;
		int maxHor = 0;
		SkillModule currentSkill;
		ArrayList<String> skillNames = new ArrayList<>();
		HashSet<String> skillSet = new HashSet<>();
		ArrayList<Integer> levels = new ArrayList<>();
		skillNames.add(mind.getMasterSkill().getName());
		levels.add(0);
		skillSet.add(mind.getMasterSkill().getName());
		for(int i = 0; i < skillNames.size(); i++)
		{
			currentSkill = mind.getSkill(skillNames.get(i));
			if(i > 0 && levels.get(i) != levels.get(i-1))
				n = 0;
			temp = new GNode(this, n * neuronSpacing,levels.get(i) * layerSpacing,neuronSize,neuronSize, currentSkill);
			gNodes.add(temp);
			gNodesHash.put(currentSkill.getName(), temp);
			for(String s :currentSkill.getSkillOutputNames())
			{
				if(!skillSet.contains(s))
				{
					skillSet.add(s);
					skillNames.add(s);
					levels.add(levels.get(i) + 1);									
				}
			}
			n++;
			if(n > maxHor)
				maxHor = n;
		}
		for(GNode nd : gNodes)
		{
			for(String s : nd.getSkillModule().getSkillOutputNames())
			{
				//System.out.println("make link");
				makeGLink(nd, gNodesHash.get(s), 0);		
			}
		}
		int vertical = 0;
		for(String s : mind.getSensorsNames())
		{
			if(mind.getSensor(s).inUse)
			{
				gSensor.add(new GNode(this, -2 * neuronSpacing,vertical * moduleSpacing,neuronSize,neuronSize, s, Color.cyan));
				vertical++;				
			}
		}
		vertical = 0;
		for(String s : mind.getVariablesNames())
		{
			if(mind.getVariable(s).inUse)
			{
				gVariable.add(new GNode(this, -4 * neuronSpacing,vertical * moduleSpacing,neuronSize,neuronSize, s, Color.yellow));
				vertical++;
			}
		}
		vertical = 0;
		for(String s : mind.getActuatorsNames())
		{
			if(mind.getActuator(s).inUse)
			{
				gActuator.add(new GNode(this, maxHor * neuronSpacing,vertical * moduleSpacing,neuronSize,neuronSize, s, Color.red));
				vertical++;
			}
		}
		
		displayGStructure();
	       timer = new Timer(INTERVAL, new ActionListener() {
	           public void actionPerformed(ActionEvent evt) {
	        	    updateInfluenceDisplay();    
	        	    updateModulesDisplay();
	        	    panelAdress.repaint();
	           }
	           });
	           timer.start();
	}
	
	public NetworkPanel(EvoAgentMind inmind, NetworkPanel old) {
		this(inmind);
		if (old != null)
		{
			for(GNode act : old.gActuator)
			{
				for(GNode thisact : gActuator)
				{
					if (thisact.getName().equals(act.getName()))
					{
						thisact.X = act.X;
						thisact.Y = act.Y;
						break;
					}
				}
			}
			for(GNode act : old.gSensor)
			{
				for(GNode thisact : gSensor)
				{
					if (thisact.getName().equals(act.getName()))
					{
						thisact.X = act.X;
						thisact.Y = act.Y;
						break;
					}
				}
			}
			for(GNode act : old.gVariable)
			{
				for(GNode thisact : gVariable)
				{
					if (thisact.getName().equals(act.getName()))
					{
						thisact.X = act.X;
						thisact.Y = act.Y;
						break;
					}
				}
			}
			for(GNode act : old.gNodes)
			{
				for(GNode thisact : gNodes)
				{
					if (thisact.getName().equals(act.getName()))
					{
						thisact.X = act.X;
						thisact.Y = act.Y;
						break;
					}
				}
			}

			offsetX = old.offsetX;
			offsetY = old.offsetY;
			zoomLevel = old.zoomLevel;
		}
	}
	
	protected void updateInfluenceDisplay() {
		for(SkillModule sk : mind.getSkills())
		{
			GNode node = gNodesHash.get(sk.getName());
			if(node != null)
			{
				node.setValue(sk.getPrevInfluence());
				for(String to : sk.getSkillOutputNames())
				{
					//System.out.println(node.getLink(to));
					node.getLink(to).absWeight = sk.getSkillOutputValueForDisplay(to);
					node.getLink(to).relWeight = sk.getFunctionOutputValueForDisplay(to);
				}				
			}
		}
	}

	protected void updateModulesDisplay() {
		for(GNode nd : gSensor)
			nd.setValue(mind.getSensor(nd.getName()).getValue());
		for(GNode nd : gVariable)
			nd.setValue(mind.getVariable(nd.getName()).getValue());
		for(GNode nd : gActuator)
			nd.setValue(mind.getActuatorValue(nd.getName()));		
	}

	protected void switchSelectedElement(JComponent object) {
		selectedElement = object;
		gLabels.clear();
		this.removeAll();
		if(selectedElement!=null)
		{
			if(selectedElement.getClass().equals(GNode.class))
			{
				//GNode cog = (GNode) selectedElement;
				// just like do stuff 
			}		
		}
		displayGStructure();
		this.repaint();
		this.revalidate();
	}

	public void makeGLink(GNode from, GNode to, double Val)
	{
		//System.out.println("link : " + from.getName() + " --> " + to.getName());
		GLink temp = new GLink(this,from,to,Val);
		gLiens.add(temp);
		from.addLink(to.getName(), temp);
	}
	
	public void displayGStructure()
	{
		int i = this.getComponentCount();
		for(GLabel l : gLabels)
		{
			this.add(l);
			this.setComponentZOrder(l, i);
			i++;			
		}
		for(GNode gc : gNodes)
		{
			this.add(gc);
			this.setComponentZOrder(gc, i);
			i++;
		}
		for(GNode gc : gSensor)
		{
			this.add(gc);
			this.setComponentZOrder(gc, i);
			i++;
		}
		for(GNode gc : gVariable)
		{
			this.add(gc);
			this.setComponentZOrder(gc, i);
			i++;
		}
		for(GNode gc : gActuator)
		{
			this.add(gc);
			this.setComponentZOrder(gc, i);
			i++;
		}
		for(GLink l : gLiens)
		{
			this.add(l);
			this.setComponentZOrder(l,i);
			i++;
		}			
	}

	public int getRelativeXPos(int pos)
	{
		return (int)((pos+offsetX)*zoomLevel);
	}

	public int getRelativeYPos(int pos)
	{
		return (int)((pos+offsetY)*zoomLevel);
	}

	public void updateDisplayedElements() {
	}

	public void displayGeneralPopup(MouseEvent e){
		
	}
}
