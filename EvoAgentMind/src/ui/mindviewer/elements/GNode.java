/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package ui.mindviewer.elements;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;

import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;

import evoagentmindelements.modules.SkillModule;


@SuppressWarnings("serial")
public class GNode extends JComponent{
	
	SkillModule module;
	private NetworkPanel panelAdress;
	private Boolean DisplayCustomColor;
	float margeEcriture = 15;
	Font cognitonFont;
	double sizeX,sizeY;
	int X,Y;
	int groupIndex = -1;
	
	String customMessage = "";
	int clickX,clickY;
	protected HashMap<String, GLink> gLinkHash = new HashMap<String, GLink>();
	public double value=0.0;
	DecimalFormat decimalFormat = new DecimalFormat("0.000");
	Color typeColor = Color.lightGray;
	
	public GNode(NetworkPanel parent , double x, double y, double w, double h) {
		super();
		decimalFormat.setRoundingMode(RoundingMode.UP);
		X = (int) x;
		Y = (int) y;
		panelAdress = parent;
		MouseInputListener mouseListener = new MouseInputListener() {
			@Override
			public void mouseMoved(MouseEvent e) {}
			@Override
			public void mouseDragged(MouseEvent e) {
				if(panelAdress.mouseIn && System.currentTimeMillis() - panelAdress.prevUpdateTime > 10)
				{
					panelAdress.prevUpdateTime = System.currentTimeMillis();
					X+=((e.getXOnScreen()- clickX)/panelAdress.zoomLevel);
					Y+=((e.getYOnScreen()- clickY)/panelAdress.zoomLevel);	
					clickX = e.getXOnScreen();
					clickY = e.getYOnScreen();
					panelAdress.repaint();
					//panelAdress.revalidate();
				}
			}
			@Override
			public void mouseClicked(MouseEvent e) {
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				panelAdress.mouseIn = true;
			}
			@Override
			public void mouseExited(MouseEvent e) {
				panelAdress.mouseIn = false;
			}
			@Override
			public void mousePressed(MouseEvent e) {
				clickX = e.getXOnScreen();
				clickY = e.getYOnScreen();
				panelAdress.setComponentZOrder(e.getComponent(),0);
			}
			@Override
			public void mouseReleased(MouseEvent e) {	
				//panelAdress.repaint();
				//panelAdress.revalidate();			
			}
		};
		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);
		cognitonFont = new Font("default", Font.BOLD, 15);
		this.DisplayCustomColor = true;
		this.setVisible(true);
	}
	
	
	public GNode(NetworkPanel parent , double x, double y, double w, double h, SkillModule mod) {
		this(parent,x,y,w,h);
		setName(mod.getName());
		module = mod;
	}
	
	public GNode(NetworkPanel parent , double x, double y, double w, double h, SkillModule mod,String message) {
		this(parent,x,y,w,h,mod);
		customMessage = message;
	}

	public GNode(NetworkPanel parent, double x, double y, double w, double h, String nam) {
		this(parent,x,y,w,h);
		setName(nam);
	}

	public GNode(NetworkPanel parent, double x, double y, double w, double h, String nam, Color col) {
		this(parent,x,y,w,h,nam);
		typeColor = col;
	}


	@Override
	public void paintComponent(Graphics g) 
    {    
		int height,width;
		String strValue = decimalFormat.format(value);
        Graphics2D g2d = (Graphics2D) g.create();
        String displayedString = getName();
        g2d.scale(panelAdress.zoomLevel, panelAdress.zoomLevel);
        g2d.setFont(cognitonFont);
    	FontMetrics fm = g2d.getFontMetrics();
    	height = 4*fm.getHeight();
    	width = Math.max(Math.max(fm.stringWidth(displayedString),fm.stringWidth(customMessage)), fm.stringWidth(strValue));
    	if(customMessage != "")
    		height = 6*fm.getHeight();
    	this.setBounds(panelAdress.getRelativeXPos((int)(X)),
    			panelAdress.getRelativeYPos((int)Y),
    			(int) ( (width+ (2*margeEcriture))*panelAdress.zoomLevel ),(int)(height*panelAdress.zoomLevel));
    	
    	g2d.fill(new Ellipse2D.Double(0 ,0,width + (2*margeEcriture),height));
    	sizeX = width + (2*margeEcriture);
    	sizeY = height;


		
//    	if(panelAdress.displayCustomColor)
//    		g2d.setColor(cogniton.getDisplayColor());
//    	else
//    	{
//    		if(cogniton.isCulturon)
//    			g2d.setColor(Color.yellow);    		
//    		else
    			g2d.setColor(typeColor.darker());    		    			
//    	}
    	g2d.fill(new Ellipse2D.Double(2,2,(int) (width + (2*margeEcriture) )-4,height-4));
    	//Ellipse2D meter = new Ellipse2D.Double(2,2,(int) (width + (2*margeEcriture) )-4,height-4);

		g2d.setColor(typeColor);   
    	//meter.getBounds2D().setRect(2,2,(int) (value * ((width + (2*margeEcriture) )-4)),height-4);
    	Area meter = new Area(new Ellipse2D.Double(2,2,(int) (width + (2*margeEcriture) )-4,height-4));

    	meter.subtract(new Area(new Rectangle((int) (2+(value * ((width + (2*margeEcriture) )-4))),2,(int) (width + (2*margeEcriture) )-4,height-4)));
    	g2d.fill(meter);
    	g2d.setColor(Color.BLACK);
    	if(customMessage == "")
    	{
    		g2d.drawString(displayedString, margeEcriture, (float) (fm.getHeight()*1.8));
    		g2d.drawString(strValue,-25 + margeEcriture + width/2 , (float) (fm.getHeight()*3.0));
    	}
    	else
    	{
    		g2d.drawString(displayedString, (int)((width + (2*margeEcriture))/2.0)-(fm.stringWidth(displayedString)/2), (float) (fm.getHeight()*1.8));    		
    		g2d.drawString(strValue, (int)((width + (2*margeEcriture))/2.0)-(fm.stringWidth(displayedString)/2), (float) (fm.getHeight()*3.0));    		
    		g2d.drawString(customMessage, (int)((width + (2*margeEcriture))/2.0)-(fm.stringWidth(customMessage)/2), (float) (fm.getHeight()*3.8));    		
    	}
    }

	public void setColor(Color c)
	{
		typeColor = c;
	}
	
	public void switchDisplayColor()
	{
		this.DisplayCustomColor = !this.DisplayCustomColor;
	}
	
	public void displayCustomColor()
	{
		this.DisplayCustomColor = true;
	}
	
	public void displayTypeColor()
	{
		this.DisplayCustomColor = false;
	}

	public int getCentreX() {
		return (int) (X + (sizeX/2));
	}

	public int getCentreY() {
		return (int) (Y + (sizeY/2));
	}
	
	public double getDistanceToEdge(double ang) // closer than it seems 
	{
		double ret;
		double cosT = Math.cos(ang);
		ret = Math.abs(((double)-(sizeX/2.0)) / cosT);
		if(Math.abs(((double)(sizeX/2.0)) / cosT)< ret)
			ret = Math.abs(((double)(sizeX/2.0)) / cosT);
		double sinT = Math.sin(ang);
		if(Math.abs(((double)(sizeY/2.0)) / sinT)< ret)
			ret = Math.abs(((double)(sizeY/2.0)) / sinT);
		if(Math.abs(((double)-(sizeY/2.0)) / sinT)< ret)
			ret = Math.abs(((double)-(sizeY/2.0)) / sinT);		
		return ret;
	}

	public SkillModule getSkillModule() {
		return module;
	}
	
	public void addLink(String to, GLink link)
	{
		gLinkHash.put(to, link);
	}
	
	public GLink getLink(String to)
	{
		//System.out.println(gLinkHash.size());
		return gLinkHash.get(to);
	}
	
	public void setValue(double val)
	{
		value = val;
	}
}
