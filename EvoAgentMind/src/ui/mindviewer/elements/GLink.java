/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package ui.mindviewer.elements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;

@SuppressWarnings("serial")
public class GLink extends JComponent {
	protected NetworkPanel panelAdress;
	public GNode from;
	public GNode to;
	public double poids;
	Color color;
	int[] arrowX = new int[3];
	int[] arrowY = new int[3];
	public double relWeight;
	public double absWeight;

	public GLink(NetworkPanel parent, GNode from, GNode to, double poids) {
		super();
		panelAdress = parent;
		this.from = from;
		this.to = to;
		this.poids = poids;
		this.setFocusable(false);
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();

		color = new Color(0.2f,(float) (0.2f+(0.8*Math.min(absWeight, 1.0))), 0.2f);
		g2d.setStroke(new BasicStroke((int)(10 * panelAdress.zoomLevel))); 
		this.setBounds(0, 0, 5000, 5000);
		g2d.setColor(color);
		g2d.drawLine((int) (panelAdress.getRelativeXPos(from.getCentreX())),
				(int) (panelAdress.getRelativeYPos(from.getCentreY())),
				(int) (panelAdress.getRelativeXPos(to.getCentreX())),
				(int) panelAdress.getRelativeYPos(to.getCentreY()));
		

		g2d.setStroke(new BasicStroke((int)(4 * panelAdress.zoomLevel))); 
		color = new Color(0.2f,(float) (0.2f+(0.8*Math.min(relWeight, 1.0))), 0.2f);	
		g2d.setColor(color);
		g2d.drawLine((int) (panelAdress.getRelativeXPos(from.getCentreX())),
				(int) (panelAdress.getRelativeYPos(from.getCentreY())),
				(int) (panelAdress.getRelativeXPos(to.getCentreX())),
				(int) panelAdress.getRelativeYPos(to.getCentreY()));
		
	
		double ang = Math.atan2(from.getCentreY() - to.getCentreY(), from.getCentreX() - to.getCentreX());
		
		arrowX[0] = (int) (panelAdress.getRelativeXPos(to.getCentreX()) + (to.getDistanceToEdge(ang) * Math.cos(ang)* panelAdress.zoomLevel));
		arrowY[0] =(int) (panelAdress.getRelativeYPos(to.getCentreY()) + (to.getDistanceToEdge(ang) * Math.sin(ang)* panelAdress.zoomLevel));

		arrowX[1] = (int) ((arrowX[0]) + ((15 * Math.cos(ang + 0.30))* panelAdress.zoomLevel));
		arrowY[1] =(int) ((arrowY[0]) + ((15 * Math.sin(ang + 0.30))* panelAdress.zoomLevel));

		arrowX[2] = (int) ((arrowX[0]) + ((15 * Math.cos(ang - 0.30))* panelAdress.zoomLevel));
		arrowY[2] = (int) ((arrowY[0]) + ((15 * Math.sin(ang - 0.30))* panelAdress.zoomLevel));

		g2d.drawPolygon(arrowX, arrowY, 3);
	}
}
