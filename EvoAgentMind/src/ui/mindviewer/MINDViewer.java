/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package ui.mindviewer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import evoagentmindelements.EvoAgentMind;
import ui.mindviewer.elements.NetworkPanel;


@SuppressWarnings("serial")
public class MINDViewer extends JPanel{
	
	NetworkPanel netPanel;	
	JScrollPane consoleScrollPane;
	JPanel mainPanel;
	JMenuItem MI_GEN_OPEN = new JMenuItem("Open");
	JMenuItem MI_HLP_ABOUT = new JMenuItem("About");
	
	JMenu selMenu;
	JMenu viewMenu;
	
	public JMenuBar mb;
	JToolBar toolbar = new JToolBar(JToolBar.HORIZONTAL);
	
	EvoAgentMind mind = null;
	ArrayList<ArrayList<EvoAgentMind>> minds = null;
	JComboBox<EvoAgentMind> mindBox = null;
	
	public MINDViewer()
	{
		mb = new JMenuBar();
		this.setLayout(new BorderLayout());
		MFListener listener = new MFListener();
		JMenu gen = new JMenu("General");
		mb.add(gen);
		MI_GEN_OPEN.addActionListener(listener);
		gen.add(MI_GEN_OPEN);
		JMenu hlp = new JMenu("Help");
		mb.add(hlp);
		MI_HLP_ABOUT.addActionListener(listener);
		hlp.add(MI_HLP_ABOUT);
	}
	
	public MINDViewer(EvoAgentMind mind) {
		this();
		setMind(mind);
	}

	public MINDViewer(ArrayList<ArrayList<EvoAgentMind>> minds) {
		this();
		setMinds(minds);
		if(minds!=null)
		{
			EvoAgentMind [] mindArr = new  EvoAgentMind[minds.get(0).size()];
			minds.get(0).toArray(mindArr);
			mindBox = new JComboBox<EvoAgentMind>(new DefaultComboBoxModel<EvoAgentMind>(mindArr));
			mindBox.setModel(new DefaultComboBoxModel<EvoAgentMind>(mindArr));
			mindBox.addActionListener(new ActionListener() {	
				@Override
				public void actionPerformed(ActionEvent e) {
					mind = (EvoAgentMind) mindBox.getSelectedItem();
					displayMIND();
				}
			});
			toolbar.add(mindBox);
			toolbar.setFloatable(false);
			this.add(toolbar,BorderLayout.NORTH);
		}
	}

	public void displayPanel(NetworkPanel p)
	{
		this.removeAll();
		this.add(p,BorderLayout.CENTER);
		if(toolbar!=null)
			this.add(toolbar,BorderLayout.NORTH);
		this.repaint();this.revalidate();
	}
	
	public void displayMIND()
	{
		if(mind != null)
		{
			netPanel = new NetworkPanel(mind, netPanel);
			displayPanel(netPanel);
		}
	}

	public void switchCogScheme(String schemeName) {
		netPanel.offsetX = 0;
		netPanel.offsetY = 0;
		selectMenuItem(selMenu,schemeName);
	}
		
	private void selectMenuItem(JMenu Menu, String schemeName) {
		for(int i = 0 ; i < Menu.getItemCount(); i++)
		{
			if(Menu.getItem(i) != null)
			{
				if (Menu.getItem(i).getText().toLowerCase().equals(schemeName.toLowerCase()))
					Menu.getItem(i).setSelected(true);
				else
					Menu.getItem(i).setSelected(false);				
			}
		}
	}

	// classes
	private class MFListener implements ActionListener {
		public MFListener() {}
		@Override
		public void actionPerformed(ActionEvent e) {
			JMenuItem source = (JMenuItem) e.getSource();

			if (source.equals(MI_GEN_OPEN)) {

			}else if(source.equals(MI_HLP_ABOUT)){
				JLabel lab1 = new JLabel("stuff");
				JLabel lab2 = new JLabel("Copyright (C) 2019  SURO François");
				JLabel lab3 = new JLabel("GNU GENERAL PUBLIC LICENSE");
				JLabel lab4 = new JLabel("Version 3, 29 June 2007");
				JLabel lab5 = new JLabel("");
				JLabel lab6 = new JLabel("This program comes with ABSOLUTELY NO WARRANTY");
				JLabel lab7 = new JLabel("This is free software, and you are welcome to redistribute it");
				JLabel lab8 = new JLabel("under certain conditions");
		
				Object[] array = {lab1,lab2,lab3,lab4,lab5,lab5,lab6,lab7,lab8};
				JOptionPane.showMessageDialog(netPanel, array,"About",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	public void setMind(EvoAgentMind md) {
		mind = md;
	}

	public void setMinds(ArrayList<ArrayList<EvoAgentMind>> mds) {
		minds = mds;
		mind = mds.get(0).get(0);
	}
}
