/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.grovepilibextends;

import java.io.IOException;

import org.iot.raspberry.grovepi.GrovePi;
import org.iot.raspberry.grovepi.GroveUtil;
import static org.iot.raspberry.grovepi.GrovePiCommands.*;

public class MYGroveUltraSonicRanger {

	  private final GrovePi grovePi;
	  private final int pin;

	  public MYGroveUltraSonicRanger(GrovePi grovePi, int pin) {
	    this.grovePi = grovePi;
	    this.pin = pin;
	  }

	  public double get() throws IOException {
	    return grovePi.exec((io) -> {
	      io.write(uRead_cmd, pin, unused, unused);
	      //io.sleep(2);
	      int[] v = GroveUtil.unsign(io.read(new byte[4]));
	      return (double) ((v[1] * 256) + v[2]);
	    });
	  }
}
