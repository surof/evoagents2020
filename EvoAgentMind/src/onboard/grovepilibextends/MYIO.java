/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.grovepilibextends;
import java.io.IOException;

import org.iot.raspberry.grovepi.GroveIO;

import com.pi4j.io.i2c.I2CDevice;

public class MYIO implements GroveIO {

		  private final I2CDevice device;

		  public MYIO(I2CDevice device) {
		    this.device = device;
		  }

		  @Override
		  public void write(int... command) throws IOException {
		    byte[] buffer = new byte[command.length];
		    for (int i = 0; i < command.length; i++) {
		      buffer[i] = (byte) command[i];
		    }
		    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO write]{0}", Arrays.toString(buffer));
		    device.write(buffer, 0, command.length);
		  }
		  
		  public void write(byte[] command) throws IOException {
		   
		    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO write]{0}", Arrays.toString(buffer));
		    device.write(command, 0, command.length);
		  }

		  @Override
		  public int read() throws IOException {
		    final int read = device.read();
		    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO read]{0}", read);
		    return read;
		  }

		  public byte[] read(int addr, byte[] buffer) throws IOException {
			    device.read(addr,buffer, 0, buffer.length);
			    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO read]{0}", Arrays.toString(buffer));
			    return buffer;
			  }
		  
		  public byte[] read(int addr, byte[] buffer, int len) throws IOException {
			    device.read(addr,buffer, 0, len);
			    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO read]{0}", Arrays.toString(buffer));
			    return buffer;
			  }
		  
		  public byte[] readWriteAll(byte[] writeBuffer, byte[] readBuffer) throws IOException {
			    device.read(writeBuffer,0,writeBuffer.length, readBuffer, 0,readBuffer.length);
//			 	read(byte[] writeBuffer, int writeOffset, int writeSize, byte[] readBuffer, int readOffset, int readSize)

			    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO read]{0}", Arrays.toString(buffer));
			    return readBuffer;
			  }
		  
		  public byte[] readAll( byte[] readBuffer) throws IOException {
			    device.read(readBuffer, 0,readBuffer.length);
//			 	read(byte[] writeBuffer, int writeOffset, int writeSize, byte[] readBuffer, int readOffset, int readSize)

			    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO read]{0}", Arrays.toString(buffer));
			    return readBuffer;
			  }
		  public void writeAll(byte[] writeBuffer) throws IOException {
			    device.write(writeBuffer,0,writeBuffer.length);
//			 	read(byte[] writeBuffer, int writeOffset, int writeSize, byte[] readBuffer, int readOffset, int readSize)

			    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO read]{0}", Arrays.toString(buffer));
			  }

		@Override
		public byte[] read(byte[] buffer) throws IOException {
		    device.read(buffer, 0, buffer.length);
		    //Logger.getLogger("GrovePi").log(Level.INFO, "[Pi4J IO read]{0}", Arrays.toString(buffer));
		    return buffer;
		  }
		}