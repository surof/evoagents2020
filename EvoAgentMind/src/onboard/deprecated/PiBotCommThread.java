/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.deprecated;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.HashMap;

import actuators.Actuator;
import actuators.Claw;
import actuators.Electromagnet;
import actuators.Motor;
import bot.Pinkie;
import sensors.Button;
import sensors.DOFPreCompute;
import sensors.MagneticSensor;
import sensors.NineDOFSensor;
import sensors.ObjectDetector;
import sensors.Sensor;
import sensors.UltraSonicSensor;
import sensors.virtualSensor;

// original remote control system
// will port it back to EATask format ... maybe

public class PiBotCommThread implements Runnable{
	protected Socket socket;
	protected DatagramSocket UDPSocket;
	protected ControlFlags flags;
	protected Pinkie bot;
	protected BufferedReader clientIn= null;
	protected PrintWriter clientOut= null;
	private boolean isConnected = false;
	ByteBuffer UDPBuffer ;
	InetAddress adress;

	ArrayList<PhysicalSensor> sensors = new ArrayList<>();
	static HashMap<String,PhysicalActuator> actuators = new HashMap<String,PhysicalActuator>();
	
	private long timer = 0;

	public PiBotCommThread(Socket s, ControlFlags f)
	{
		socket = s;
		flags = f;
	}

	@Override
	public void run() {
		String controlToken;
		init();
		if(isConnected)
		{
			try {
				controlToken = clientIn.readLine();
				if(controlToken == null)
					quitTask();
				else if(controlToken.equals(PiBotCommDefines.mode_TCP))
					runTCP();
				else if(controlToken.equals(PiBotCommDefines.mode_UDP))
					runUDP();
				else					
					quitTask();

			}catch (InterruptedIOException iioe)
			{System.err.println ("Remote host timed out during read operation");					
			quitProgram();}
			catch (IOException ioe)
			{System.err.println ("Network I/O error - " + ioe);
			quitProgram();}
		}
		quitTask();
	}

	private void quitProgram() {
		quitTask();
		flags.closePiBotControl();
	}

	private void quitTask() {
		
		for(PhysicalSensor s : sensors)
			s.shutdown();
		try {
			bot.shutdown();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		flags.setThreadInactive();
	}

	private void runUDP() {
		System.out.println("UDP mode");
		try {
			String controlToken;
			String UDPMsg;
			adress = socket.getInetAddress();
			InetSocketAddress UDPAdress = new InetSocketAddress (adress.getHostAddress(),PiBotCommDefines.portUDP);
			DatagramChannel serverChannel = DatagramChannel.open();
			Selector selector = Selector.open();
			UDPSocket = serverChannel.socket();
			UDPSocket.setReuseAddress(true);
			UDPSocket.bind(new InetSocketAddress (PiBotCommDefines.portUDP));
			serverChannel.configureBlocking (false);
			serverChannel.register(selector, SelectionKey.OP_READ|SelectionKey.OP_WRITE);
			serverChannel.connect(new InetSocketAddress(adress.getHostAddress(), PiBotCommDefines.portUDP));    
			while(flags.doContinue() && flags.hasActiveThread())
			{
				bot.updateActuators();
				if(timer - System.currentTimeMillis() > 100)
				{
					bot.updateSensors();
					UDPMsg = new String(PiBotCommDefines.tag_sensor + "\n");
					for(PhysicalSensor s : sensors)
						UDPMsg+=s.getName() + PiBotCommDefines.commSeparator+ s.get() + "\n";
					UDPMsg+=PiBotCommDefines.tag_endmsg + "\n";
					UDPBuffer =  ByteBuffer.allocate(8192);
					UDPBuffer.put(UDPMsg.getBytes());
					UDPBuffer.flip();
					try{
						serverChannel.send(UDPBuffer,UDPAdress);
					}catch (Exception e) {
						System.out.println("cant send udp");
					}
					timer = System.currentTimeMillis();
				}
				UDPBuffer = ByteBuffer.allocate(8192);
				if(serverChannel.receive(UDPBuffer)!=null);
				{
					BufferedReader br = new BufferedReader(new StringReader(new String(UDPBuffer.array())));
					controlToken = br.readLine();
					if(controlToken == null || controlToken.equals(PiBotCommDefines.ctrl_quit))
					{
						System.out.println("received quit");
						flags.setThreadInactive();
						flags.closePiBotControl();
					}					
					else if(controlToken.equals(PiBotCommDefines.ctrl_do))
						getActuatorValue(br);
					else if(controlToken.equals(PiBotCommDefines.ctrl_start))
						bot.reset();
					else if(controlToken.equals(PiBotCommDefines.ctrl_stop)){
						System.out.println("received stop");
						flags.setThreadInactive();		
						}
				}
			}	
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void runTCP() throws IOException {
		System.out.println("TCP mode");
		String controlToken;
		while(flags.doContinue() && flags.hasActiveThread())
		{
			controlToken = clientIn.readLine();
			if(controlToken == null || controlToken.equals(PiBotCommDefines.ctrl_quit))
			{
				System.out.println("received quit");
				flags.setThreadInactive();
				flags.closePiBotControl();
			}					
			else if(controlToken.equals(PiBotCommDefines.ctrl_do))
			{
				getActuatorValue(clientIn);
				sendSensorValues();
				bot.updateActuators();
				bot.updateSensors();						
			}
			else if(controlToken.equals(PiBotCommDefines.ctrl_start))
			{
				bot.reset();
				bot.updateActuators();
				bot.updateSensors();
				sendSensorValues();
			}
			else if(controlToken.equals(PiBotCommDefines.ctrl_stop))
			{
				System.out.println("received stop");
				flags.setThreadInactive();
			}					
		}				
	}

	private void sendSensorValues() {
		clientOut.println(PiBotCommDefines.tag_sensor);
		for(PhysicalSensor s : sensors)
			clientOut.println(s.getName() + PiBotCommDefines.commSeparator+ s.get());
		clientOut.println(PiBotCommDefines.tag_endmsg);
		clientOut.flush();
	}

	protected static void getActuatorValue(BufferedReader src) {
		String line;

		try {
			while((line = src.readLine())!=null &&!(line).equals(PiBotCommDefines.tag_endmsg))
			{
				try {
				actuators.get(line.split(PiBotCommDefines.commSeparator)[0]).set(Double.parseDouble(line.split(PiBotCommDefines.commSeparator)[1]));
				}
				catch (Exception e) {
					
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void init()
	{
		bot = new Pinkie();
		readConfigFile(PiBotCommDefines.configFilePath);
		try {
			socket.setSoTimeout(10000);
		} catch (SocketException e1) {
			System.out.println("cant set socket timeout");
			e1.printStackTrace();
		}
		try {
			clientIn=new BufferedReader (new InputStreamReader (socket.getInputStream()));
			clientOut=new PrintWriter(socket.getOutputStream());
			isConnected = true;
		} catch (IOException e) {
			System.out.println("cant create read/write socket");
			e.printStackTrace();
		}
	}

	private void readConfigFile(String cfgFile) {
		File  f = new File(cfgFile);
		System.out.println("config file : " + cfgFile);
		if(f.exists())
		{
			InputStream ips = null;
			try {
				ips = new FileInputStream(f);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.out.println("file not found : " + cfgFile);
				return ;
			} 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne = "xx";
			try {
				while((ligne = br.readLine()) != null && !ligne.equals("SENSORS"));
				while((ligne = br.readLine()) != null && !ligne.equals("ACTUATORS"))
				{
					if(!ligne.startsWith("#"))
					{
						if(ligne.split(":")[1].equals("GroveUltrasonicRanger"))
							sensors.add(new UltraSonicSensor(ligne,bot.usa));
						else if(ligne.split(":")[1].equals("Grove9DOF"))
							sensors.add(new NineDOFSensor(ligne,bot.ninedof));
						else if(ligne.split(":")[1].equals("GroveMagneticSensor"))
							sensors.add(new MagneticSensor(ligne,bot.magneticSwitch));
						else if(ligne.split(":")[1].equals("DOFPreCompute"))
							sensors.add(new DOFPreCompute(ligne,bot.ninedof));
						else if(ligne.split(":")[1].equals("ObjectDetector"))
							sensors.add(new ObjectDetector(ligne,bot.servoControl));
						else if(ligne.split(":")[1].equals("Button"))
							sensors.add(new Button(ligne,bot.objectDetect));
						else if(ligne.split(":")[1].equals("virtualSensor"))
							sensors.add(new virtualSensor(ligne,0.0));
					}
				}			
				if(ligne.equals("ACTUATORS"))
				{
					while((ligne = br.readLine()) != null)
					{
						if(!ligne.startsWith("#"))
						{
							if(ligne.split(":")[1].equals("MotorControl"))
								actuators.put(ligne.split(":")[0],new Motor(ligne,bot.mcCard));
							else if(ligne.split(":")[1].equals("ServoClaw"))
								actuators.put(ligne.split(":")[0],new Claw(ligne,bot.servoControl));
						}
					}			
				}	
				br.close();

			}catch (Exception e) {
			}
			System.out.println("created : "+sensors.size() + " sensors and " + actuators.size() + " actuators");
		}
		else
			System.out.println("not found !");
	}
}
