/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.deprecated;


public class PiBotCommDefines {

	public static int port = 28001;	
	public static int portUDP = 28002;	
	
	public static String mode_TCP = "MODE_TCP";
	public static String mode_UDP = "MODE_UDP";
	public static String ctrl_quit = "CTRL_QUIT";
	public static String ctrl_do = "CTRL_DO";
	public static String ctrl_start = "CTRL_START";
	public static String ctrl_stop = "CTRL_STOP";
	public static String configFilePath = "ressources/Pibot.botdesc";
	public static String tag_sensor = "SENSOR";
	public static String tag_endmsg = "ENDMSG";
	public static String commSeparator = ":";

}
