/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.deprecated;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.opencv.core.Core;

public class PiBotControl {

	static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
	static ControlFlags flags = new ControlFlags();

	
	public static void main(String[] args) {
		if(args.length > 1 && args[0].equals("-local"))
		{
			runLocalMind(args[1]);
		}
		else
		{
			networkListeningLoop();			
		}
	}
	
	private static void runLocalMind(String string) {
		PiBotLocalMind lm = new PiBotLocalMind(string);
		lm.run();
	}

	private static void networkListeningLoop()
	{
		Thread activeThread;
		System.out.println("listening on : " + PiBotCommDefines.port);
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(PiBotCommDefines.port);
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port", e);
		}

		while (flags.doContinue()) {
			Socket clientSocket = null;
			try {
				clientSocket = serverSocket.accept();
				
				System.out.println("new client");
			} catch (IOException e) {
				if (!flags.doContinue()) {
					System.out.println("Server Stopped.");
					return;
				}
				throw new RuntimeException("Error accepting client connection",
						e);
			}
			if(!flags.hasActiveThread())
			{
				activeThread = new Thread(new PiBotCommThread(clientSocket, flags));
				flags.setThreadActive();
				activeThread.start();				
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
