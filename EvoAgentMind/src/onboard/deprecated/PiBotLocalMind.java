/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.deprecated;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import actuators.Actuator;
import actuators.Claw;
import actuators.Electromagnet;
import actuators.Motor;
import bot.Pinkie;
import onboardMind.evoagentmindelements.EvoAgentMind;
import onboardMind.evoagentmindelements.EvoAgentMindFactory;
import onboardMind.evoagentmindelements.EvoAgentMindTemplate;
import sensors.Button;
import sensors.DOFPreCompute;
import sensors.MagneticSensor;
import sensors.NineDOFSensor;
import sensors.ObjectDetector;
import sensors.Sensor;
import sensors.UltraSonicSensor;
import sensors.virtualSensor;

public class PiBotLocalMind {

	private Pinkie bot;
	ArrayList<PhysicalSensor> sensors = new ArrayList<>();
	static HashMap<String,PhysicalActuator> actuators = new HashMap<String,PhysicalActuator>();
	String pathToMindFile;
	EvoAgentMind mind;
	EvoAgentMindTemplate mindTemplate = null;
	String AgentRootFolder,botname, masterSkill;

	public PiBotLocalMind(String pth) {
		initBot();
		pathToMindFile = pth;
		File  f = new File(pth);
		if(f.exists())
		{
			InputStream ips = null;
			try {
				ips = new FileInputStream(f);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return ;
			} 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			try {
				AgentRootFolder= br.readLine();
				botname= br.readLine();
				masterSkill= br.readLine();
			}catch (Exception e) {
				// TODO: handle exception
			}
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		mindTemplate = EvoAgentMindFactory.loadMindTemplate(AgentRootFolder,botname, masterSkill);
		mind = EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
	}

	public void run() {
		mind.reset();
		HashMap<String,Double> mindSV = new HashMap<>();
		HashMap<String,Double> mindAV = null;
		while(true)
		{
			bot.updateSensors();
			for(PhysicalSensor s : sensors)
				mindSV.put(s.getName(), s.get());
			mindAV = mind.doStep(mindSV);
			for(String a : actuators.keySet())
				actuators.get(a).set(mindAV.get(a));
			bot.updateActuators();
		}		
	}

	private void initBot()
	{
		bot = new Pinkie();
		readConfigFile(PiBotCommDefines.configFilePath);
	}

	private void readConfigFile(String cfgFile) {
		File  f = new File(cfgFile);
		System.out.println("config file : " + cfgFile);
		if(f.exists())
		{
			InputStream ips = null;
			try {
				ips = new FileInputStream(f);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.out.println("file not found : " + cfgFile);
				return ;
			} 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne = "xx";
			try {
				while((ligne = br.readLine()) != null && !ligne.equals("SENSORS"));
				while((ligne = br.readLine()) != null && !ligne.equals("ACTUATORS"))
				{
					if(!ligne.startsWith("#"))
					{
						if(ligne.split(":")[1].equals("GroveUltrasonicRanger"))
							sensors.add(new UltraSonicSensor(ligne,bot.usa));
						else if(ligne.split(":")[1].equals("Grove9DOF"))
							sensors.add(new NineDOFSensor(ligne,bot.ninedof));
						else if(ligne.split(":")[1].equals("GroveMagneticSensor"))
							sensors.add(new MagneticSensor(ligne,bot.magneticSwitch));
						else if(ligne.split(":")[1].equals("DOFPreCompute"))
							sensors.add(new DOFPreCompute(ligne,bot.ninedof));
						else if(ligne.split(":")[1].equals("ObjectDetector"))
							sensors.add(new ObjectDetector(ligne,bot.servoControl));
						else if(ligne.split(":")[1].equals("Button"))
							sensors.add(new Button(ligne,bot.objectDetect));
						else if(ligne.split(":")[1].equals("virtualSensor"))
							sensors.add(new virtualSensor(ligne,0.0));
					}
				}			
				if(ligne.equals("ACTUATORS"))
				{
					while((ligne = br.readLine()) != null)
					{
						if(!ligne.startsWith("#"))
						{
							if(ligne.split(":")[1].equals("MotorControl"))
								actuators.put(ligne.split(":")[0],new Motor(ligne,bot.mcCard));
							else if(ligne.split(":")[1].equals("ServoClaw"))
								actuators.put(ligne.split(":")[0],new Claw(ligne,bot.servoControl));
						}
					}			
				}	
				br.close();

			}catch (Exception e) {
			}
			System.out.println("created : "+sensors.size() + " sensors and " + actuators.size() + " actuators");
		}
		else
			System.out.println("not found !");
	}
	
}
