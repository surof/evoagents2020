/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.actuators;

import onboard.bot.drivers.actuators.MaestroSSC;

public class Claw extends PhysicalActuator{
	MaestroSSC servoCard;
	int channelRight = 1;
	int channelLeft = 3;
	
	/// min : 592 - max : 2352
	int openRight = 2200;
	int openLeft = 592;
	int closeRight = 592;
	int closeLeft = 2200;
	
	
	public Claw(String in, MaestroSSC servCont) {
		super(in);
		servoCard = servCont;
	}
	
	public void step()
	{
		
		if(normalizedValue > 0.5)
		{
			servoCard.setTarget(channelRight, closeRight);
			servoCard.setTarget(channelLeft, closeLeft);
		}
		else 
		{
			servoCard.setTarget(channelRight, openRight);
			servoCard.setTarget(channelLeft, openLeft);
		}			
	}
}
