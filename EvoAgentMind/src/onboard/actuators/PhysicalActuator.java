/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.actuators;

import evoagentmindelements.modules.ActuatorModule;

public class PhysicalActuator {
	String name; 
	int port; 
	double min; 
	double max; 
	String type;
	String param;
	public double normalizedValue = 0.0;
	ActuatorModule actuator=null;
	
	public PhysicalActuator(String Name, int Port, int Min, int Max, String Type, String Param)
	{
		name = Name;
		port = Port;
		min = Min;
		max = Max;
		type = Type;
		param = Param;
	}

	public PhysicalActuator(String in)
	{
		String[] tab = in.split(":");
		name = tab[0];
		type = tab[1];
		if(tab.length>2)
			port = Integer.parseInt(tab[2]);
		if(tab.length>3)
			min = Double.parseDouble(tab[3]);
		if(tab.length>4)
			max = Double.parseDouble(tab[4]);
		if(tab.length>5)
			param = tab[5];
	}
	
	public String makeString()
	{
		return name+":"+port+":"+min+":"+max+":"+type+":"+param;
	}
	
	public void set(double command)
	{
		
	}
	
	public void setNormalizedValue(double parseDouble) {
		normalizedValue = parseDouble;		
	}

	public void step() {
		//Override me
	}

	public void reset() {
	}

	public void autoStep() {
		if(actuator!=null)
		{
			normalizedValue = actuator.getMotorValue();
			step();			
		}
	}

	public void setMindModule(ActuatorModule actuator) {
		this.actuator = actuator;
	}	
}
