/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.sensors;

import onboard.bot.drivers.sensors.MagneticSwitchCard;

public class MagneticSensor extends PhysicalSensor{
	MagneticSwitchCard magneticSwitch = null;
	
	public MagneticSensor(String in, MagneticSwitchCard ms) {
		super(in);
		magneticSwitch = ms;
	}

	@Override
	public double getValue() {
		if(magneticSwitch.getValue())
			return 1.0;
		else
			return 0.0;
	}
	
	public double getNormalizedValue()
	{
		return (normalizedValue = getValue());
	}
}
