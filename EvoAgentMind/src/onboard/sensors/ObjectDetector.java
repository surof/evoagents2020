/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.sensors;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import onboard.bot.drivers.actuators.MaestroSSC;
import onboard.bot.drivers.sensors.ObjectRecognition;


public class ObjectDetector extends PhysicalSensor{
	class ObjectDetectorInternal
	{
		ObjectRecognition or;
		MaestroSSC ms;
		int panChannel = 5;
		int panMin = 560;
		int panMax = 2352;
		int panCenter = 1390;
		int panPos = 1345;
		int panRange = panMax - panMin;
		int tiltChannel = 4;
		int tiltMin = 1600;
		int tiltMax = 2352;
		int tiltRange = tiltMax - tiltMin;
		int tiltCenter = 2200;
		int tiltPos = 2200;
		double azimuth = 0.5;
		double distance = 0.5;
		int searchPositive = 1;
		// a timer for acquiring the video stream
		private ScheduledExecutorService timer;
		private Thread updThread;
		private boolean runThread = true;
		
		public ObjectDetectorInternal(MaestroSSC inms) {
			or = new ObjectRecognition();
			ms = inms;
			start();
		}
		
		public void start()
		{
			System.out.println("start");
			or.startCamera();
			ms.setTarget(panChannel, panCenter);
			ms.setTarget(tiltChannel, tiltCenter);
			// grab a frame every 33 ms (30 frames/sec)
			runThread = true;
			Runnable frameGrabber = new Runnable() {
				
				@Override
				public void run()
				{
					while(runThread)
						update();
				}
			};
			updThread = new Thread(frameGrabber);
			updThread.start();
			
		}

		public void update()
		{
			or.detect();
			if(or.hasDetected())
			{
				
				panPos = Math.min(panMax, Math.max(panMin, (int) (panPos-(or.getBiasedYOffset()*50))));
				ms.setTarget(panChannel, panPos);
				tiltPos = Math.min(tiltMax, Math.max(tiltMin, (int) (tiltPos-(or.getBiasedXOffset()*30))));
				ms.setTarget(tiltChannel, tiltPos);
				azimuth = Math.min(1.0, Math.max(0, 0.5 + (((double)(panPos - panCenter))/((double)panRange))));
				distance =  Math.min(1.0, Math.max(0, 0 + (((double)(tiltMax - tiltPos))/((double)(tiltRange * 0.7)))));		
				if(or.getNormalizedYOffset()>0)
					searchPositive = 1;
				else
					searchPositive = -1;	
			}
			else
			{
				searchLoop();
				azimuth = 0.5;
				distance = 0.5;
			}
		}

		private void searchLoop() {
			tiltPos = tiltCenter;
			ms.setTarget(tiltChannel, tiltPos);
			panPos = Math.min(panMax, Math.max(panMin, (int) (panPos-(70*searchPositive))));
			if(panPos==panMax || panPos==panMin)
				searchPositive = searchPositive * -1;
			ms.setTarget(panChannel, panPos);
		}

		public void close()
		{
			if (this.timer!=null && !this.timer.isShutdown())
			{
				try
				{
					// stop the timer
					this.timer.shutdown();
					this.timer.awaitTermination(200, TimeUnit.MILLISECONDS);
				}
				catch (InterruptedException e)
				{
					// log any exception
					System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
				}
			}
			runThread = false;
			ms.shutdown();
			or.setClosed();
		}

		public double getAzimuth() {
			return azimuth;
		}

		public double getDistance() {
			return distance;
		}
	}
		
	static ObjectDetectorInternal od = null;

	public ObjectDetector(String in,MaestroSSC inms) {
		super(in);
		if(od == null)
			od = new ObjectDetectorInternal(inms);
	}
	
	@Override
	public double get() {
		if(port == 0)
			return od.getAzimuth();
		else
			return od.getDistance();
	}
	
	public double getNormalizedValue()
	{		
		if(port == 0)
			return (normalizedValue =((od.getAzimuth()*0.2)+0.4));
		else
			return (normalizedValue =od.getDistance());
	}
	
	public double getValue() 
	{
		if(port == 0)
			return od.getAzimuth();
		else
			return od.getDistance();
	}
}
