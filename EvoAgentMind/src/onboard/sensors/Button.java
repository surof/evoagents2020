/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.sensors;

import onboard.bot.drivers.sensors.ButtonCard;

public class Button extends PhysicalSensor{
	ButtonCard card;
	int timer = 0;
	int timerLength = 10;
	
	public Button(String in,ButtonCard bc) {
		super(in);
		card = bc;
	}
	
	public double getNormalizedValue()
	{
		return (normalizedValue = getValue());
	}
	
	 public double getValue() {
		if(card.getValue() || timer > 0)
		{
			if(timer <= 0)
				timer = timerLength;
			timer--;
			return 1.0;			
		}
		return 0.0;
	}
}
