/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.bot;

import java.util.HashMap;

import evoagentmindelements.EvoAgentMind;
import onboard.actuators.PhysicalActuator;
import onboard.sensors.PhysicalSensor;

public interface PhysicalBot {

	public HashMap<String,PhysicalSensor> sensors = new HashMap<String,PhysicalSensor>();
	public HashMap<String,PhysicalActuator> actuators = new HashMap<String,PhysicalActuator>();
	
	public void shutdown();
	public void reset();
	public void updateSensors();
	public void updateActuators();
	public void readBotConfigFile(String cfgFile);
	public default void testCommand(String command) {};
	
	public default void setMind(EvoAgentMind evoAgentMind) {
		if(evoAgentMind!=null)
		{
			for(String k : actuators.keySet())
				actuators.get(k).setMindModule(evoAgentMind.getActuator(k));		
			for(String k : sensors.keySet())
				sensors.get(k).setMindModule(evoAgentMind.getSensor(k));			
		}
	}	
	
	public default void printAllSensorValues()
	{	       
		System.out.print("\033[H\033[2J");  
    	System.out.flush();  
		for(String k : sensors.keySet())
		{
			System.out.println(k + " : " + sensors.get(k).getNormalizedValue());
		}	
		for(String k : actuators.keySet())
		{
			System.out.println(k + " : " + actuators.get(k).normalizedValue);
		}	
		/*
		for(String k : actuators.keySet())
		{
			System.out.print("\033[F\r");
		}
		for(String k : sensors.keySet())
		{
			System.out.print("\033[F\r");
		}	
		System.out.print("\033[F\r");
		*/   
	}
}
