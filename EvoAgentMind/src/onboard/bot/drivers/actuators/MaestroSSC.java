package onboard.bot.drivers.actuators;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import onboard.bot.drivers.actuatorCard;

/**
 * Primary class for controlling servos via USB.
 * 
 * Android requires us to get user permission to access a USB device. The best
 * approach is to do that in the Activity and then pass the device into the
 * overloaded constructor.
 * 
 * @See MaestroSSCActivity.java
 * 
 * @author Patrick H. Ryan
 * 
 */

// port from that guy's android (haha fuck that) bullshit, copyright 2018 , suro

/*
 * Copyright (C) 2014, 2020 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */
// and this guy too ...

public class MaestroSSC  implements actuatorCard{
	//private static final String TAG = "MaestroServoControl";
	
	// Pololu Protocol Commands
	public static final int CMD_SET_POSITION = 0x85;
	public static final int CMD_SET_SPEED = 0x87;
	public static final int CMD_SET_ACCELERATION = 0x89;
	public static final int CMD_SET_PWM = 0x8A;
	public static final int CMD_GET_POSITION = 0x90;
	public static final int CMD_GET_MOVING_STATE = 0x93;
	public static final int CMD_GET_ERRORS = 0xA1;
	public static final int CMD_GO_HOME = 0xA2;
	
	public static final int VENDOR_ID = 0x1ffb;
	public static final int PRODUCT_ID = 0x0089;
	
	private ArrayList<String> portNames;
	private InputStream in ;
    private OutputStream out ;
	
	public MaestroSSC() {	
		String serialPortID = "/dev/ttyACM0:/dev/ttyACM1:/dev/ttyACM2:/dev/ttyACM3:/dev/ttyACM4";
		System.setProperty("gnu.io.rxtx.SerialPorts", serialPortID);
		findPorts();
		try {
			int i = 0;
			boolean connected = false;
			while(i < portNames.size() && ! connected)
			{
				connected = connect(portNames.get(i));
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    private boolean  connect ( String portName ) throws Exception
    {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if ( portIdentifier.isCurrentlyOwned() )
        {
            System.out.println("Error: Port is currently in use");
        }
        else
        {
            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
            if ( commPort instanceof SerialPort )
            {
            	System.out.println("connected to : " +portName );
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(57600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                in = serialPort.getInputStream();
                out = serialPort.getOutputStream();
                return true;
            }
            else
            {
                System.out.println("Error: Only serial ports are handled by this example.");
            }
        }     
        return false;
    }

    public void listPorts()
	    {
		  @SuppressWarnings("unchecked")
	        java.util.Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();

	        while ( portEnum.hasMoreElements() ) 
	        {
	            CommPortIdentifier portIdentifier = portEnum.nextElement();
	            System.out.println(portIdentifier.getName()  +  " - " +  getPortTypeName(portIdentifier.getPortType()) );           

	            if (portIdentifier.getPortType() == 1)
	            {
	                SerialPort serialPort;
					try
	                {
	                    serialPort =  (SerialPort) portIdentifier.open(portIdentifier.getName(), 3000);
	                }
	                catch (PortInUseException e)
	                {
	                    System.err.print("port in use");
	                    continue;
	                }

	                System.out.println("Baud is " + serialPort.getBaudRate());    
	                System.out.println("Bits is " + serialPort.getDataBits());    
	                System.out.println("Stop is " + serialPort.getStopBits());    
	                System.out.println("Par is " + serialPort.getParity());
	                //serialPort.shutdown();// borken?
	            }
	        }
	    }

	  public void findPorts()
	    {
		  portNames = new ArrayList<>();
		  @SuppressWarnings("unchecked")
	        java.util.Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();

	        while ( portEnum.hasMoreElements() ) 
	        {
	            CommPortIdentifier portIdentifier = portEnum.nextElement();
	            System.out.println("found :"+ portIdentifier.getName()  +  " - " +  getPortTypeName(portIdentifier.getPortType()) );           
	            if (portIdentifier.getPortType() == 1)
	            {
	            	portNames.add(portIdentifier.getName());
	            }
	        }
	    }

	    static String getPortTypeName ( int portType )
	    {
	        switch ( portType )
	        {
	            case CommPortIdentifier.PORT_I2C:
	                return "I2C";
	            case CommPortIdentifier.PORT_PARALLEL:
	                return "Parallel";
	            case CommPortIdentifier.PORT_RAW:
	                return "Raw";
	            case CommPortIdentifier.PORT_RS485:
	                return "RS485";
	            case CommPortIdentifier.PORT_SERIAL:
	                return "Serial";
	            default:
	                return "unknown type";
	        }
	    }

	public void sendCommand(int command, int channel, int value) {

		// NOTE: The Maestro's serial mode must be set to "USB Dual Port".
		
		byte[] message = new byte[4];
		message[0] = (byte) 0x84;
		message[1] = (byte) channel;
		message[2] = (byte) (value & 0x7F);
		message[3] = (byte) (value>>7 & 0X7F);
		try {
			
			out.write(message);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setTarget(int channel, int target) {
		// Pololu Protocol expects you to pass in how many QUARTER-seconds.
		target = target * 4;
		sendCommand(CMD_SET_POSITION, channel, target);
	}
	
	public void setSpeed(int channel, int speed) {
		sendCommand(CMD_SET_SPEED, channel, speed);
	}
	
	public void setAcceleration(int channel, int acceleration) {		
		sendCommand(CMD_SET_ACCELERATION, channel, acceleration);
	}
	
	public void setPWM(int onTime, int period) {
		sendCommand(CMD_SET_PWM, onTime, period);
	}
	
	public int getPosition(int channel){

		byte[] message = new byte[2];
		message[0] = (byte) CMD_GET_POSITION;
		message[1] = (byte) channel;
		try {
			
			out.write(message);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			in.read(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return((Byte.toUnsignedInt(message[0])+(Byte.toUnsignedInt(message[1])*255))/4);
	}
	
	public int getMovingState(){
		sendCommand(CMD_GET_MOVING_STATE, 0, 0);
		return(0);
	}
	
	public int getErrors(){
		sendCommand(CMD_GET_ERRORS, 0, 0);
		return(0);
	}
	
	public void goHome(int panChannel) {
		sendCommand(CMD_GO_HOME, panChannel, 0);
	}

	public void close()
	{
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendCommand() {
	}
}