/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.bot.drivers.actuators;

import java.io.IOException;

import org.iot.raspberry.grovepi.GroveDigitalOut;

import onboard.bot.drivers.actuatorCard;
import onboard.grovepilibextends.MYGrovePi4J;


public class ElectromagnetCard implements actuatorCard{
	private GroveDigitalOut pin = null;
	private int address = 0;
	private boolean value = false;
	
	public ElectromagnetCard(MYGrovePi4J gp,int addr)
	{
		address = addr;
		try {
			pin = gp.getDigitalOut(address);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setValue(boolean cmd)
	{
		value = cmd;
	}

	@Override
	public void sendCommand() {
		try {
			pin.set(value);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}
