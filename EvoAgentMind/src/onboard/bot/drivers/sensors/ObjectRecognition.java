/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.bot.drivers.sensors;

import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;


/**
 * The controller associated with the only view of our application. The
 * application logic is implemented here. It handles the button for
 * starting/stopping the camera, the acquired video stream, the relative
 * controls and the image segmentation process.
 * 
 * @author <a href="mailto:luigi.derussis@polito.it">Luigi De Russis</a>
 * @version 2.0 (2017-03-10)
 * @since 1.0 (2015-01-13)
 * 
 */
public class ObjectRecognition
{
	// sat : 119.2-255
	// val : 0 - 255
	// hue 1 : 0 - 13
	// hue 2 : 160 - 180
	
	private double hueStart = 0;
	private double saturationStart = 254;
	private double valueStart = 150;
	private double hueStop = 1;
	private double saturationStop = 255;
	private double valueStop = 240;
	
	Scalar minValues;
	Scalar maxValues;

	Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(24, 24));
	//Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(12, 12));
	Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(8, 8));

	Mat blurredImage = new Mat();
	Mat hsvImage = new Mat();
	Mat mask = new Mat();
	Mat morphOutput = new Mat();
	Mat hierarchy = new Mat();
	
	private int posX = -1;
	private int posY = -1;
	
	private int xMax=0;
	private int yMax=0;
	private int xMid=0;
	private int yMid=0;
	
	
	//my cam buffer
	private boolean cameraActive = false;
	private ArrayList<Mat> frames;
	private int frameIndex = 0;
	private int frameNumber = 3;
	//private int camindex = Videoio.CAP_FFMPEG;
	// the OpenCV object that performs the video capture
	private VideoCapture capture = new VideoCapture();
	
	private Mat currentFrame;
		
	public ObjectRecognition()
	{
		minValues = new Scalar(this.hueStart, this.saturationStart,
				this.valueStart);
		maxValues = new Scalar(this.hueStop, this.saturationStop,
				this.valueStop);
	}

	public void startCamera()
	{
		if (!this.cameraActive)
		{
			// start the video capture
			if(!this.capture.open(0))
				System.exit(0);
			
			//this.capture.set(Videoio.CAP_PROP_FRAME_WIDTH, 800);
			//this.capture.set(Videoio.CAP_PROP_FRAME_HEIGHT, 600);
			// config cam
			System.out.println("setting: autoexposure : " + this.capture.get(Videoio.CAP_PROP_AUTO_EXPOSURE));
			this.capture.set(Videoio.CAP_PROP_AUTO_EXPOSURE, 0.5);
			System.out.println("set: autoexposure : " + this.capture.get(Videoio.CAP_PROP_AUTO_EXPOSURE));
			System.out.println("setting: exposure :" + this.capture.get(Videoio.CAP_PROP_EXPOSURE));
			this.capture.set(Videoio.CAP_PROP_EXPOSURE, 0.0002);
			System.out.println("set: exposure :" + this.capture.get(Videoio.CAP_PROP_EXPOSURE));		

			System.out.println("setting: fps : " + this.capture.get(Videoio.CAP_PROP_FPS));
			this.capture.set(Videoio.CAP_PROP_FPS, 5.0);
			System.out.println("set: fps : " + this.capture.get(Videoio.CAP_PROP_FPS));
			System.out.println("setting: contrast : " + this.capture.get(Videoio.CAP_PROP_CONTRAST));
			this.capture.set(Videoio.CAP_PROP_CONTRAST, 0.2);
			System.out.println("set: contrast : " + this.capture.get(Videoio.CAP_PROP_CONTRAST));
			System.out.println("setting: brightness : " + this.capture.get(Videoio.CAP_PROP_BRIGHTNESS));
			this.capture.set(Videoio.CAP_PROP_BRIGHTNESS, 0.0);
			System.out.println("set: brightness : " + this.capture.get(Videoio.CAP_PROP_BRIGHTNESS));

			// is the video stream available?
			if (this.capture.isOpened())
			{
				frames = new ArrayList<>();
				for (int i = 0 ; i < frameNumber; i++)
					frames.add(new Mat());
				currentFrame = frames.get(frameIndex);
				this.cameraActive = true;
				this.capture.read(currentFrame);
				Imgcodecs.imwrite("startCam.png", currentFrame);
				xMax = currentFrame.cols();
				yMax = currentFrame.rows();
				xMid = xMax/2;
				yMid = yMax/2;
				System.out.println("camera started");
				
				Runnable frameGrabber = new Runnable() {
					@Override
					public void run()
					{
						while (cameraActive)
						{
							capture.read(frames.get((frameIndex+1)%frameNumber));
							frameIndex = (frameIndex+1)%frameNumber;
						}
					}
				};
				new Thread(frameGrabber).start();
			}
			else
			{
				// log the error
				System.err.println("Failed to open the camera connection...");
				System.exit(0);
			}
		}
		else
		{
			// the camera is not active at this point
			this.cameraActive = false;
			this.stopAcquisition();
		}
	}
	
	/**
	 * Get a frame from the opened video stream (if any)
	 * 
	 * @return the {@link Image} to show
	 */
	public void detect()
	{
		// check if the capture is open
		if (this.capture.isOpened())
		{
			try
			{
				currentFrame = frames.get(frameIndex);
				//System.out.println(frameIndex);
				// if the frame is not empty, process it
				if (!currentFrame.empty())
				{	
					 //Imgcodecs.imwrite("frame.png", currentFrame);
					// remove some noise
					Imgproc.blur(currentFrame, blurredImage, new Size(7, 7));
					
					// convert the frame to HSV
					Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);

					// threshold HSV image to select tennis balls
					Core.inRange(hsvImage, minValues, maxValues, mask);
					
					//Imgcodecs.imwrite("mask2.png", mask);
					
					Imgproc.erode(mask, morphOutput, erodeElement);
					//Imgproc.erode(morphOutput, morphOutput, erodeElement);
					//Imgproc.erode(morphOutput, morphOutput, erodeElement);
					
					//Imgproc.dilate(morphOutput, morphOutput, dilateElement);
					//Imgproc.dilate(morphOutput, morphOutput, dilateElement);
					Imgproc.dilate(morphOutput, morphOutput, dilateElement);
					
					//Imgcodecs.imwrite("mask.png", morphOutput);
					
					this.findPosition(morphOutput, currentFrame);
				}
				else
				{
					posX = -1;posY = -1;
				}
				
			}
			catch (Exception e)
			{
				// log the (full) error
				System.err.print("Exception during the image elaboration...");
				//e.printStackTrace(System.err);
				posX = -1;posY = -1;
			}
		}				
		else
		{
			posX = -1;posY = -1;
		}
	}

	private void findPosition(Mat maskedImage, Mat frame)
	{
		// init
		List<MatOfPoint> contours = new ArrayList<>();
		
		double bestArea = 0;
		double curArea;
		Rect selected;
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
		posX = -1; posY = -1;		
		// if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			selected = Imgproc.boundingRect(contours.get(0));
			  for (int i = 0; i < contours.size(); i++)
		      {
		        Rect r = Imgproc.boundingRect(contours.get(i));
		        curArea = r.area();
		        if(curArea > bestArea)
		        {
		        	bestArea = curArea;
		        	selected = r;
		        }
		        posX = selected.x + (selected.width / 2);
		        posY = selected.y + (selected.height / 2);
		      }
		}
	}
	
	/**
	 * Stop the acquisition from the camera and release all the resources
	 */
	public void stopAcquisition()
	{
		cameraActive = false;
		dilateElement.release();
		erodeElement.release();
		blurredImage.release();
		hsvImage.release();
		mask.release();
		morphOutput.release();
		hierarchy.release();
		for (int i = 0 ; i < frameNumber; i++)
			frames.get(i).release();
		if (this.capture.isOpened())
		{
			// release the camera
			this.capture.release();
		}
	}
	
	/**
	 * On application close, stop the acquisition from the camera
	 */
	public void setClosed()
	{
		this.stopAcquisition();
	}
	
	public int getYOffset()
	{
		return yMid - posY;
	}

	public int getXOffset()
	{
		return xMid - posX;
	}
	
	public double getNormalizedYOffset()
	{
		return (yMid - posY)/(yMax/2.0);
	}

	public double getNormalizedXOffset()
	{
		return (xMid - posX)/(xMax/2.0);
	}

	public double getBiasedYOffset()
	{
		if(yMid - posY < 0)
			return - Math.pow((yMid - posY)/(yMax/2.0),2.0);
		else
			return Math.pow((yMid - posY)/(yMax/2.0),2.0);			
	}

	public double getBiasedXOffset()
	{
		if(xMid - posX < 0)
			return - Math.pow((xMid - posX)/(xMax/2.0),2.0);
		else
			return Math.pow((xMid - posX)/(xMax/2.0),2.0);				
	}

	public boolean hasDetected()
	{
		if(posX != -1 && posY != -1)
			return true;
		return false;
	}
}