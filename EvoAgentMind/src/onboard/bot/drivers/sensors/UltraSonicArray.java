/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package onboard.bot.drivers.sensors;

import java.io.IOException;

import org.iot.raspberry.grovepi.GroveUtil;

import onboard.bot.drivers.sensorCard;
import onboard.grovepilibextends.MYGrovePi4J;
import onboard.grovepilibextends.MYIO;
import static org.iot.raspberry.grovepi.GrovePiCommands.*;

public class UltraSonicArray implements sensorCard{

	private final MYGrovePi4J grovePi1;
	private final MYGrovePi4J grovePi2;

	private int[] adress = new int[] {2,3,4,7,8};
	private double[] ret = new double[10];

	public UltraSonicArray(MYGrovePi4J GP1T,MYGrovePi4J GP2B) {
		    this.grovePi1 = GP1T;
		    this.grovePi2 = GP2B;
		  }

	public void update(){	
		//System.out.print("\033[H\033[2J");  
	    //System.out.flush(); 
	    byte[] buff2 = new byte[4];
	    byte[] buff = new byte[4];
		buff2[0] = uRead_cmd;
	    buff2[2] = unused;
	    buff2[3] = unused;

		for(int i = 0 ; i < adress.length;i++)
		{
			int index = i;
			int add = adress[i];
			buff2[1] = (byte) add;	    
			try {
				grovePi1.execVoid((io) -> {
					//io.write(uRead_cmd, add, unused, unused);

					((MYIO)io).write(buff2);
					io.sleep(5);
				});
				
				grovePi1.execVoid((io2) -> {

					int[] v1 = GroveUtil.unsign(((MYIO)io2).read(add,buff,4));
					//System.out.println("top");
					//System.out.println(buff[0]+ " " +buff[1] + " " + buff[2] + " " + buff[3]);
					//System.out.println(v1[0]+ " " +v1[1] + " " + v1[2] + " " + v1[3]);
					ret[index] = (double) ((v1[1] * 256) + v1[2]);
					io2.sleep(5);
				});	
				
				grovePi2.execVoid((io) -> {
					//io.write(uRead_cmd, add, unused, unused);

					((MYIO)io).write(buff2);
					io.sleep(5);
				});
				
				
				grovePi2.execVoid((io2) -> {

					int[] v2 = GroveUtil.unsign(((MYIO)io2).read(add,buff,4));
					//System.out.println("bot");
					//System.out.println(buff[0]+ " " +buff[1] + " " + buff[2] + " " + buff[3]);
					//System.out.println(v2[0]+ " "+ v2[1] + " " + v2[2] + " " + v2[3]);
					ret[index+5] = (double) ((v2[1] * 256) + v2[2]);
					io2.sleep(5);
				});
	
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
		}
	}

	public double get(int port) {
		return ret[port];
	}
}