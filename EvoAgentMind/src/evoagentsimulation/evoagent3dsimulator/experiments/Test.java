/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.Transform;

import evoagentsimulation.evoagent3dsimulator.BotMotionState;
import evoagentsimulation.evoagent3dsimulator.Entity3D;
import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;

public class Test extends SimulationEnvironment3D {

	public Test() {
		super();
		Xborder = 150;
		Yborder = 150;
		Zborder = 150;
	}

	@Override
	public void init() {
		super.init();
		bot.setStartPosition(2f, 5f, -15f);
		bot.makeBody();
	}

	@Override
	protected void makeObstacles() {

		// create 125 (5x5x5) dynamic object
		final int ARRAY_SIZE_X = 5;
		final int ARRAY_SIZE_Y = 5;
		final int ARRAY_SIZE_Z = 5;

		final int START_POS_X = -5;
		final int START_POS_Y = -5;
		final int START_POS_Z = -3;

		{
			// create a few dynamic rigidbodies
			// Re-using the same collision is better for memory usage and performance

			CollisionShape colShape = new BoxShape(new Vector3f(1, 1, 1));
			// CollisionShape colShape = new SphereShape(1f);
			collisionShapes.add(colShape);

			// Create Dynamic Objects
			Transform startTransform = new Transform();
			startTransform.setIdentity();

			float mass = 5f;

			// rigidbody is dynamic if and only if mass is non zero, otherwise static
			boolean isDynamic = (mass != 0f);

			Vector3f localInertia = new Vector3f(0, 0, 0);
			if (isDynamic) {
				colShape.calculateLocalInertia(mass, localInertia);
			}

			float start_x = START_POS_X - ARRAY_SIZE_X / 2;
			float start_y = START_POS_Y;
			float start_z = START_POS_Z - ARRAY_SIZE_Z / 2;

			for (int k = 0; k < ARRAY_SIZE_Y; k++) {
				for (int i = 0; i < ARRAY_SIZE_X; i++) {
					for (int j = 0; j < ARRAY_SIZE_Z; j++) {
						startTransform.origin.set(2f * i + start_x, 10f + 2f * k + start_y, 2f * j + start_z);

						// using motionstate is recommended, it provides interpolation capabilities, and
						// only synchronizes 'active' objects
						BotMotionState myMotionState = new BotMotionState(startTransform);
						RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, colShape,
								localInertia);
						RigidBody body = new RigidBody(rbInfo);
						body.setActivationState(RigidBody.ISLAND_SLEEPING);
						body.setDamping(WorldDamping, WorldDamping);
						dynamicsWorld.addRigidBody(body);
						body.setActivationState(RigidBody.ISLAND_SLEEPING);
						obstacles.add(new Entity3D(colShape, body));
					}
				}
			}
		}
	}
}