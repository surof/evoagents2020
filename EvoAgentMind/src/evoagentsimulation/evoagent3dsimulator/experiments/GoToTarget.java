/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments;

import javax.vecmath.Vector3f;

import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;
import evoagentsimulation.evoagent3dsimulator.bot.elements.S3D_TargetSensor;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.CF3D_NextOnTimeout;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_ClosingOnTarget;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_Forward;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_Manual;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RewardFunction3D;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.TargetObject;

public class GoToTarget extends SimulationEnvironment3D {

	double reachTargetReward = 5;
	float reachDistance = 20f;
	TargetObject targObj;
	RW3D_Manual manualReward;
	
	public GoToTarget() {
		super();

		Xborder = 250;
		Yborder = 250;
		Zborder = 250;
	}

	@Override
	public void init() {
		super.init();
		underwaterWorldPhysicsPreset();
		bot.setStartPosition(0f, 0f, 0f);
		Vector3f targPos = new Vector3f(0f,0f,0f);
		markers.add(targPos);
		targObj = new TargetObject(targPos);
		((S3D_TargetSensor)bot.sensors.get("TARG_AZIM")).setTarget(targObj);
		((S3D_TargetSensor)bot.sensors.get("TARG_ELEV")).setTarget(targObj);
		((S3D_TargetSensor)bot.sensors.get("TARG_BANK")).setTarget(targObj);
		bot.makeBody();
		posTargetObject(targObj);
		
		controlFunctions.add(new CF3D_NextOnTimeout(bot,this,1000000));
		rewardFunctions.add(new RW3D_ClosingOnTarget(bot, 0.001, targObj));
		rewardFunctions.add(new RW3D_Forward(bot, 0.0001));
		manualReward = new RW3D_Manual(bot, reachTargetReward);
		rewardFunctions.add(manualReward);
		
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();
		Vector3f vec = new Vector3f(targObj.position);
		vec.sub(bot.currentWorldTrans.origin);
		if(vec.length() < reachDistance)
		{
			posTargetObject(targObj);
			for(RewardFunction3D r: rewardFunctions)
				r.reset();
			manualReward.addRewardStep();
		}
	}
	
	private void posTargetObject(TargetObject obj) {
		Vector3f dist,pos;
		do {
			pos = generatePositionInBoundaries(20f);
			dist = new Vector3f(pos);
			dist.sub(bot.currentWorldTrans.origin);
		}while(dist.length() < 100f);
		obj.position.setX(pos.x);
		obj.position.setY(pos.y);
		obj.position.setZ(pos.z);
	}

	@Override
	public void reset()
	{
		super.reset();
		posTargetObject(targObj);
		for(RewardFunction3D r: rewardFunctions)
			r.reset();
	}
}