/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments;

import java.util.ArrayList;

import javax.vecmath.Vector3f;


import com.bulletphysics.linearmath.Transform;

import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;
import evoagentsimulation.evoagent3dsimulator.bot.elements.S3D_TargetSensor;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.CF3D_NextOnTimeoutAndCollision;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_ClosingOnTarget;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_Manual;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RewardFunction3D;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.Static3DObstacle;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.TargetObject;

public class GTTAvoid extends SimulationEnvironment3D {

	double reachTargetReward = 1;
	float reachDistance = 20f;
	TargetObject targObj;
	RW3D_Manual manualReward;
	ArrayList<Static3DObstacle> staticObstacles = new ArrayList<>();
	
	public GTTAvoid() {
		super();
		Xborder = 250;
		Yborder = 120;
		Zborder = 250;
	}

	@Override
	public void init() {
		super.init();
		underwaterWorldPhysicsPreset();
		bot.setStartPosition(200f, 0f, 200f);
		Vector3f targPos = new Vector3f(0f,0f,0f);
		markers.add(targPos);
		targObj = new TargetObject(targPos);
		((S3D_TargetSensor)bot.sensors.get("TARG_AZIM")).setTarget(targObj);
		((S3D_TargetSensor)bot.sensors.get("TARG_ELEV")).setTarget(targObj);
		((S3D_TargetSensor)bot.sensors.get("TARG_BANK")).setTarget(targObj);
		bot.makeBody();
		makeStaticGridObstacles(55);

		
		Static3DObstacle obs;
		obs = new Static3DObstacle(this, new Vector3f(2f,20f,250f)
				, new Vector3f(0,0,0),40f);
		staticObstacles.add(obs);
		obstacles.add(obs.getEntity());
		obs = new Static3DObstacle(this, new Vector3f(250f,20f,2f)
				, new Vector3f(0,0,0),40f);
		staticObstacles.add(obs);
		obstacles.add(obs.getEntity());
		

		posTargetObject(targObj);
		
		controlFunctions.add(new CF3D_NextOnTimeoutAndCollision(bot,this,1000000));
		rewardFunctions.add(new RW3D_ClosingOnTarget(bot, 0.001, targObj));
		//rewardFunctions.add(new RW3D_CollisionDelayedStart(bot, this, -10,200));
		manualReward = new RW3D_Manual(bot, reachTargetReward);
		rewardFunctions.add(manualReward);	
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();
		//posTargetObject(targObj);
		Vector3f vec = new Vector3f(targObj.position);
		vec.sub(bot.currentWorldTrans.origin);
		if(vec.length() < reachDistance)
		{
			posTargetObject(targObj);
			for(RewardFunction3D r: rewardFunctions)
				r.reset();
			manualReward.addRewardStep();
		}
	}
	
	private void posTargetObject(TargetObject obj) {
		Vector3f dist,pos;
		float maxDist = 820f;
		do {
			pos = generatePositionInBoundaries(80f);
			dist = new Vector3f(pos);
			dist.sub(bot.currentWorldTrans.origin);
			maxDist -= 10f;
		}while(dist.length() < maxDist || noObstaclesInRange(pos, 50));
		obj.position.setX(pos.x);
		obj.position.setY(pos.y);
		obj.position.setZ(pos.z);
	}

	private boolean noObstaclesInRange(Vector3f pos, int space) {
		Vector3f dist;
		Transform tr = new Transform();
		if ((pos.x< 20 && pos.x > -20)||(pos.z< 20 && pos.z > -20))
			return true;
		for(Static3DObstacle so :staticObstacles)
		{
			dist = new Vector3f(pos);
			so.getEntity().getBody().getWorldTransform(tr);
			dist.sub(tr.origin);
			if(dist.length() < space)
				return true;
		}
		return false;
	}

	protected void makeStaticGridObstacles_old(int space)
	{
		Static3DObstacle obs;
		double thirdSpace = space/ 3.0;
		double doubleSpace = space*2;
		double halfSpace = space*0.5;
		double varFactor = 0.3;
		double finalVarFactor = 1.8;
		float obsRestitution = 40f;
		for(int x = space; x < Xborder ; x+=doubleSpace)
		{
			for(int y = space; y < Yborder ; y+=doubleSpace)
			{
				for(int z = space; z < Zborder ; z+=doubleSpace)
				{
					if(z!=space || y!=space || x !=space)
						varFactor = finalVarFactor;
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
			
				}}}			
	}
	
	protected void makeStaticGridObstacles(int space)
	{
		Static3DObstacle obs;
		double thirdSpace = space/ 4.0;
		//double thirdSpace = space/ 5.0;
		double doubleSpace = space*2;
		double halfSpace = space*0.5;
		double varFactor = 0.3;
		double lowVarFactor = 0.3;
		double finalVarFactor = 1.7;
		varFactor =finalVarFactor;
		float obsRestitution = 40f;
		for(int x = space; x < Xborder ; x+=doubleSpace)
		{
			for(int y = space; y < Yborder ; y+=doubleSpace)
			{
				for(int z = space; z < Zborder ; z+=doubleSpace)
				{
					
					if(z + doubleSpace >= Zborder && x + doubleSpace >= Xborder )
						varFactor = lowVarFactor;
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					varFactor = finalVarFactor;
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					if(z + doubleSpace >= Zborder && x + doubleSpace >= Xborder )
						varFactor = lowVarFactor;
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					varFactor = finalVarFactor;
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-y+(((Math.random()*space)-halfSpace)*varFactor)),(float)(-z+(((Math.random()*space)-halfSpace)*varFactor))),obsRestitution);
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
			
				}}}			
	}
	
	@Override
	public void reset()
	{
		super.reset();
		posTargetObject(targObj);
		for(RewardFunction3D r: rewardFunctions)
			r.reset();
	}
}