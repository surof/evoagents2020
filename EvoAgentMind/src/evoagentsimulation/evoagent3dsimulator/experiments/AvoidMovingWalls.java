/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments;

import java.util.ArrayList;

import javax.vecmath.Vector3f;

import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.CF3D_NextOnTimeout;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.Homing3DDynamicObstacle;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.Homing3DObstacle;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.MovingObstacle;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_ActuatorPowerConsumption;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_Alive;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_CollisionDelayedStart;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RW3D_Manual;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RewardFunction3D;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.Static3DObstacle;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.TargetObject;

public class AvoidMovingWalls extends SimulationEnvironment3D {

	TargetObject targObj;
	RW3D_Manual manualReward;
	ArrayList<MovingObstacle> movingObstacles = new ArrayList<>();
	ArrayList<Static3DObstacle> staticObstacles = new ArrayList<>();
	ArrayList<Homing3DObstacle> homingObstacles = new ArrayList<>();
	ArrayList<Homing3DDynamicObstacle> homingDynamicObstacles = new ArrayList<>();
	
	public AvoidMovingWalls() {
		super();
		Xborder = 120;
		Yborder = 120;
		Zborder = 120;
	}

	@Override
	public void init() {
		super.init();
		underwaterWorldPhysicsPreset();
		bot.setStartPosition(0f, 0f, 0f);
		bot.makeBody();
		MovingObstacle obs;
		obs = new MovingObstacle(this,new Vector3f(Xborder,Yborder,Zborder),new Vector3f(Xborder*2,0,0),new Vector3f(1,0,0),0.1f);		
		movingObstacles.add(obs);
		obstacles.add(obs.getEntity());
		
		controlFunctions.add(new CF3D_NextOnTimeout(bot,this,1000000));
		rewardFunctions.add(new RW3D_CollisionDelayedStart(bot, this, -10,200));
		rewardFunctions.add(new RW3D_Alive(bot, 0.1));
		rewardFunctions.add(new RW3D_ActuatorPowerConsumption(bot,this, 0.005));
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();
		for(MovingObstacle m : movingObstacles)
			m.step();
		for(Homing3DObstacle m : homingObstacles)
			m.step();		
		for(Homing3DDynamicObstacle m : homingDynamicObstacles)
			m.step();
		//dynamicsWorld.updateAabbs();
	}
	
	protected void makeMovingObstacles(int count)
	{
		MovingObstacle obs;
		Vector3f dist,pos;
		for(int i = 0; i < count ; i++)
		{
			do {
				pos = generatePositionInBoundaries(20f);
				dist = new Vector3f(pos);
				dist.sub(bot.currentWorldTrans.origin);
			}while(dist.length() < 120f);
			
			obs = new MovingObstacle(this, new Vector3f((float)(20+ (Math.random() * 3)),(float)(20+ (Math.random() * 3)),(float)(20+ (Math.random() * 3))), pos,
					new Vector3f((float)(0.5-Math.random()),(float)(0.5-Math.random()),(float)(0.5-Math.random())), (float)(0.05+ (Math.random() *0.02)));
			movingObstacles.add(obs);
			obstacles.add(obs.getEntity());
		}
	}

	protected void makeHomingObstacles(int count)
	{
		Homing3DObstacle obs;
		Vector3f dist,pos;
		for(int i = 0; i < count ; i++)
		{
			do {
				pos = generatePositionInBoundaries(20f);
				dist = new Vector3f(pos);
				dist.sub(bot.currentWorldTrans.origin);
			}while(dist.length() < 120f);
			
			obs = new Homing3DObstacle(this, new Vector3f((float)(15+ (Math.random() * 3)),(float)(20+ (Math.random() * 3)),(float)(20+ (Math.random() * 3))), pos,
					new Vector3f((float)(0.5-Math.random()),(float)(0.5-Math.random()),(float)(0.5-Math.random())), (float)(0.015+ (Math.random() *0.005)));
			homingObstacles.add(obs);
			obstacles.add(obs.getEntity());
		}
	}
	
	protected void makeHomingDynamicObstacles(int count)
	{
		Homing3DDynamicObstacle obs;
		Vector3f dist,pos;
		for(int i = 0; i < count ; i++)
		{
			do {
				pos = generatePositionInBoundaries(20f);
				dist = new Vector3f(pos);
				dist.sub(bot.currentWorldTrans.origin);
			}while(dist.length() < 120f);
			
			obs = new Homing3DDynamicObstacle(this, new Vector3f((float)(10+ (Math.random() * 3)),(float)(20+ (Math.random() * 3)),(float)(20+ (Math.random() * 3))), pos,
					new Vector3f((float)(0.5-Math.random()),(float)(0.5-Math.random()),(float)(0.5-Math.random())), (float)(0.015+ (Math.random() *0.005)));
			homingDynamicObstacles.add(obs);
			obstacles.add(obs.getEntity());
		}
	}

	
	protected void makeStaticObstacles(int count)
	{
		Static3DObstacle obs;
		Vector3f dist,pos;
		for(int i = 0; i < count ; i++)
		{
			do {
				pos = generatePositionInBoundaries(20f);
				dist = new Vector3f(pos);
				dist.sub(bot.currentWorldTrans.origin);
			}while(dist.length() < 120f);
			
			obs = new Static3DObstacle(this, new Vector3f((float)(40+ (Math.random() * 10)),(float)(40+ (Math.random() * 10)),(float)(40+ (Math.random() * 10))), pos);
			staticObstacles.add(obs);
			obstacles.add(obs.getEntity());
		}
	}
	
	protected void makeStaticGridObstacles(int space)
	{
		Static3DObstacle obs;
		double thirdSpace = space/ 3.0;
		double doubleSpace = space*2;
		for(int x = space; x < Xborder ; x+=doubleSpace)
		{
			for(int y = space; y < Yborder ; y+=doubleSpace)
			{
				for(int z = space; z < Zborder ; z+=doubleSpace)
				{
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x),(float)(y),(float)(z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x),(float)(y),(float)(z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x),(float)(-y),(float)(z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x),(float)(-y),(float)(z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x),(float)(y),(float)(-z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x),(float)(y),(float)(-z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(x),(float)(-y),(float)(-z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
					obs = new Static3DObstacle(this, new Vector3f((float)(thirdSpace + (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)),(float)(thirdSpace+ (Math.random() * thirdSpace)))
							, new Vector3f((float)(-x),(float)(-y),(float)(-z)));
					staticObstacles.add(obs);
					obstacles.add(obs.getEntity());
			
				}}}			
	}

	@Override
	public void reset()
	{
		super.reset();
		for(RewardFunction3D r: rewardFunctions)
			r.reset();
	}
}