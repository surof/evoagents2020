/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments.elements;

import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;

public class RewardFunction3D{
	double rewardStep = 0.0;
	Bot3DBody body;
	
	public RewardFunction3D(Bot3DBody b, double rewardSt) {
		body = b;
		rewardStep = rewardSt;
	}

	public void addReward() {
		// TODO Auto-generated method stub
		
	}

	public void reset()
	{
		
	}
	
	public void resetStep()
	{
		
	}

	public double computeRewardValue() {
		return 0.0;
	}
}
