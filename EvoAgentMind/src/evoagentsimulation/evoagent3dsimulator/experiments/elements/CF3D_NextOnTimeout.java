/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments.elements;

import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;
import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;

public class CF3D_NextOnTimeout extends ControlFunction3D {
	int tickLimit;
	int tickCounter = 0;
	
	public CF3D_NextOnTimeout(Bot3DBody b,SimulationEnvironment3D wt, int tl) {
		super(b,wt);
		tickLimit = tl;
	}

	@Override
	public boolean performCheck()
	{
		if(forceNextSignal)
		{
			forceNextSignal = false;
			return true;
		}
		else
		{
			tickCounter++;
			if(tickCounter > tickLimit)
				return true;
		}
		return false;
	}
	
	public void reset()
	{
		super.reset();
		tickCounter = 0;
	}
	
	
}
