/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments.elements;

import javax.vecmath.Vector3f;

import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;

public class RW3D_ClosingOnTarget extends RewardFunction3D{
	TargetObject target = null;
	boolean reset = true;
	float prevDist;
	
	
	public RW3D_ClosingOnTarget(Bot3DBody b, double rewardSt, TargetObject targ) {
		super(b, rewardSt);
		target = targ;
	}
	

	public void addReward() {
		// TODO Auto-generated method stub
		
	}

	public void reset()
	{
		reset = true;
	}
	
	public void resetStep()
	{
		
	}

	public double computeRewardValue() {
		double reward = 0.0;
		if(target != null && body != null)
		{
			float dist;
			Vector3f vec = new Vector3f(target.position);
			vec.sub(body.currentWorldTrans.origin);
			dist = vec.length();
			if(!reset)
				reward = (prevDist - dist) * rewardStep;
			prevDist = dist;
			reset = false;
		}
		return reward;
	}
}
