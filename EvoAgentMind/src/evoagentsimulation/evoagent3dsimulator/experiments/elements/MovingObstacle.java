/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments.elements;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

import evoagentsimulation.evoagent3dsimulator.BotMotionState;
import evoagentsimulation.evoagent3dsimulator.Entity3D;
import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;

public class MovingObstacle {
	protected SimulationEnvironment3D sim = null;
	private Entity3D entity = null;
	private Vector3f direction = null;
	private float speed = 0.1f;
	private int tick = 0;
	
	public MovingObstacle(SimulationEnvironment3D s, Vector3f size, Vector3f pos,Vector3f dir, float sp)
	{
		sim = s;
		if (entity == null)
		{
			direction = dir;
			direction.normalize();
			speed = sp;
			direction.scale(speed);
			//CollisionShape colShape = new BoxShape(size);
			CollisionShape colShape = new BoxShape(size);
			Transform startTransform = new Transform();
			startTransform.setIdentity();
			Vector3f localInertia = new Vector3f(0, 0, 0);
			colShape.calculateLocalInertia(0f, localInertia);		
			startTransform.origin.set(pos);
			MotionState myMotionState = new BotMotionState(startTransform);
			RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(0f, myMotionState, colShape,localInertia);
			RigidBody body = new RigidBody(rbInfo);
			body.setFriction(0.1f);
			body.setRestitution(0.5f);
			//body.setDamping(SimulationEnvironment3D.WorldDamping, SimulationEnvironment3D.WorldDamping);
			//body.setActivationState(RigidBody.DISABLE_DEACTIVATION );
			sim.getWorld().addRigidBody(body);
			//body.setActivationState(RigidBody.DISABLE_DEACTIVATION );
			entity = new Entity3D(colShape, body);
		}
	}
	
	public void step()
	{
		if(tick <= 0)
		{
			Transform tr = new Transform();
			
			((BotMotionState)entity.getBody().getMotionState()).getWorldTransform(tr);		

				direction = new Vector3f(sim.getBot().currentWorldTrans.origin);
				direction.sub(tr.origin);
				direction.normalize();
				direction.scale(speed*7);
				tick = (int) (1800 + (Math.random()*500));
		
		}
		tick--;
		entity.getBody().applyCentralImpulse(direction);
	}

	public Entity3D getEntity() {
		
		return entity;
	}
}
