/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.experiments.elements;


import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;
import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;

public class ControlFunction3D{
	SimulationEnvironment3D worldThread;
	boolean forceNextSignal = false;
	Bot3DBody body;
	
	public ControlFunction3D(Bot3DBody b,SimulationEnvironment3D wt) {
		body = b;
		worldThread = wt;
	}

	public boolean performCheck() {
		return false;
	}

	public void forceNextSignal() {
		forceNextSignal = true;
	}

	public void reset()
	{
		forceNextSignal = false;
	}
}
