/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.Clock;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;

import evoagentmindelements.EvoAgentMind;
import evoagentsimulation.SimulationEnvironment;
import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.ControlFunction3D;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.RewardFunction3D;
import evoagentsimulation.simulationlearning.ScoreCounter;
import evoagentsimulation.simulationlearning.SimulationInterruptFlag;

public class SimulationEnvironment3D extends SimulationEnvironment{

	public static float WorldDamping = 0.0f;
	
	protected Clock clock = new Clock();
	public ObjectArrayList<CollisionShape> collisionShapes = new ObjectArrayList<CollisionShape>();
	public ObjectArrayList<Entity3D> walls = new ObjectArrayList<Entity3D>();
	public ObjectArrayList<Entity3D> obstacles = new ObjectArrayList<Entity3D>();
	public ArrayList<Vector3f> markers = new ArrayList<Vector3f>();
	protected DynamicsWorld dynamicsWorld = null;
	protected Bot3DBody bot;
	public float Xborder = 200;
	public float Yborder = 200;
	public float Zborder = 200;

	private BroadphaseInterface broadphase;
	private CollisionDispatcher dispatcher;
	private ConstraintSolver solver;
	private DefaultCollisionConfiguration collisionConfiguration;

	protected ArrayList<ControlFunction3D> controlFunctions = new ArrayList<>();
	protected ArrayList<RewardFunction3D> rewardFunctions = new ArrayList<>();
	protected EvoAgentMind mind = null;

	public SimulationEnvironment3D() {
	}

	public void init() {
		// collision configuration contains default setup for memory, collision setup
		collisionConfiguration = new DefaultCollisionConfiguration();
		// use the default collision dispatcher. For parallel processing you can use a
		// diffent dispatcher (see Extras/BulletMultiThreaded)
		dispatcher = new CollisionDispatcher(collisionConfiguration);
		broadphase = new DbvtBroadphase();
		// the default constraint solver. For parallel processing you can use a
		// different solver (see Extras/BulletMultiThreaded)
		SequentialImpulseConstraintSolver sol = new SequentialImpulseConstraintSolver();
		solver = sol;

		// TODO: needed for SimpleDynamicsWorld
		// sol.setSolverMode(sol.getSolverMode() &
		// ~SolverMode.SOLVER_CACHE_FRIENDLY.getMask());
		dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
		dynamicsWorld.setGravity(new Vector3f(0f, 10f, 0f));
		makeBorders();
		makeObstacles();
		makeBot();
		clientResetScene();
	}


	public void doWorldStep() {
		dynamicsWorld.stepSimulation(1 / 10f, 3);
	}
	
	public HashMap<String, Double> doStep(HashMap<String, Double> actuatorValues, ScoreCounter scoreCounter, SimulationInterruptFlag simulationInterruptFlag) {
		super.doStep(actuatorValues, scoreCounter, simulationInterruptFlag);
		HashMap<String, Double> ret = null;
		if(bot !=null)
		{
			
			ret = bot.step(actuatorValues);
			if(scoreCounter != null)
				for(RewardFunction3D r : rewardFunctions)
					scoreCounter.addToScore(r.computeRewardValue());
			for(ControlFunction3D c : controlFunctions)
				simulationInterruptFlag.registerInterrupt(c.performCheck());
			//if(scoreCounter != null)
			//	System.out.println("score : " + scoreCounter.getCurrentScore());
		}

		return ret;
	}
	
    public void doStep(ScoreCounter scoreCounter, SimulationInterruptFlag simulationInterruptFlag) {
    	super.doStep(scoreCounter, simulationInterruptFlag);
		HashMap<String,Double> sensorValues = null;
		if(bot !=null)
		{
			if(scoreCounter != null)
				for(RewardFunction3D r : rewardFunctions)
					scoreCounter.addToScore(r.computeRewardValue());
						
			for(ControlFunction3D c : controlFunctions)
				simulationInterruptFlag.registerInterrupt(c.performCheck());
			
			sensorValues = bot.step(mind.getPreviousActuatorValues());
			mind.doStep(sensorValues);
		}
	}
	
	public void doAutoStep(ScoreCounter scoreCounter, SimulationInterruptFlag interruptFlag) {
		super.doAutoStep(scoreCounter, interruptFlag);
		if(getBot() !=null)
		{
			if(scoreCounter != null)
				for(RewardFunction3D r : rewardFunctions)
					scoreCounter.addToScore(r.computeRewardValue());
						
			for(ControlFunction3D c : controlFunctions)
				interruptFlag.registerInterrupt(c.performCheck());
			getBot().step();
			mind.doStep();
		}
	}
    
	public void preStepOps() {
		//Override me
		// called before a simulation step
	}
	
	public void preMindOps() {
		//Override me
		// called after the world step (physics), before the Mind step (agent logic)
	}
	
	public void postStepOps() {
		//Override me
		// called after a simulation step
	}
    
	public void preRepetitionOps() {
		//Override me
		// called before a repetition
	}
	
	public void postRepetitionOps(ScoreCounter scoreCounter, SimulationInterruptFlag simulationInterruptFlag) {
		//Override me		
		// called after a repetition
	}
	
	public void preRunOps() {
		//Override me
		// called after the init, before starting the first repetition
	}
	
	public void postRunOps(ScoreCounter score, SimulationInterruptFlag interruptFlag) {
		//Override me		
		// called after the last repetition
	}

	protected void underwaterWorldPhysicsPreset()
	{
		dynamicsWorld.setGravity(new Vector3f(0f, 0f, 0f));
		WorldDamping = 0.4f;
	}

	protected void defaultWorldPhysicsPreset()
	{
		dynamicsWorld.setGravity(new Vector3f(0f, 10f, 0f));
		WorldDamping = 0.0f;
	}

	
	protected void makeObstacles() {

	}

	protected void makeBorders() {
		CollisionShape wallShapePX = new BoxShape(new Vector3f(1f, Yborder,  Zborder));
		CollisionShape wallShapeNX = new BoxShape(new Vector3f(1f, Yborder,  Zborder));
		CollisionShape wallShapePY = new BoxShape(new Vector3f(Xborder, 1.0f,  Zborder));
		CollisionShape wallShapeNY = new BoxShape(new Vector3f( Xborder, 1.0f,  Zborder));
		CollisionShape wallShapePZ = new BoxShape(new Vector3f( Xborder,  Yborder, 1.0f));
		CollisionShape wallShapeNZ = new BoxShape(new Vector3f( Xborder,  Yborder, 1.0f));

		Transform wallTransformPX = new Transform();
		wallTransformPX.setIdentity();
		wallTransformPX.origin.set(Xborder, 0, 0);
		Transform wallTransformNX = new Transform();
		wallTransformNX.setIdentity();
		wallTransformNX.origin.set(-Xborder, 0, 0);

		Transform wallTransformPY = new Transform();
		wallTransformPY.setIdentity();
		wallTransformPY.origin.set(0, Yborder, 0);
		Transform wallTransformNY = new Transform();
		wallTransformNY.setIdentity();
		wallTransformNY.origin.set(0, -Yborder, 0);

		Transform wallTransformPZ = new Transform();
		wallTransformPZ.setIdentity();
		wallTransformPZ.origin.set(0,0,Zborder);
		Transform wallTransformNZ = new Transform();
		wallTransformNZ.setIdentity();
		wallTransformNZ.origin.set(0,0,-Zborder);

		BotMotionState myMotionState;
		RigidBodyConstructionInfo rbInfo;
		RigidBody body;
		
			// using motionstate is recommended, it provides interpolation capabilities, and
			// only synchronizes 'active' objects
		myMotionState = new BotMotionState(wallTransformPX);
		rbInfo = new RigidBodyConstructionInfo(0f, myMotionState, wallShapePX,new Vector3f(0, 0, 0));
		body = new RigidBody(rbInfo);
		body.setRestitution(1f);
		dynamicsWorld.addRigidBody(body);
		walls.add(new Entity3D(wallShapePX,body));
		
		myMotionState = new BotMotionState(wallTransformNX);
		rbInfo = new RigidBodyConstructionInfo(0f, myMotionState, wallShapeNX,new Vector3f(0, 0, 0));
		body = new RigidBody(rbInfo);
		body.setRestitution(1f);
		dynamicsWorld.addRigidBody(body);
		walls.add(new Entity3D(wallShapeNX,body));
		
		myMotionState = new BotMotionState(wallTransformPY);
		rbInfo = new RigidBodyConstructionInfo(0f, myMotionState, wallShapePY,new Vector3f(0, 0, 0));
		body = new RigidBody(rbInfo);
		body.setRestitution(1f);
		dynamicsWorld.addRigidBody(body);
		walls.add(new Entity3D(wallShapePY,body));
		
		myMotionState = new BotMotionState(wallTransformNY);
		rbInfo = new RigidBodyConstructionInfo(0f, myMotionState, wallShapeNY,new Vector3f(0, 0, 0));
		body = new RigidBody(rbInfo);
		body.setRestitution(1f);
		dynamicsWorld.addRigidBody(body);
		walls.add(new Entity3D(wallShapeNY,body));
		
		myMotionState = new BotMotionState(wallTransformPZ);
		rbInfo = new RigidBodyConstructionInfo(0f, myMotionState, wallShapePZ,new Vector3f(0, 0, 0));
		body = new RigidBody(rbInfo);
		body.setRestitution(1f);
		dynamicsWorld.addRigidBody(body);
		walls.add(new Entity3D(wallShapePZ,body));
		
		myMotionState = new BotMotionState(wallTransformNZ);
		rbInfo = new RigidBodyConstructionInfo(0f, myMotionState, wallShapeNZ,new Vector3f(0, 0, 0));
		body = new RigidBody(rbInfo);
		body.setRestitution(1f);
		dynamicsWorld.addRigidBody(body);
		walls.add(new Entity3D(wallShapeNZ,body));
		

	}

	protected void makeBot() {
		Class<?> clazz;
		try {
			clazz = Class.forName("evoagentsimulation.evoagent3dsimulator.bot." + botModel);
			Constructor<?> constructor = clazz.getConstructor(DynamicsWorld.class);
			bot = (Bot3DBody) constructor.newInstance(dynamicsWorld);			
			if(mind != null)
				bot.setMind(mind);
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
				| IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			System.out.println("bot 3D type not found " + botModel );
			bot = null;
		}
	}

	protected Vector3f generatePositionInBoundaries(float margin) {
		return new Vector3f(((float)Math.random()*2*(Xborder - margin)) - (Xborder - margin),
				((float)Math.random()*2*(Yborder - margin)) - (Yborder - margin),
				((float)Math.random()*2*(Zborder - margin)) - (Zborder - margin));
	}

	public void reset() {
    	super.reset();
		for(ControlFunction3D c : controlFunctions)
			c.reset();
		for(RewardFunction3D r : rewardFunctions)
			r.reset();
		if(bot!=null)
			bot.reset();
		if(mind!=null)
			mind.reset();
		clientResetScene();
	}
	
	public void exit() {

	}

	public void dumpWorld() {

	}

	public Bot3DBody getBot()
	{
		return bot;
	}
	
	public DynamicsWorld getWorld()
	{
		return dynamicsWorld;
	}

	public float getDeltaTimeMicroseconds() {
		// #ifdef USE_BT_CLOCK
		float dt = clock.getTimeMicroseconds();
		clock.reset();
		return dt;
	}
	
	public void clientResetScene() {
		int numObjects = 0;
		if (dynamicsWorld != null) {
			dynamicsWorld.stepSimulation(1f / 60f, 0);
			numObjects = dynamicsWorld.getNumCollisionObjects();
		}

		for (int i = 0; i < numObjects; i++) {
			CollisionObject colObj = dynamicsWorld.getCollisionObjectArray().getQuick(i);
			RigidBody body = RigidBody.upcast(colObj);
			if (body != null) {
				if (body.getMotionState() != null) {
					DefaultMotionState myMotionState =  (DefaultMotionState) body.getMotionState();
					myMotionState.graphicsWorldTrans.set(myMotionState.startWorldTrans);
					colObj.setWorldTransform(myMotionState.graphicsWorldTrans);
					colObj.setInterpolationWorldTransform(myMotionState.startWorldTrans);
					colObj.activate();
				}
				// removed cached contact points
				dynamicsWorld.getBroadphase().getOverlappingPairCache().cleanProxyFromPairs(colObj.getBroadphaseHandle(), dynamicsWorld.getDispatcher());

				body.setDamping(WorldDamping, WorldDamping);
				body = RigidBody.upcast(colObj);
				if (body != null && !body.isStaticObject()) {
					RigidBody.upcast(colObj).setLinearVelocity(new Vector3f(0f, 0f, 0f));
					RigidBody.upcast(colObj).setAngularVelocity(new Vector3f(0f, 0f, 0f));
				}
			}
		}
	}

	public void setBotModel(String botModel)
	{
		this.botModel = botModel;
	}
	
	public void setBotMind(EvoAgentMind mind)
	{
		this.mind = mind;
		if(bot !=null)
			bot.setMind(mind);
	}
}
