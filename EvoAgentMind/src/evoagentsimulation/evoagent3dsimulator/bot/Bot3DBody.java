/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.bot;

import java.util.HashMap;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

import evoagentmindelements.EvoAgentMind;
import evoagentsimulation.evoagent3dsimulator.BotMotionState;
import evoagentsimulation.evoagent3dsimulator.Entity3D;
import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;
import evoagentsimulation.evoagent3dsimulator.bot.elements.Actuator3D;
import evoagentsimulation.evoagent3dsimulator.bot.elements.Sensor3D;

public class Bot3DBody{
	private HashMap<String,Double> sensorsOut = new HashMap<String, Double>();
	public HashMap<String,Sensor3D> sensors = new HashMap<String,Sensor3D>();
	public HashMap<String,Actuator3D> actuators = new HashMap<String,Actuator3D>();
	public Transform currentWorldTrans = null;
	public Transform currentWorldTransInverse = null;
	public Matrix3f currentBasisTranspose = new Matrix3f();
	
	protected DynamicsWorld dynamicsWorld = null;
	private Entity3D entity = null;
	protected Vector3f frontVector = new Vector3f(0f,0f,1f);
	protected Vector3f upVector = new Vector3f(0f,-1f,0f);
	protected Vector3f currentFrontVector = new Vector3f(0f,0f,1f);
	protected Vector3f currentUpVector = new Vector3f(0f,-1f,0f);
	float mass = 1f;
	float startPosX = 0f;
	float startPosY = 0f;
	float startPosZ = 0f;
	float sizeX = 4f;
	float sizeY = 2f;
	float sizeZ = 4f;
	static int count = 0;
	public Bot3DBody()
	{}
	
	public Bot3DBody(DynamicsWorld world)
	{
		dynamicsWorld = world;
	}
	
	public void setWorld(DynamicsWorld world)
	{
		dynamicsWorld = world;
	}
	public void makeBody() {
		if (entity == null)
		{
			CollisionShape colShape = new BoxShape(new Vector3f(sizeX, sizeY, sizeZ));
			Transform startTransform = new Transform();
			startTransform.setIdentity();
			Vector3f localInertia = new Vector3f(0, 0, 0);
			colShape.calculateLocalInertia(mass, localInertia);		
			startTransform.origin.set(startPosX,startPosY,startPosZ);
			MotionState myMotionState = new BotMotionState(startTransform);
			RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, colShape,localInertia);
			RigidBody body = new RigidBody(rbInfo);
			body.setFriction(0.1f);
			body.setRestitution(0.2f);
			body.setDamping(SimulationEnvironment3D.WorldDamping, SimulationEnvironment3D.WorldDamping);
			body.setActivationState(RigidBody.DISABLE_DEACTIVATION );
			dynamicsWorld.addRigidBody(body);
			body.setActivationState(RigidBody.DISABLE_DEACTIVATION );
			entity = new Entity3D(colShape, body);
			computePositions();
		}
	}
	
	public void computePositions()
	{
		currentWorldTrans = ((BotMotionState)getEntity().getBody().getMotionState()).graphicsWorldTrans;
		currentBasisTranspose.set(currentWorldTrans.basis);
		currentBasisTranspose.transpose();
		//currentWorldTransInverse = new Transform();
		//currentWorldTransInverse.inverse(currentWorldTrans);
		//currentFrontVector = new Vector3f(frontVector);
		//currentBasisTranspose.transform(currentFrontVector);
		//currentUpVector = new Vector3f(upVector);
		//currentBasisTranspose.transform(currentUpVector);
		
	}
	
	public HashMap<String,Double> step(HashMap<String,Double> actuatorValues)
	{
		computePositions();
		
		if(actuatorValues != null)
			for(String k : actuatorValues.keySet())
				setNormalisedActuatorData(actuators.get(k), actuatorValues.get(k));

		for(String k : sensors.keySet())
				sensorsOut.put(k, sensors.get(k).getNormalizedValue());

		return sensorsOut;
	}
	
	public void step()
	{
		computePositions();
		for(Actuator3D a : actuators.values())
			a.autoStep();
		
		for(Sensor3D s : sensors.values())
			s.autoStep();
	}
	
	 protected void setNormalisedActuatorData(Actuator3D actuator3d, double parseDouble) {
		actuator3d.setNormalizedValue(parseDouble);
		actuator3d.step();
	}

	public void reset() {
		for(Sensor3D s : sensors.values())
			s.reset();
		for(Actuator3D a : actuators.values())
			a.reset();
	}

	public Entity3D getEntity() {
		return entity;
	}

	public void setStartPosition(float x, float y, float z) {
		startPosX = x;
		startPosY = y;
		startPosZ = z;
	}
	
	public Vector3f getFrontVector()
	{
		return frontVector;
	}

	public Vector3f getUpVector()
	{
		return upVector;
	}
	
	public Vector3f getCurrentFrontVector()
	{
		return currentFrontVector;
	}

	public Vector3f getCurrentUpVector()
	{
		return currentUpVector;
	}
	
	public void setMind(EvoAgentMind evoAgentMind) {
		for(String k : actuators.keySet())
			actuators.get(k).setMindModule(evoAgentMind.getActuator(k));		
		for(String k : sensors.keySet())
			sensors.get(k).setMindModule(evoAgentMind.getSensor(k));
	}	
}