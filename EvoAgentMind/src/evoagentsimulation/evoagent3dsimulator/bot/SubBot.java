/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.bot;

import javax.vecmath.Vector3f;

import com.bulletphysics.dynamics.DynamicsWorld;

import evoagentsimulation.evoagent3dsimulator.bot.elements.A3D_BiDirectionalThruster;
import evoagentsimulation.evoagent3dsimulator.bot.elements.S3D_ProximitySensor;
import evoagentsimulation.evoagent3dsimulator.bot.elements.S3D_TargetSensor;

public class SubBot extends Bot3DBody{
	
	public SubBot(DynamicsWorld wrld) {
			super(wrld);
			sizeZ = 4f;
			mass = 20f;
			sensors.put("S1",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,0,0),100f,this, wrld));		
			sensors.put("S2",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,0,0),100f,this, wrld));		
			sensors.put("S3",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,1f,0),100f,this, wrld));		
			sensors.put("S4",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,-1f,0),100f,this, wrld));		
			sensors.put("S5",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,1f,0),100f,this, wrld));		
			sensors.put("S6",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,-1f,0),100f,this, wrld));		
			sensors.put("S7",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,1f,0),100f,this, wrld));		
			sensors.put("S8",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,-1f,0),100f,this, wrld));		
			sensors.put("S9",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,0,1f),100f,this, wrld));		
			sensors.put("S10",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,0,-1f),100f,this, wrld));		
			sensors.put("S11",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,0,1f),100f,this, wrld));		
			sensors.put("S12",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,0,1f),100f,this, wrld));		
			sensors.put("S13",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,1f,1f),100f,this, wrld));		
			sensors.put("S14",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,-1f,1f),100f,this, wrld));		
			sensors.put("S15",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,1f,1f),100f,this, wrld));		
			sensors.put("S16",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,-1f,1f),100f,this, wrld));		
			sensors.put("S17",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,1f,1f),100f,this, wrld));		
			sensors.put("S18",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,-1f,1f),100f,this, wrld));		
			sensors.put("S19",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,0,-1f),100f,this, wrld));		
			sensors.put("S20",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,0,-1f),100f,this, wrld));		
			sensors.put("S21",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,1f,-1f),100f,this, wrld));		
			sensors.put("S22",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(0,-1f,-1f),100f,this, wrld));		
			sensors.put("S23",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,1f,-1f),100f,this, wrld));		
			sensors.put("S24",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(1f,-1f,-1f),100f,this, wrld));		
			sensors.put("S25",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,1f,-1f),100f,this, wrld));		
			sensors.put("S26",new S3D_ProximitySensor(new Vector3f(0,0,0),	new Vector3f(-1f,-1f,-1f),100f,this, wrld));		
			
			sensors.put("TARG_AZIM",new S3D_TargetSensor(new Vector3f(0,0,0),	new Vector3f(0,0,1f),this, S3D_TargetSensor.AZIMUTH,null));		
			sensors.put("TARG_ELEV",new S3D_TargetSensor(new Vector3f(0,0,0),	new Vector3f(0,0,1f),this, S3D_TargetSensor.ELEVATION,null));		
			sensors.put("TARG_BANK",new S3D_TargetSensor(new Vector3f(0,0,0),	new Vector3f(0,0,1f),this, S3D_TargetSensor.BANK,null));		
			
			actuators.put("M1",new A3D_BiDirectionalThruster(0.10f,new Vector3f(1.5f,0,0),new Vector3f(0,-1f,0),this));
			actuators.put("M2",new A3D_BiDirectionalThruster(0.10f,new Vector3f(-1.5f,0,0),new Vector3f(0,-1f,0),this));
			actuators.put("M3",new A3D_BiDirectionalThruster(0.10f,new Vector3f(0,0,1.5f),new Vector3f(0,-1f,0),this));
			
			actuators.put("M4",new A3D_BiDirectionalThruster(0.3f,new Vector3f(2f,0,2f),new Vector3f(-0.6f,0,1f),this));
			actuators.put("M5",new A3D_BiDirectionalThruster(0.3f,new Vector3f(-2f,0,2f),new Vector3f(0.6f,0,1f),this));
			actuators.put("M6",new A3D_BiDirectionalThruster(0.3f,new Vector3f(2f,0,-2f),new Vector3f(-0.6f,0,-1f),this));
			actuators.put("M7",new A3D_BiDirectionalThruster(0.3f,new Vector3f(-2f,0,-2f),new Vector3f(0.6f,0,-1f),this));
	}
}