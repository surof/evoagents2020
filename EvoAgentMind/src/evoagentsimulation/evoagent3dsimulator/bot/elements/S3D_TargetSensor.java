/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.bot.elements;

import javax.vecmath.Vector3f;

import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;
import evoagentsimulation.evoagent3dsimulator.experiments.elements.TargetObject;

public class S3D_TargetSensor extends Sensor3D{
	int axis = 1; 
	public static final int AZIMUTH = 1;
	public static final int ELEVATION = 2;
	public static final int BANK = 3;

	TargetObject target;
	
	public S3D_TargetSensor(Vector3f pos, Vector3f direc, Bot3DBody bod, int ax, TargetObject targ) {
		super(pos, direc, bod);
		axis = ax; 
		target = targ;
	}

	
	public Double getNormalizedValue() { 
		if(target != null)
		{
			if(axis == AZIMUTH)
			{
				Vector3f vec = new Vector3f(target.position);
				vec.sub(body.currentWorldTrans.origin);
				body.currentBasisTranspose.transform(vec);
				//System.out.println("AZIM :  "+(((Math.atan2(vec.x, vec.z))/(Math.PI*2.0))+0.5));
				return ((Math.atan2(vec.x, vec.z))/(Math.PI*2.0))+0.5;			
			}			
			if(axis == ELEVATION)
			{
				Vector3f vec = new Vector3f(target.position);
				vec.sub(body.currentWorldTrans.origin);
				body.currentBasisTranspose.transform(vec);
				//System.out.println("ELEV :  "+(((Math.atan2(vec.y, vec.z))/(Math.PI*2.0))+0.5));
				return ((Math.atan2(vec.y, vec.z))/(Math.PI*2.0))+0.5;			
			}			
			if(axis == BANK)
			{
				Vector3f vec = new Vector3f(target.position);
				vec.sub(body.currentWorldTrans.origin);
				body.currentBasisTranspose.transform(vec);
				//System.out.println("BANK :  "+(((Math.atan2(vec.x, vec.y))/(Math.PI*2.0))+0.5));
				return ((Math.atan2(vec.x, vec.y))/(Math.PI*2.0))+0.5;			
			}			
		}
		return 0.5;
	}
	

	public void setTarget(TargetObject targetObject) {
		target = targetObject;
	}

}
