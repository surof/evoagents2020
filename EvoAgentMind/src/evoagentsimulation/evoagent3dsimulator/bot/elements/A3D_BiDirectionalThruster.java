/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.bot.elements;

import javax.vecmath.Vector3f;

import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;

public class A3D_BiDirectionalThruster extends Actuator3D{

	public float maxForce = 0.5f;
	public Vector3f lastImpulse = null;
	

	public A3D_BiDirectionalThruster(float mForce, Vector3f pos , Vector3f direc, Bot3DBody bod)
	{
		super(pos, direc, bod);
		maxForce = mForce;
		direction.normalize();
	}

	public A3D_BiDirectionalThruster(Vector3f pos , Vector3f direc, Bot3DBody bod)
	{
		super(pos, direc, bod);
	}

	@Override
	public void step() {
		computeNormalizedValue();
		Vector3f impulse = new Vector3f(direction);
		Vector3f pos = new Vector3f(position);
		impulse.scale(value*maxForce);
		body.currentWorldTrans.basis.transform(pos);
		body.currentWorldTrans.basis.transform(impulse);
		lastImpulse = impulse;
		body.getEntity().getBody().applyImpulse(impulse,pos);
	}


	@Override
	public void setNormalizedValue(double val) {
		normalizedValue = val;
	}
	
	private void computeNormalizedValue() {		
		value = (((float)normalizedValue*2.0f)-1.0f)*maxForce;
		//System.out.println(value);
	}
}
