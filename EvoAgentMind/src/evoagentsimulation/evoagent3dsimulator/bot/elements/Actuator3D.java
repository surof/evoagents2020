/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.bot.elements;

import javax.vecmath.Vector3f;

import evoagentmindelements.modules.ActuatorModule;
import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;

public class Actuator3D {
	public Vector3f position, direction;
	public float value = 0.0f;
	public double normalizedValue = 0.0f;
	public Bot3DBody body;
	ActuatorModule actuator=null;
	
	public Actuator3D(Vector3f pos , Vector3f direc, Bot3DBody bod)
	{
		body = bod;
		position = pos;
		direction = direc;
	}

	public void setNormalizedValue(double val) {
		// TODO Auto-generated method stub
		
	}
	
	public double getNormalizedValue() {
		return normalizedValue;
	}

	public void step() {
		// TODO Auto-generated method stub
		
	}

	public void autoStep() {
		if(actuator!=null)
		{
			normalizedValue = actuator.getMotorValue();
			step();			
		}
	}
	
	public void reset() {
		// TODO Auto-generated method stub
		
	}	
	
	public void setMindModule(ActuatorModule actuator) {
		this.actuator = actuator;
	}	
}
