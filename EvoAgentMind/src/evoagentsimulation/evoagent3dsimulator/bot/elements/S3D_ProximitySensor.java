/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent3dsimulator.bot.elements;


import javax.vecmath.Vector3f;

import com.bulletphysics.collision.dispatch.CollisionWorld;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;

import evoagentsimulation.evoagent3dsimulator.bot.Bot3DBody;

public class S3D_ProximitySensor extends Sensor3D{
	public float MaxDistance;
	public Vector3f rayToVector = new Vector3f();
	CollisionWorld collisionWorld;

	public Vector3f spread11 = new Vector3f();
	public Vector3f spread12 = new Vector3f();
	public Vector3f spread21 = new Vector3f();
	public Vector3f spread22 = new Vector3f();
	
	float spreadFactor = 0.13f;
	
	public S3D_ProximitySensor(Vector3f pos , Vector3f direc, float maxDist, Bot3DBody bod, CollisionWorld cWrld)
	{
		super(pos, direc,bod);
		direc.normalize();
		MaxDistance = maxDist;
		rayToVector.scale(MaxDistance, direc);
		collisionWorld = cWrld;
		
		Vector3f perp1 = new Vector3f(direc);
		Vector3f perp2 = new Vector3f();
		if(perp1.x != 0)
			perp1.setX(-perp1.x);
		else if(perp1.y != 0)
			perp1.setY(-perp1.y);
		else if(perp1.z != 0)
			perp1.setZ(-perp1.z);
		perp1.add(new Vector3f(0.1f,0.1f,0.1f));
		
		perp1.cross(direc, perp1);
		perp2.cross(direc, perp1);
		perp1.normalize();
		perp2.normalize();
		perp1.scale(spreadFactor);
		perp2.scale(spreadFactor);
		spread11.add(direc);
		spread11.add(perp1);
		spread11.scale(maxDist);
		spread12.add(direc);
		spread12.sub(perp1);
		spread12.scale(maxDist);
		spread21.add(direc);
		spread21.add(perp2);
		spread21.scale(maxDist);
		spread22.add(direc);
		spread22.sub(perp2);
		spread22.scale(maxDist);
	}
	
	public void step()
	{
		
		Vector3f to =new Vector3f(rayToVector);
		Vector3f from = new Vector3f(position);
		body.currentWorldTrans.transform(to);
		body.currentWorldTrans.transform(from);
		ClosestRayResultCallback result = new ClosestRayResultCallback(from, to);
		collisionWorld.rayTest(from, to, result);
		value =  result.closestHitFraction;

		// spread
		to = new Vector3f(spread11);
		body.currentWorldTrans.transform(to);
		result = new ClosestRayResultCallback(from, to);
		collisionWorld.rayTest(from, to, result);
		if(result.closestHitFraction < value)
			value = result.closestHitFraction;

		
		to = new Vector3f(spread12);
		body.currentWorldTrans.transform(to);
		result = new ClosestRayResultCallback(from, to);
		collisionWorld.rayTest(from, to, result);
		if(result.closestHitFraction < value)
			value = result.closestHitFraction;

		
		to = new Vector3f(spread21);
		body.currentWorldTrans.transform(to);
		result = new ClosestRayResultCallback(from, to);
		collisionWorld.rayTest(from, to, result);
		if(result.closestHitFraction < value)
			value = result.closestHitFraction;

		
		to = new Vector3f(spread22);
		body.currentWorldTrans.transform(to);
		result = new ClosestRayResultCallback(from, to);
		collisionWorld.rayTest(from, to, result);
		if(result.closestHitFraction < value)
			value = result.closestHitFraction;

	}

	public Double getNormalizedValue() {
		step();
		return (double) value;
	}
	

	public Double getLastNormalizedValue() {
		return (double) value;
	}
}
