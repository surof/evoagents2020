/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.simulationlearning;

import java.util.HashMap;

import org.encog.ml.CalculateScore;
import org.encog.ml.MLMethod;
import org.encog.ml.MLRegression;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentmindelements.EvoAgentMindTemplate;
import evoagentmindelements.modules.NeuralNetworkModule;
import remote.RemoteSimulationEnvironmentComm;


//update plz
@Deprecated
public class SimScoreRemote implements CalculateScore {
	private int repeatCount = 1; 
	private int ticksPerEval = 100000; 
	private ThreadLocal<RemoteSimulationEnvironmentComm> remoteEnv = new ThreadLocal<RemoteSimulationEnvironmentComm>();
	private String skillName = "";
	EvoAgentMindTemplate mindTemplate;

	public SimScoreRemote(int repeatC, int ticksEval, String skilln, EvoAgentMindTemplate mTemplate) {
		repeatCount = repeatC;
		ticksPerEval = ticksEval;
		mindTemplate = mTemplate;
		skillName = skilln;
	}

	@Override
	public double calculateScore(MLMethod network) {
		ThreadLocal<ScoreCounter> score = new ThreadLocal<ScoreCounter>();
		ThreadLocal<SimulationInterruptFlag> interruptFlag = new ThreadLocal<SimulationInterruptFlag>();
		score.set(new ScoreCounter());
		interruptFlag.set(new SimulationInterruptFlag());
		ThreadLocal<HashMap<String,Double>> actuatorValues = new ThreadLocal<HashMap<String,Double>>();
		actuatorValues.set(null);
		remoteEnv.set(new RemoteSimulationEnvironmentComm());
		ThreadLocal<EvoAgentMind> mind = new ThreadLocal<EvoAgentMind>(){
			@Override
			protected EvoAgentMind initialValue()
			{
				return EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
			}
		};
		((NeuralNetworkModule)mind.get().getSkill(skillName)).setNetwork((MLRegression) network);
		remoteEnv.get().init();
		for(int i = 0; i < repeatCount; i++)
		{
			//System.out.println("rep num : " + i);
			remoteEnv.get().reset();
			mind.get().reset();
			interruptFlag.get().resetState();
			for(int j = 0 ; j < ticksPerEval && !interruptFlag.get().interrupted(); j++)
			{
				//System.out.println("tick num : " + j);
				actuatorValues.set( mind.get().doStep(remoteEnv.get().doStep(actuatorValues.get(), score.get(), interruptFlag.get())));
				//System.out.println(actuatorValues.toString());
			}
		}		
		remoteEnv.get().quit();
		//System.out.println("eval ended with score : " + score.get().getCurrentScore());
		return score.get().getCurrentScore();
	}

	@Override
	public boolean shouldMinimize() {
		return false;
	}

	@Override
	public boolean requireSingleThreaded() {
		return false;
	}
}