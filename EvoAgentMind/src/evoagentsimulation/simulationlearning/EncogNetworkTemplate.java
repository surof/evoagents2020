/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.simulationlearning;

import java.util.ArrayList;

import evoagentapp.EvoAgentAppDefines;

public class EncogNetworkTemplate{
	String type = EncogNetworkFactory.TAG_TYPE_BASIC;
	int numLayers = 2;
	String hiddenTransfer = "RELU";
	int in;
	int out;
	int hiddenNeuronAdded = 2;

	int[] convoIn;
	int[] convoOut;
	int[][][] convoLink;
	int[] convoBypass;
	
	public EncogNetworkTemplate(int in, int out,ArrayList<String> param)
	{
		this.in = in;
		this.out = out;
		ArrayList<Integer> convoIn = new ArrayList<>();
		ArrayList<Integer> convoOut= new ArrayList<>();
		ArrayList<ArrayList<ArrayList<Integer>>> convoLink= new ArrayList<>();
		ArrayList<Integer> convoBypass= new ArrayList<>();
		for(String l: param)
		{
			if(l.split(EvoAgentAppDefines.separator)[0].toUpperCase().equals(EncogNetworkFactory.TAG_PARAM_LAYERS))
				numLayers = Integer.parseInt(l.split(EvoAgentAppDefines.separator)[1]);
			if(l.split(EvoAgentAppDefines.separator)[0].toUpperCase().equals(EncogNetworkFactory.TAG_PARAM_HIDDEN_XFER))
				hiddenTransfer = l.split(EvoAgentAppDefines.separator)[1];
			if(l.split(EvoAgentAppDefines.separator)[0].toUpperCase().equals(EncogNetworkFactory.TAG_PARAM_HIDDEN_NEUR))
				hiddenNeuronAdded = Integer.parseInt(l.split(EvoAgentAppDefines.separator)[1]);
			if(l.split(EvoAgentAppDefines.separator)[0].toUpperCase().equals(EncogNetworkFactory.TAG_TYPE))
				type = l.split(EvoAgentAppDefines.separator)[1];
			if(l.split(EvoAgentAppDefines.separator)[0].toUpperCase().equals(EncogNetworkFactory.TAG_CONVO_LAYER))
			{
				convoIn.add(Integer.parseInt(l.split(EvoAgentAppDefines.separator)[1]));
				convoOut.add(Integer.parseInt(l.split(EvoAgentAppDefines.separator)[2]));
				convoLink.add(new ArrayList<ArrayList<Integer>>());
			}
			if(l.split(EvoAgentAppDefines.separator)[0].toUpperCase().equals(EncogNetworkFactory.TAG_CONVO_LINK))
			{	
				int lay = Integer.parseInt(l.split(EvoAgentAppDefines.separator)[1]);
				int neur = Integer.parseInt(l.split(EvoAgentAppDefines.separator)[2]);
				convoLink.get(lay).add(new ArrayList<Integer>());
				for(int i = 3; i < l.split(EvoAgentAppDefines.separator).length; i++)
				{
					convoLink.get(lay).get(neur).add(Integer.parseInt(l.split(EvoAgentAppDefines.separator)[i]));
				}					
			}
			if(l.split(EvoAgentAppDefines.separator)[0].toUpperCase().equals(EncogNetworkFactory.TAG_CONVO_BYPASS))
			{	
				for(int i = 1; i < l.split(EvoAgentAppDefines.separator).length; i++)
				{
					convoBypass.add(Integer.parseInt(l.split(EvoAgentAppDefines.separator)[i]));
				}					
			}			

		}
		if ((convoIn.size() == 0 || convoIn.size() != convoLink.size()) && type.equals(EncogNetworkFactory.TAG_TYPE_CNN))
		{
			System.out.println("ENCOG NETWORK DEFINITION ERROR : switching CNN to basic");
			type = EncogNetworkFactory.TAG_TYPE_BASIC;
		}
		else
		{
			this.convoIn = arrayListToTab(convoIn);
			this.convoOut = arrayListToTab(convoOut);
			this.convoBypass = arrayListToTab(convoBypass);
			this.convoLink = new int[convoLink.size()][][];
			for(int i = 0 ; i < convoLink.size() ; i++)
			{
				this.convoLink[i] = new int[convoLink.get(i).size()][];	
				for(int j = 0 ; j < convoLink.get(i).size() ; j++)
				{
					this.convoLink[i][j] = arrayListToTab(convoLink.get(i).get(j));
				}
			}
		}
	}
	
	private int[] arrayListToTab(ArrayList<Integer> shit)
	{
		int[] tab = new int[shit.size()];
		for(int i = 0 ; i < shit.size() ; i++)
		{
			tab[i] = shit.get(i);
		}	
		return tab;
	}
}
