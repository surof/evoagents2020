/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.simulationlearning.MyNEAT;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import org.encog.neural.neat.training.NEATGenome;
import org.encog.neural.neat.training.NEATLinkGene;
import org.encog.neural.neat.training.NEATNeuronGene;

//NEATO : an attempt to also mutate the activation functions ... not very successful

public class NEATOGenome extends NEATGenome implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Create a new genome with the specified connection density. This
	 * constructor is typically used to create the initial population.
	 * @param rnd Random number generator.
	 * @param pop The population.
	 * @param inputCount The input count.
	 * @param outputCount The output count.
	 * @param connectionDensity The connection density.
	 */
	public NEATOGenome(final Random rnd, final NEATOPopulation pop,
			final int inputCount, final int outputCount,
			double connectionDensity) {
		
		super(rnd,pop, inputCount, outputCount, connectionDensity);
	}

	public NEATOGenome(List<NEATNeuronGene> neurons, List<NEATLinkGene> links, int inputCount, int outputCount) {
		super(neurons,links,inputCount,outputCount);
	}

	public NEATOGenome(NEATGenome other) {
		super(other);
	}

	public NEATOGenome() {
		super();
	}
}
