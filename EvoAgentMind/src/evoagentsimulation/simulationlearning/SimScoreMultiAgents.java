/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.simulationlearning;

import java.io.Serializable;
import java.util.ArrayList;

import org.encog.ml.MLMethod;
import org.encog.ml.MLRegression;
import org.encog.util.obj.SerializeObject;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindTemplate;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D.ObstaclePos;

public class SimScoreMultiAgents extends SimScore {
	private ArrayList<ArrayList<ObstaclePos>> obstaclesData = null;
	private ArrayList<String> botModels;
	ArrayList<EvoAgentMindTemplate> mindTemplates;
	
	public SimScoreMultiAgents(ArrayList<String> botMods, ArrayList<ArrayList<ObstaclePos>> od, int repeatC, int ticksEval, String envN,String skilln, ArrayList<EvoAgentMindTemplate> mTemplate) {
		super(repeatC,ticksEval,envN,skilln);
		envClasspath = EvoAgentAppDefines.classpath2DMultiExp;
		botModels = botMods;
		obstaclesData = od;
		mindTemplates = mTemplate;
	}

	protected void initEnvironement()
	{
		((SimulationEnvironment2DMultiBot) environment.get()).setMindTemplates(mindTemplates);
		environment.get().setBotModel(botModels);
		environment.get().init();
		if(environment.get().getHasObstacles() && obstaclesData != null && obstaclesData.size() < 1)
			((SimulationEnvironment2D)environment.get()).generateAllObstaclesParameters(obstaclesData, repeatCount);
	}
	
	synchronized
	protected void setLearningFunction(MLMethod func) {
		for(EvoAgentMind mind :((SimulationEnvironment2DMultiBot) environment.get()).getTeamMinds(0))
			(mind.getSkill(learningSkill)).setInternalFunction((MLRegression)SerializeObject.serializeClone((Serializable) func));
	}

	protected void preRepeatOps(int iter) {
		if(obstaclesData!=null && environment.get().getHasObstacles())
			((SimulationEnvironment2D)environment.get()).loadObstacles(obstaclesData.get(iter));
	}
}