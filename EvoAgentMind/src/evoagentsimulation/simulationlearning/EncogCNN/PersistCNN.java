/*
 * Encog(tm) Core v3.4 - Java Version
 * http://www.heatonresearch.com/encog/
 * https://github.com/encog/encog-java-core
 
 * Copyright 2008-2020 Heaton Research, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *   
 * For more information on Heaton Research copyrights, licenses 
 * and trademarks visit:
 * http://www.heatonresearch.com/copyright
 */
package evoagentsimulation.simulationlearning.EncogCNN;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.encog.engine.network.activation.ActivationFunction;
import org.encog.neural.flat.FlatNetwork;
import org.encog.neural.networks.BasicNetwork;
import org.encog.persist.EncogFileSection;
import org.encog.persist.EncogPersistor;
import org.encog.persist.EncogReadHelper;
import org.encog.persist.EncogWriteHelper;
import org.encog.persist.PersistConst;
import org.encog.persist.PersistError;
import org.encog.util.csv.CSVFormat;
import org.encog.util.csv.NumberList;

/**
 * Persist a CNN network.
 * 
 * ConvolutionLayer class describes a kernel/layer, several layers can be chained
 * the output of the last layer is fed to a BasicNetwork along with inputs on the bypass list
 * 
 */

public class PersistCNN implements EncogPersistor {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFileVersion() {
		return 1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPersistClassString() {
		return "ConvolutionalNetwork";
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public Object read(final InputStream is) {
		final ConvolutionalNetwork cnn = new ConvolutionalNetwork();
		final BasicNetwork basic = new BasicNetwork();
		final FlatNetwork flat = new FlatNetwork();
		final EncogReadHelper in = new EncogReadHelper(is);
		EncogFileSection section;

		while ((section = in.readNextSection()) != null) {
			if (section.getSectionName().equals("CONVOLUTION")
					&& section.getSubSectionName().equals(ConvolutionalNetwork.TAG_SEC_CONVO_PARAM)) {
				final Map<String, String> params = section.parseParams();
				cnn.makeConvolutionLayers(EncogFileSection.parseIntArray(params,
						ConvolutionalNetwork.TAG_CONVO_INPUT_SIZES),EncogFileSection.parseIntArray(params,
								ConvolutionalNetwork.TAG_CONVO_OUTPUT_SIZES));
				cnn.setBypassLinks(EncogFileSection.parseIntArray(params,ConvolutionalNetwork.TAG_CONVO_BYPASS_LINKS));
			}
			if (section.getSectionName().equals("CONVOLUTION")
					&& section.getSubSectionName().equals(ConvolutionalNetwork.TAG_SEC_CONVO_LINKS)) {
				for (final String line : section.getLines()) {
					cnn.setConvoLink(Integer.parseInt(line.split("::")[0]), Integer.parseInt(line.split("::")[1]), NumberList.fromListInt(CSVFormat.EG_FORMAT, line.split("::")[2]));
				}
			}
			if (section.getSectionName().equals("CONVOLUTION")
					&& section.getSubSectionName().equals(ConvolutionalNetwork.TAG_SEC_CONVO_WEIGHTS)) {
				for (final String line : section.getLines()) {
					cnn.setConvoWeights(Integer.parseInt(line.split("::")[0]), NumberList.fromList(CSVFormat.EG_FORMAT, line.split("::")[1]));
				}
			}
			if (section.getSectionName().equals("CONVOLUTION") && section.getSubSectionName().equals(ConvolutionalNetwork.TAG_SEC_CONVO_ACTIVATION)) 
			{
				int indexC = 0;
				for (final String line : section.getLines()) 
				{
					ActivationFunction af = null;
					final List<String> cols = EncogFileSection.splitColumns(line);					
					// if this is a class name with a path, then do not default to inside of the Encog package.
					String name;
					if( cols.get(0).indexOf('.')!=-1 ) {
						name = cols.get(0);
					} else {
						name = "org.encog.engine.network.activation."
								+ cols.get(0);
					}					
					try {
						final Class<?> clazz = Class.forName(name);
						af = (ActivationFunction) clazz.newInstance();
					} catch (final ClassNotFoundException e) {
						throw new PersistError(e);
					} catch (final InstantiationException e) {
						throw new PersistError(e);
					} catch (final IllegalAccessException e) {
						throw new PersistError(e);
					}
					for (int i = 0; i < af.getParamNames().length; i++) {
						af.setParam(i,
								CSVFormat.EG_FORMAT.parse(cols.get(i + 1)));
					}
					cnn.setConvoActivationFunction(indexC, af);
					indexC++;
				}
			}
			if (section.getSectionName().equals("BASIC")
					&& section.getSubSectionName().equals("PARAMS")) {
				final Map<String, String> params = section.parseParams();
				basic.getProperties().putAll(params);
			}
			if (section.getSectionName().equals("BASIC")
					&& section.getSubSectionName().equals("NETWORK")) {
				final Map<String, String> params = section.parseParams();

				flat.setBeginTraining(EncogFileSection.parseInt(params,
						BasicNetwork.TAG_BEGIN_TRAINING));
				flat.setConnectionLimit(EncogFileSection.parseDouble(params,
						BasicNetwork.TAG_CONNECTION_LIMIT));
				flat.setContextTargetOffset(EncogFileSection.parseIntArray(
						params, BasicNetwork.TAG_CONTEXT_TARGET_OFFSET));
				flat.setContextTargetSize(EncogFileSection.parseIntArray(
						params, BasicNetwork.TAG_CONTEXT_TARGET_SIZE));
				flat.setEndTraining(EncogFileSection.parseInt(params,
						BasicNetwork.TAG_END_TRAINING));
				flat.setHasContext(EncogFileSection.parseBoolean(params,
						BasicNetwork.TAG_HAS_CONTEXT));
				flat.setInputCount(EncogFileSection.parseInt(params,
						PersistConst.INPUT_COUNT));
				flat.setLayerCounts(EncogFileSection.parseIntArray(params,
						BasicNetwork.TAG_LAYER_COUNTS));
				flat.setLayerFeedCounts(EncogFileSection.parseIntArray(params,
						BasicNetwork.TAG_LAYER_FEED_COUNTS));
				flat.setLayerContextCount(EncogFileSection.parseIntArray(
						params, BasicNetwork.TAG_LAYER_CONTEXT_COUNT));
				flat.setLayerIndex(EncogFileSection.parseIntArray(params,
						BasicNetwork.TAG_LAYER_INDEX));
				flat.setLayerOutput(section.parseDoubleArray(params,
						PersistConst.OUTPUT));
				flat.setLayerSums(new double[flat.getLayerOutput().length]);
				flat.setOutputCount(EncogFileSection.parseInt(params,
						PersistConst.OUTPUT_COUNT));
				flat.setWeightIndex(EncogFileSection.parseIntArray(params,
						BasicNetwork.TAG_WEIGHT_INDEX));
				flat.setWeights(section.parseDoubleArray(params,
						PersistConst.WEIGHTS));
				flat.setBiasActivation(section.parseDoubleArray(
						params, BasicNetwork.TAG_BIAS_ACTIVATION));
			} else if (section.getSectionName().equals("BASIC")
					&& section.getSubSectionName().equals("ACTIVATION")) {
				int index = 0;

				flat.setActivationFunctions(new ActivationFunction[flat
						.getLayerCounts().length]);

				for (final String line : section.getLines()) {
					ActivationFunction af = null;
					final List<String> cols = EncogFileSection
							.splitColumns(line);
					
					// if this is a class name with a path, then do not default to inside of the Encog package.
					String name;
					if( cols.get(0).indexOf('.')!=-1 ) {
						name = cols.get(0);
					} else {
						name = "org.encog.engine.network.activation."
								+ cols.get(0);
					}
					
					try {
						final Class<?> clazz = Class.forName(name);
						af = (ActivationFunction) clazz.newInstance();
					} catch (final ClassNotFoundException e) {
						throw new PersistError(e);
					} catch (final InstantiationException e) {
						throw new PersistError(e);
					} catch (final IllegalAccessException e) {
						throw new PersistError(e);
					}

					for (int i = 0; i < af.getParamNames().length; i++) {
						af.setParam(i,
								CSVFormat.EG_FORMAT.parse(cols.get(i + 1)));
					}

					flat.getActivationFunctions()[index++] = af;
				}
			}
		}
		basic.getStructure().setFlat(flat);
		basic.updateProperties();
		cnn.setFullNetwork(basic);
		return cnn;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void save(final OutputStream os, final Object obj) {
		final EncogWriteHelper out = new EncogWriteHelper(os);
		final ConvolutionalNetwork cnn = (ConvolutionalNetwork) obj;
		//cnn stuff
		out.addSection("CONVOLUTION");
		out.addSubSection(ConvolutionalNetwork.TAG_SEC_CONVO_PARAM);
		out.writeProperty(ConvolutionalNetwork.TAG_CONVO_INPUT_SIZES,cnn.getConvoInputSizes());
		out.writeProperty(ConvolutionalNetwork.TAG_CONVO_OUTPUT_SIZES,cnn.getConvoOutputSizes());
		out.writeProperty(ConvolutionalNetwork.TAG_CONVO_BYPASS_LINKS,cnn.getBypassLinks());
				
		out.addSubSection(ConvolutionalNetwork.TAG_SEC_CONVO_LINKS);
		StringBuilder temp = new StringBuilder();
		for(int lay = 0 ; lay < cnn.getConvoLink().length; lay++)
		{
			for(int neur = 0 ; neur < cnn.getConvoLink()[lay].length; neur++)
			{
				NumberList.toListInt(CSVFormat.EG_FORMAT, temp, cnn.getConvoLink()[lay][neur]);
				out.addLine(lay+"::"+neur+"::"+temp);
			}
		}
			
		out.addSubSection(ConvolutionalNetwork.TAG_SEC_CONVO_WEIGHTS);
		for(int lay = 0 ; lay < cnn.getConvoLayers().size(); lay++)
		{
			NumberList.toList(CSVFormat.EG_FORMAT, temp, cnn.getConvoLayers().get(lay).getWeights());
			out.addLine(lay+"::"+temp);
		}
		
		out.addSubSection(ConvolutionalNetwork.TAG_SEC_CONVO_ACTIVATION);

		for(int lay = 0 ; lay < cnn.getConvoLayers().size(); lay++)
		{
			ActivationFunction af = cnn.getConvoLayers().get(lay).getActivationFunction();
			String sn = af.getClass().getSimpleName();
			// if this is an Encog class then only add the simple name, so it works with C#
			if( sn.startsWith("org.encog.") ) {
				out.addColumn(sn);
			} else {
				out.addColumn(af.getClass().getName());
			}
			for (int i = 0; i < af.getParams().length; i++) {
				out.addColumn(af.getParams()[i]);
			}
			out.writeLine();
		}
		
		
		/// basic stuff
		final BasicNetwork full = cnn.getFullNetwork();
		final FlatNetwork flat = full.getStructure().getFlat();
		out.addSection("BASIC");
		out.addSubSection("PARAMS");
		out.addProperties(full.getProperties());
		out.addSubSection("NETWORK");

		out.writeProperty(BasicNetwork.TAG_BEGIN_TRAINING,
				flat.getBeginTraining());
		out.writeProperty(BasicNetwork.TAG_CONNECTION_LIMIT,
				flat.getConnectionLimit());
		out.writeProperty(BasicNetwork.TAG_CONTEXT_TARGET_OFFSET,
				flat.getContextTargetOffset());
		out.writeProperty(BasicNetwork.TAG_CONTEXT_TARGET_SIZE,
				flat.getContextTargetSize());
		out.writeProperty(BasicNetwork.TAG_END_TRAINING, flat.getEndTraining());
		out.writeProperty(BasicNetwork.TAG_HAS_CONTEXT, flat.getHasContext());
		out.writeProperty(PersistConst.INPUT_COUNT, flat.getInputCount());
		out.writeProperty(BasicNetwork.TAG_LAYER_COUNTS, flat.getLayerCounts());
		out.writeProperty(BasicNetwork.TAG_LAYER_FEED_COUNTS,
				flat.getLayerFeedCounts());
		out.writeProperty(BasicNetwork.TAG_LAYER_CONTEXT_COUNT,
				flat.getLayerContextCount());
		out.writeProperty(BasicNetwork.TAG_LAYER_INDEX, flat.getLayerIndex());
		out.writeProperty(PersistConst.OUTPUT, flat.getLayerOutput());
		out.writeProperty(PersistConst.OUTPUT_COUNT, flat.getOutputCount());
		out.writeProperty(BasicNetwork.TAG_WEIGHT_INDEX, flat.getWeightIndex());
		out.writeProperty(PersistConst.WEIGHTS, flat.getWeights());
		out.writeProperty(BasicNetwork.TAG_BIAS_ACTIVATION,
				flat.getBiasActivation());
		out.addSubSection("ACTIVATION");
		for (final ActivationFunction af : flat.getActivationFunctions()) {
			String sn = af.getClass().getSimpleName();
			// if this is an Encog class then only add the simple name, so it works with C#
			if( sn.startsWith("org.encog.") ) {
				out.addColumn(sn);
			} else {
				out.addColumn(af.getClass().getName());
			}
			for (int i = 0; i < af.getParams().length; i++) {
				out.addColumn(af.getParams()[i]);
			}
			out.writeLine();
		}

		out.flush();
	}

}
