/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.simulationlearning;

import org.encog.ml.MLMethod;
import org.encog.ml.MLRegression;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentmindelements.EvoAgentMindTemplate;
import evoagentmindelements.modules.NeuralNetworkModule;
import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;

public class SimScore3D extends SimScore {
	private String botModel = "";
	EvoAgentMindTemplate mindTemplate;
	ThreadLocal<EvoAgentMind> mind = new ThreadLocal<EvoAgentMind>(){
        @Override
        protected EvoAgentMind initialValue()
        {
            return EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
        }
    };
	
	public SimScore3D(String botMod, int repeatC, int ticksEval, String envN,String skilln, EvoAgentMindTemplate mTemplate) {
		super(repeatC,ticksEval,envN,skilln);
		envClasspath = EvoAgentAppDefines.classpath3D;
		botModel= botMod;
		mindTemplate = mTemplate;
	}

	protected void initEnvironement()
	{
		environment.get().setBotModel(botModel);
		environment.get().init();
	}
	
	synchronized
	protected void setLearningFunction(MLMethod func) {
		
	    mind.get().getSkill(learningSkill).setInternalFunction((MLRegression) func);
	    ((SimulationEnvironment3D)environment.get()).setBotMind(mind.get());
	}
}