/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.bot;

import java.util.HashMap;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import evoagentsimulation.evoagent2dsimulator.bot.elements.Actuator;
import evoagentsimulation.evoagent2dsimulator.bot.elements.Sensor;

public class BotBody {
	private boolean active = true;
	public double size = 1.0;
	public String label = "";
	public int ID = 0;
	public Body body;
	public World world;
	private HashMap<String,Double> sensorsOut = new HashMap<String, Double>();
	public HashMap<String,Sensor> sensors = new HashMap<String,Sensor>();
	public HashMap<String,Actuator> actuators = new HashMap<String,Actuator>();
	public int contactCounter = 0;

	FixtureDef sd;
	BodyDef bd;
	
	public BotBody()
	{
		
	}
	
	public HashMap<String,Double> step(HashMap<String,Double> actuatorValues)
	{
		if(actuatorValues != null)
			for(String k : actuatorValues.keySet())
				setNormalisedActuatorData(actuators.get(k), actuatorValues.get(k));

		
		for(String k : sensors.keySet())
				sensorsOut.put(k, sensors.get(k).getNormalizedValue());
		//System.out.println("end bot step");
		return sensorsOut;
	}
	
	 protected void setNormalisedActuatorData(Actuator actuator, double parseDouble) {
		 if(actuator != null)
		 {
				actuator.setNormalizedValue(parseDouble);
				actuator.step();			 
		 }
	}

	public World getWorld() {
		return world;
	}


	public void reset() {
		for(Sensor s : sensors.values())
			s.reset();
		for(Actuator a : actuators.values())
			a.reset();
	}
	
	public void registerBotToWorld(World w) {
		world = w;
		body = world.createBody(bd);
		sd.userData = this;
		body.createFixture(sd);
	}
	
	public void removeBotFromWorld(World w) {
		world = null;
		w.destroyBody(this.body);
	}
	
	public void initBotPosition(Vec2 startPos,double startAngle)
	{
		bd.position.set(startPos.x,startPos.y);
		bd.angle = (float)startAngle;
	}
	
	public void setPosition(Vec2 botStartPos, double double1) {
		body.setTransform(botStartPos, (float)double1);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setAngle(float Angle) {
		bd.angle = Angle;
	}	
}
