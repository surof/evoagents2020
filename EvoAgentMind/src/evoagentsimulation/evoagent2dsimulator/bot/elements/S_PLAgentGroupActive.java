/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.bot.elements;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;

public class S_PLAgentGroupActive extends S_PointListener{
	int IDToTrack = SimulationEnvironment2DMultiBot.NOTEAM_ID;
	
	public S_PLAgentGroupActive(Vec2 lp, float la, BotBody2D b, SimulationEnvironment2DMultiBot simEnv) {
		super(lp, la, b, simEnv);
	}

	public S_PLAgentGroupActive(Vec2 lp, float la, BotBody2D b, SimulationEnvironment2DMultiBot simEnv, double maxDist) {
		super(lp, la, b, simEnv, maxDist);
	}

	public boolean updateTarget(boolean sigin) {
		if (simEnvironment != null) {
			computeWorldPosAndAngle();
			if (signalUpdater == sigin) {
				targetPosition = null;
				boolean trig = false;
				double x = 0;
				double y = 0;
				double count = 0;
				signalUpdater = !signalUpdater;
				for (int i = 0; i < ((SimulationEnvironment2DMultiBot) simEnvironment).getBots().size(); i++) {
					if (i == IDToTrack) {
						for (BotBody2D B : ((SimulationEnvironment2DMultiBot) simEnvironment).getBots().get(i))
							if (maxDistance< 0 || MathUtils.distance(B.body.getPosition(), bot.body.getPosition()) <= maxDistance) {
								trig = true;
								x+=B.body.getPosition().x;
								y+=B.body.getPosition().y;
								count++;
							}
					}
				}
				if(trig)
				{
					targetPosition = new Vec2((float)(x/count),(float)(y/count));
				}
			}
		}
		return signalUpdater;
	}
	
	public void setIDToTrack(int id)
	{
		IDToTrack = id;
	}
}
