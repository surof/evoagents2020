/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.bot.elements;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;

public class A_GunCharge extends Actuator {
	Body obj = null;
	double ChargeSpeed = 1.0;
	double ChargeLevel = 0.0;
	
	public A_GunCharge(Vec2 lp, float la, BotBody2D b, double chargeSp) {
		super(lp, la, b);
		ChargeSpeed = chargeSp;
	}

	@Override
	public void step() {
		if(normalizedValue>0.5)
			ChargeLevel+= ChargeSpeed;
		else
			ChargeLevel-= ChargeSpeed;
		ChargeLevel = Math.min(100, Math.max(0, ChargeLevel));		
	}
	
	@Override
	public void reset()
	{
		ChargeLevel = 0.0;
	}

	public boolean isCharged() {
		if (ChargeLevel >=100)
			return true;
		return false;
	}

	public void empty() {
		ChargeLevel = 0.0;
	}
	
}
