/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.bot.elements;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBotCTF;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.worldElements.Projectile;

public class S_PLProjectileActive extends S_PointListener{

	public S_PLProjectileActive(Vec2 lp, float la, BotBody2D b, SimulationEnvironment2DMultiBotCTF simEnv, double maxDist) {
		super(lp, la, b, simEnv, maxDist);
	}

	public boolean updateTarget(boolean sigin) {
		if (simEnvironment != null) {
			computeWorldPosAndAngle();
			double ndist = maxDistance;
			double tmpDist;
			if (signalUpdater == sigin) {
				targetPosition = null;
				signalUpdater = !signalUpdater;
				for (int i = 0; i < ((SimulationEnvironment2DMultiBotCTF)simEnvironment).getProjectiles().size(); i++) {
					if (i != bot.ID) {
						for (Projectile p : ((SimulationEnvironment2DMultiBotCTF)simEnvironment).getProjectiles().get(i))
							if ((tmpDist = MathUtils.distance(p.body.getPosition(), bot.body.getPosition())) <= ndist || ndist < 0) {
								ndist = tmpDist;
								targetPosition = p.body.getPosition();
							}
					}
				}
			}
		}
		return signalUpdater;
	}
}
