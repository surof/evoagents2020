/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.bot.elements;


import org.jbox2d.common.Vec2;

import evoagentapp.EvoAgentAppDefines;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.Flag;

public class S_FlagCarry extends Sensor{
	private Flag Flag = null;
	double catchDistance = 2;
	RewardFunction rw = null;
	
	public S_FlagCarry(Vec2 lp, float la, BotBody2D b, double catchDist) {
		super(lp, la, b);
		catchDistance = catchDist;
	}

	public double getValue() {
		computeWorldPosAndAngle();
		if(Flag != null && normalizedValue == EvoAgentAppDefines.doubleBooleanFalse && Flag.carry == SimulationEnvironment2DMultiBot.NOTEAM_ID  && new Vec2(Flag.getPosition().x-worldPosition.x,Flag.getPosition().y-worldPosition.y).length() < catchDistance)
		{
			normalizedValue = EvoAgentAppDefines.doubleBooleanTrue;
			Flag.pickUpFlag(bot.body.getPosition(), bot.ID);
		}			
		return normalizedValue;
	}

	@Override
	public double getNormalizedValue() {
		return getValue();
	}
	
	public void setFlag(Flag f)
	{
		Flag = f;
	}

	public void setValue(double i) {
		normalizedValue = i;
	}

	public void dropFlag() {
		if(Flag != null) 
			Flag.dropFlag();
		normalizedValue = EvoAgentAppDefines.doubleBooleanFalse;
	}
	
	public void setRewardFunction(RewardFunction rf)
	{
		rw = rf;
	}
	
	public void resetFlag()
	{
		if(Flag != null) 
			Flag.returnToBase();
		normalizedValue = EvoAgentAppDefines.doubleBooleanFalse;
	}
}
