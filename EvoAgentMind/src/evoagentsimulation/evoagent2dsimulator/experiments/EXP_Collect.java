/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectDetector;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLZoneActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosingOnTargetVariableReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.DynamicWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

public class EXP_Collect extends SimulationEnvironment2DSingleBot {

	private TriggerZone dz ;
	private TargetObject to ;
	private RW_ManualReward manualReward;
	
	public EXP_Collect()
	{
		super();
		this.name = "Collect";
		hasObstacles = true;
		makeCircleWorld = true;
	}
	
	public void init() 
	{
		super.init();
		
		botStartPos = new Vec2(-00.5f,-0.0f);		
		makeBot();
		int WORLD_SIZE = 250;
		getCorners()[0] = new Vec2(-WORLD_SIZE , WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
	    minObstacleSize = 2.2;
	    maxObstacleSizeVariability = 4.0;
	    maxObstacleSpacingVariability = 12.0;
	    obstacleSpacing = 48.0;

		manualReward = new RW_ManualReward( getBot(), 0.0);
		rewardFunctions.add(manualReward);
		controlFunctions.add(new CF_NextOnTimeout(getBot(),this, 20000));
		dz = new TriggerZone(new Vec2(-50,-50), 0,10);
		((S_ZonePresence)getBot().sensors.get("SENSDZ")).setTarget(dz);
		((S_PLZoneActive)getBot().sensors.get("ACTDZ")).setTarget(dz);
		getWorldElements().add(dz);
		to = new TargetObject(new Vec2(-20,-20), (float)(Math.PI/4), 1.0f);
		((S_ObjectDetector)getBot().sensors.get("SENSOBJ")).setTarget(to);
		((S_PLObjectListActive)getBot().sensors.get("ACTOBJA")).setTarget(to);
		getWorldElements().add(to);
		rewardFunctions.add(new RW_ClosingOnTargetVariableReward( getBot(), 0.0005, 2.0,to));
		rewardFunctions.add(new RW_ClosingOnTargetVariableReward( getBot(), 0.0005, 2.0,dz));
		makeWorld();
		to.registerToWorld(getWorld());
		getBot().registerBotToWorld();
		posTargetObject(to);
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();
		if(dz.isPointInZone(to.getWorldPosition())&&getBot().actuators.get("EMAG").normalizedValue<0.5)
		{
			posTargetObject(to);
			for(RewardFunction r: rewardFunctions)
				r.reset();
			manualReward.addToCurrentValue(300);
		}
	}
	
	private void posTargetObject(DynamicWorldElement obj) {
		Vec2 pos = new Vec2(generatePositionInBoundaries());
		while(!checkElementPositionConficts(pos,50.0f) || !checkObstaclePositionConficts(pos,5.0f))
			pos.set(generatePositionInBoundaries());
		obj.setWorldPosition(pos.x,pos.y);
		obj.body.setLinearVelocity(new Vec2(0,0));
		obj.body.setAngularVelocity(0);
	}

	@Override
	public void reset()
	{
		super.reset();
		posTargetObject(to);
		for(RewardFunction r: rewardFunctions)
			r.reset();
	}
}
