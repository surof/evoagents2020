/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import org.jbox2d.common.Vec2;

import Utils.Files;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectDetector;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLZoneActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollisionAndTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.DynamicWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;
import evoagentsimulation.simulationlearning.ScoreCounter;
import evoagentsimulation.simulationlearning.SimulationInterruptFlag;

// an example of a benchmark environment
// use postRepetitionOps an postRunOps
public class EXP_CollectBenchmark extends SimulationEnvironment2DSingleBot {

	private TriggerZone dz ;
	private TargetObject to ;
	int tickLimit = 20000;
	
	int tickCount = 0;
	
	double averageTickCount = 0.0;
	double averageGoal = 0.0;
	
	int episode = 1;

	BigDecimal totalReward = new BigDecimal(0);
	BigDecimal tickPercent = new BigDecimal(0);
	
	private RW_ManualReward manualReward;
	
	PrintWriter GoalWriter;
	PrintWriter TickWriter;
	
	public EXP_CollectBenchmark()
	{
		super();
		this.name = "Collect_benchmark";
		hasObstacles = true;
	}
	
	@Override
	public void init() 
	{
		super.init();
		getCorners()[0] = new Vec2(-50.0f, 50.0f);
		getCorners()[1] = new Vec2(50.0f, 50.0f);
		getCorners()[2] = new Vec2(50.0f, -50.0f);
		getCorners()[3] = new Vec2(-50.0f, -50.0f);
		botStartPos = new Vec2(-35.7f,-35.0f);	
		
		makeBot();

		controlFunctions.add(new CF_NextOnCollisionAndTimeout(getBot(),this, 30000));
		
		dz = new TriggerZone(new Vec2(-35,-35), 0, 10);
		((S_ZonePresence)getBot().sensors.get("SENSDZ")).setTarget(dz);
		((S_PLZoneActive)getBot().sensors.get("ACTDZ")).setTarget(dz);
		worldElements.add(dz);
		
		to = new TargetObject(new Vec2(-20,-20), (float)(Math.PI/4), 1.0f);
		((S_ObjectDetector)getBot().sensors.get("SENSOBJ")).setTarget(to);
		((S_PLObjectListActive)getBot().sensors.get("ACTOBJA")).setTarget(to);
		worldElements.add(to);
		
		manualReward = new RW_ManualReward( getBot(), 0.0);
	
		makeWorld();
		posTargetObject(to);

		GoalWriter = Files.makePlotWriter("Bench_Goal_"+botModel+".data");
		GoalWriter.println("Ep\tGoalVal\tGoalAverage");
		TickWriter = Files.makePlotWriter("Bench_Tick_"+botModel+".data");
		TickWriter.println("Ep\tTickPercent\tTickPercentAverage");
	}

	@Override
	public void postStepOps() {
		super.postStepOps();
		
		if(dz.isPointInZone(to.getWorldPosition())&&getBot().actuators.get("EMAG").normalizedValue<0.5)
		{
			posTargetObject(to);
			manualReward.addToCurrentValue(30);
			for(RewardFunction r: rewardFunctions)
				r.reset();
		}
		tickCount++;
	}
	
	private void posTargetObject(DynamicWorldElement obj) {
		Vec2 pos = new Vec2(generatePositionInBoundaries());
		while(!checkElementPositionConficts(pos,40.0f) || !checkObstaclePositionConficts(pos,5.0f))
			pos.set(generatePositionInBoundaries());
		obj.setWorldPosition(pos.x,pos.y);
		obj.body.setLinearVelocity(new Vec2(0,0));
		obj.body.setAngularVelocity(0);
	}

	@Override
	public void reset()
	{
		super.reset();
		posTargetObject(to);
		for(RewardFunction r: rewardFunctions)
			r.reset();
	}
	
	public void postRepetitionOps(ScoreCounter scoreCounter, SimulationInterruptFlag simulationInterruptFlag) {
		double tickp = ((double)tickCount-1.0) / (double)tickLimit;
		double reward = manualReward.computeRewardValue();
		tickPercent = tickPercent.add(new BigDecimal(tickp));
		totalReward = totalReward.add(new BigDecimal(reward));
		manualReward.reset();
		TickWriter.println(episode+"\t"+tickp);
		GoalWriter.println(episode+"\t"+reward);
		tickCount = 0;
		episode++;
	}
	
	public void postRunOps(ScoreCounter score, SimulationInterruptFlag interruptFlag) {

		TickWriter.println("Average"+"\t"+tickPercent.divide(new BigDecimal(episode),new MathContext(10, RoundingMode.HALF_UP)).toPlainString());
		GoalWriter.println("Average"+"\t"+totalReward.divide(new BigDecimal(episode),new MathContext(10, RoundingMode.HALF_UP)).toPlainString());
		TickWriter.close();
		GoalWriter.close();
	}
}
