/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments;

import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ObjectDetector;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLZoneActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosingOnTargetSwitchable;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;
import evoagentsimulation.evoagent2dsimulator.worldElements.WorldElement;

public class EXP_CollectMultiObjVid extends SimulationEnvironment2DSingleBot {

	private TriggerZone dz ;
	private RW_ManualReward manualReward;
	private RW_ClosingOnTargetSwitchable closingTargetReward;
	private ArrayList<WorldElement> targetObjectList;
	private int objectGenerateCount = 3;
	private float objectSize = 1.0f;
	private boolean carry = false;
	
	public EXP_CollectMultiObjVid()
	{
		super();
		this.name = "Collect";
		hasObstacles = true;
	}
	
	public void init() 
	{
		super.init();
		botStartPos = new Vec2(-00.5f,-0.0f);		
		makeBot();
		int WORLD_SIZE = 250;
		getCorners()[0] = new Vec2(-WORLD_SIZE , WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
	    minObstacleSize = 2.2;
	    maxObstacleSizeVariability = 7.0;
	    maxObstacleSpacingVariability = 30.0;
	    obstacleSpacing = 45.0;

		dz = new TriggerZone(new Vec2(0,-20), 0,20);
		((S_ZonePresence)getBot().sensors.get("SENSDZ")).setTarget(dz);
		((S_PLZoneActive)getBot().sensors.get("ACTDZ")).setTarget(dz);
		getWorldElements().add(dz);
		targetObjectList = new ArrayList<WorldElement>();
		((S_ObjectDetector)getBot().sensors.get("SENSOBJ")).setTargetList(targetObjectList);

		((S_PLObjectListActive)getBot().sensors.get("ACTOBJA")).setList(targetObjectList);
		((S_PLObjectListActive)getBot().sensors.get("ACTOBJA")).setDistance(8000.0);

		manualReward = new RW_ManualReward( getBot(), 200);
		rewardFunctions.add(manualReward);
		controlFunctions.add(new CF_NextOnTimeout(getBot(),this, 50000));
		closingTargetReward = new RW_ClosingOnTargetSwitchable(getBot(), 0.0005, dz);
		rewardFunctions.add(closingTargetReward);
		makeWorld();
		getBot().registerBotToWorld();
		generateObjects();
	}
	
	protected Vec2 generateLocalPosition(Vec2 ref, float spread) {
		return new Vec2(
				(float)((Math.random() * spread * 2) + ref.x - spread),
				(float)((Math.random() * spread * 2) + ref.y - spread)
				);
	}
	
	@Override
	protected Vec2 generatePositionInBoundaries(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[1].y - getCorners()[2].y - (2*margin)))+getCorners()[2].y+margin
				);
	}
	
	protected Vec2 generatePositionInBoundariesNorth(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin ,
				(float)((Math.random() * (getCorners()[1].y - getCorners()[2].y - (2*margin)))/2) + margin
				);
	}
	protected Vec2 generatePositionInBoundariesSouth(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin ,
				(float)((Math.random() * (getCorners()[1].y - getCorners()[2].y - (2*margin)))/-2) -  margin
				);
	}
	protected Vec2 generatePositionInBoundariesEast(float margin) {
		return new Vec2(
				(float)((Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))/2) + margin ,
				(float)(Math.random() * (getCorners()[1].y - getCorners()[2].y - (2*margin)))+getCorners()[2].y+margin
				);
	}
	protected Vec2 generatePositionInBoundariesWest(float margin) {
		return new Vec2(
				(float)((Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))/-2 ) - margin ,
				(float)(Math.random() * (getCorners()[1].y - getCorners()[2].y - (2*margin)))+getCorners()[2].y+margin
				);
	}
	
	private void generateObjects() {	
		int sector = ((int)(Math.random() * 4000)) % 4;
		float posSpacing = 40f;
		float posObsSpacing = 8f;
		Vec2 pos;
		TargetObject to;
		//System.out.println(sector);
		for(int i = 0; i < objectGenerateCount; i++)
		{
			if(sector == 0)
			{
				 pos = new Vec2(generatePositionInBoundariesNorth(posSpacing));
				while(!checkElementPositionConficts(pos,objectSize+posSpacing) || !checkObstaclePositionConficts(pos,objectSize+posObsSpacing))
					pos.set(generatePositionInBoundariesNorth(posSpacing));
			}
			else if(sector == 1)
			{
				 pos = new Vec2(generatePositionInBoundariesSouth(posSpacing));
				while(!checkElementPositionConficts(pos,objectSize+posSpacing) || !checkObstaclePositionConficts(pos,objectSize+posObsSpacing))
					pos.set(generatePositionInBoundariesSouth(posSpacing));
			}
			else if(sector == 2)
			{
				 pos = new Vec2(generatePositionInBoundariesEast(posSpacing));
				while(!checkElementPositionConficts(pos,objectSize+posSpacing) || !checkObstaclePositionConficts(pos,objectSize+posObsSpacing))
					pos.set(generatePositionInBoundariesEast(posSpacing));
			}
			else
			{
				 pos = new Vec2(generatePositionInBoundariesWest(posSpacing));
				while(!checkElementPositionConficts(pos,objectSize+posSpacing) || !checkObstaclePositionConficts(pos,objectSize+posObsSpacing))
					pos.set(generatePositionInBoundariesWest(posSpacing));
			}
			to = new TargetObject(pos, 1.0f, objectSize);
			//to.setFriction(100);
			//to.setDamping(100);
			getWorldElements().add(to);
			targetObjectList.add(to);	
			to.registerToWorld(getWorld());
		}		
	}

	@Override
	public void postStepOps() {
		super.postStepOps();
		int i = 0;
		if(((S_ObjectDetector)getBot().sensors.get("SENSOBJ")).getNormalizedValue() > 0.5 && !carry)
		{
			//manualReward.addReward();
			closingTargetReward.switchState(true);		
			carry = true;
		}if(((S_ObjectDetector)getBot().sensors.get("SENSOBJ")).getNormalizedValue() < 0.5 && carry)
		{
			closingTargetReward.switchState(false);		
			carry = false;
		}
		
		while(i < targetObjectList.size())
		{
			if(dz.isPointInZone(targetObjectList.get(i).getWorldPosition()) && !((TargetObject)targetObjectList.get(i)).lock)
			{
				targetObjectList.get(i).removeFromWorld();
				getWorldElements().remove(targetObjectList.get(i));
				targetObjectList.remove(i);
				//rew lad
				manualReward.addReward();
				carry = false;
				closingTargetReward.switchState(false);		
			}
			else
				i++;
		}
		if(targetObjectList.size() <= 0)
			generateObjects();
	}
	
	@Override
	public void reset()
	{
		super.reset();
		carry = false;
		closingTargetReward.switchState(false);		
		clearTargetObjectList();
		for(RewardFunction r: rewardFunctions)
			r.reset();
		generateObjects();
	}
	
	public void clearTargetObjectList()
	{
		for(WorldElement tObj : targetObjectList)
		{
			tObj.removeFromWorld();
			getWorldElements().remove(tObj);
		}
		targetObjectList.clear();
	}
}
