/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollisionAndTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_AvoidObs;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosestObstaclePenalty;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Explorer;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ForwardMotion;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_PunishOnCollision;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_StraightMovement;

public class EXP_Avoid extends SimulationEnvironment2DSingleBot {

	public EXP_Avoid()
	{
		super();
		this.name = "Avoid";
		hasObstacles = true;
		makeCircleWorld = true;
	}
	
	public void init() 
	{
		botStartPos = new Vec2(0.0f,5.0f);		
		makeBot();
	    minObstacleSize = 2.2;
	    maxObstacleSizeVariability = 4.0;
	    maxObstacleSpacingVariability = 12.0;
	    obstacleSpacing = 18.0;
		int WORLD_SIZE = 200;
		getCorners()[0] = new Vec2(-WORLD_SIZE , WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);

		controlFunctions.add(new CF_NextOnCollisionAndTimeout(getBot(),this, 30000));
		rewardFunctions.add(new RW_Explorer(getBot(), 10.0,getCorners(),4.0));
		rewardFunctions.add(new RW_PunishOnCollision(getBot(), 200));
		rewardFunctions.add(new RW_ForwardMotion(getBot(), 0.05));
		//rewardFunctions.add(new RW_Speed(getBot(), 0.01));
		rewardFunctions.add(new RW_AvoidObs(getBot(), 0.01, 0.08));
		rewardFunctions.add(new RW_ClosestObstaclePenalty(getBot(), 0.002, 0.4));	
		rewardFunctions.add(new RW_StraightMovement(getBot(), 0.02));
		makeWorld();
		getBot().registerBotToWorld();
	}
}
