/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments.deprecated;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Distance;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Radar;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollisionAndTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosingOnTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_FixedDistanceFromTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ForwardMotion;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_NotMovingBackward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_SameOrientationAsTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_SameSpeedAsTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_SensorOverThreshold;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Speed;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_StraightMovement;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.DynamicWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.VirtualWorldElement;
import evoagentsimulation.evoagent2dsimulator.worldElements.Waypoint;
import evoagentsimulation.simulationlearning.ScoreCounter;
import evoagentsimulation.simulationlearning.SimulationInterruptFlag;

public class EXP_AlignWithTargetBenchmark extends SimulationEnvironment2DSingleBot {
	
	private final float WORLD_SIZE = 50;
	private final float TICK_LIMIT = 4000;
	
	private final float TARGET_SIZE = 1;
	private final float TARGET_SPEED = 0.07f;
	
	// Changer la methode de génération de point random
	
	private float tickCount = 0;
	private Waypoint target;
	private Vec2 targetHeading;
	
	// Variable benchmark
	
	long repetitionTickCount = 0;
	int repetition = 0;
		
	PrintWriter ScoreWriter;
	
	public EXP_AlignWithTargetBenchmark(String botMod) {
		super(botMod);
		this.name = "AlignWithTargetBenchmark";
		hasObstacles = false;
	}

	public void init() {
		super.init();
		
		getCorners()[0] = new Vec2(-WORLD_SIZE, WORLD_SIZE);
		getCorners()[1] = new Vec2(WORLD_SIZE, WORLD_SIZE);
		getCorners()[2] = new Vec2(WORLD_SIZE, -WORLD_SIZE);
		getCorners()[3] = new Vec2(-WORLD_SIZE, -WORLD_SIZE);
		
		botStartPos = new Vec2(-00.5f,-0.0f);
		
		makeBot();

		target = new Waypoint(new Vec2(-20,-20), (float)(Math.PI/4), TARGET_SIZE);
		getWorldElements().add(target);

		((S_Radar)getBot().sensors.get("TargetOrient")).setTarget(target);       // orient
		((S_Distance)getBot().sensors.get("TargetDistance")).setTarget(target);  // distance

		rewardFunctions.add(new RW_FixedDistanceFromTarget(getBot(), target, 1));
		rewardFunctions.add(new RW_SameOrientationAsTarget(getBot(), target, 4));
		rewardFunctions.add(new RW_SameSpeedAsTarget(getBot(), target, 1000));
		rewardFunctions.add(new RW_NotMovingBackward(getBot(), 2));
		
		// le meilleur score théorique possible en combinant toutes ces fonctions de récompense est 0
				
		makeWorld();
		
		getBot().registerBotToWorld();
		
		getBot().body.getFixtureList().m_isSensor = true; // Disable bot collisions
		
		placeTarget(target);
		
		ScoreWriter = makePlotW("Bench_score_"+botModel+".data");
		ScoreWriter.println("Repetition\tScoreValue");
	}
	
	@Override
	public void postStepOps() {
		super.postStepOps();
		
		if (tickCount >= TICK_LIMIT) {
			reset();
		}
		else {
			moveTarget(target);
			tickCount++;
		}
		
		repetitionTickCount++;
	}
	
	@Override
	public void reset() {
		super.reset();
		
		tickCount = 0;
		
		placeTarget(target);
		
		for(RewardFunction r: rewardFunctions)
			r.reset();
	}
	
	private Vec2 angleToVec2(float angle, float length) {
		return new Vec2(length * (float)Math.cos(angle), length * (float)Math.sin(angle));
	}
	
	private Vec2 generateTargetPosition() {
		float angle = (float)(Math.random() * 2 * Math.PI);
		float length = (float)(Math.random() * WORLD_SIZE);
		return getBot().body.getPosition().add(angleToVec2(angle, length));
	}
	
	private void placeTarget(VirtualWorldElement obj) {	
		target.worldPosition = generateTargetPosition();
		float angle = (float)(Math.random() * 2 * Math.PI);
		targetHeading = angleToVec2(angle, TARGET_SPEED);
	}
	
	private void moveTarget(VirtualWorldElement obj) {
		target.worldPosition.addLocal(targetHeading);
	}
	
	public void postRepetitionOps(ScoreCounter scoreCounter, SimulationInterruptFlag simulationInterruptFlag) {

		ScoreWriter.println(repetition+"\t"+scoreCounter.getCurrentScore());
		
		repetitionTickCount = 0;
		repetition++;
	}
	
	public void postRunOps(ScoreCounter score, SimulationInterruptFlag interruptFlag) {
		ScoreWriter.close();
	}
	
	private PrintWriter makePlotW(String path)
	{
		PrintWriter plotWriter = null;
		
		File  f = new File(path);
		
		if(f.exists())
			f.delete();
		
		try {
			f.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			plotWriter = new PrintWriter(new FileWriter(f),true);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return plotWriter;
	}
}
