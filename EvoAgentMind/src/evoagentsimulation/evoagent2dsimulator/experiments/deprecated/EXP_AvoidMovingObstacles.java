/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments.deprecated;

import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot.ObstaclePos;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollision;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollisionAndTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_AvoidObs;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Explorer;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ForwardMotion;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_PunishOnCollision;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Speed;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_StraightMovement;
import evoagentsimulation.evoagent2dsimulator.worldElements.ObstacleMovingBox;
import evoagentsimulation.evoagent2dsimulator.worldElements.ObstacleStaticBox;
import evoagentsimulation.evoagent2dsimulator.worldElements.WorldElement;

public class EXP_AvoidMovingObstacles extends SimulationEnvironment2DSingleBot {

	
	public EXP_AvoidMovingObstacles(String botMod)
	{
		super(botMod);
		this.name = "Avoid";
		hasObstacles = true;
	}
	
	public void init() 
	{
		botStartPos = new Vec2(0.0f,0.0f);		
		makeBot();
		
		obstacleSpacing = 28;
		minObstacleSize = 2.0;
		maxObstacleVariability = 2.0;

		controlFunctions.add(new CF_NextOnCollisionAndTimeout(getBot(),this, 20000));
		rewardFunctions.add(new RW_Explorer(getBot(), 10.0,getCorners(),4.0));
		rewardFunctions.add(new RW_PunishOnCollision(getBot(), 2000));
		rewardFunctions.add(new RW_ForwardMotion(getBot(), 0.01));
		rewardFunctions.add(new RW_Speed(getBot(), 0.001));
		rewardFunctions.add(new RW_AvoidObs(getBot(), 0.01, 0.08));
		rewardFunctions.add(new RW_StraightMovement(getBot(), 0.01));
		makeWorld();
		//System.out.println(World);
		getBot().registerBotToWorld(getWorld());
	}
	
	@Override
	protected void postStepOps() {
		for(WorldElement o : getObstacles())
			((ObstacleMovingBox) o).step();
	}
	
	@Override
	public void loadObstacles(ArrayList<ObstaclePos> obs) {
		//System.out.println(World);
		for(WorldElement o : getObstacles())
			o.removeFromWorld(getWorld());
		getObstacles().clear();
		for(ObstaclePos o : obs)
			getObstacles().add(new ObstacleMovingBox(o.wp, o.orientation,o.size, getWorld()));		
	}
}
