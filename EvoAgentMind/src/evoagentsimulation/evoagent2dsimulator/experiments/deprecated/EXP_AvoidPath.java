/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments.deprecated;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.WorldThread;
import evoagentsimulation.evoagent2dsimulator.bot.BotType2;
import evoagentsimulation.evoagent2dsimulator.bot.BotType3;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnCollisionAndTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_AvoidObs;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Explorer;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ForwardMotion;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_PunishOnCollision;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_StraightMovement;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.ObstacleStaticBox;

public class EXP_AvoidPath extends SimulationEnvironment2DSingleBot {

	
	public EXP_AvoidPath(String mode,String botMod, String botN, int rep)
	{
		super(mode , botMod, botN, rep);
		this.name = "AvoidPath";
	}
	
	@Override
	protected void init() 
	{
		super.init();
		corners[0] = new Vec2(-50.0f, 50.0f);
		corners[1] = new Vec2(50.0f, 50.0f);
		corners[2] = new Vec2(50.0f, -50.0f);
		corners[3] = new Vec2(-50.0f, -50.0f);
		//makeBorders();
		
		botStartPos = new Vec2(0.0f,0.0f);		
		makeBot(world,botStartPos,0);
		setupBot();
		bot.connect();

		controlFunctions.add(new CF_NextOnCollisionAndTimeout(world, bot,this, 10000));
		rewardFunctions.add(new RW_Explorer(world, bot, 10.0,corners,4.0));
		rewardFunctions.add(new RW_PunishOnCollision(world, bot, 200));
		rewardFunctions.add(new RW_ForwardMotion(world, bot, 0.01));
		rewardFunctions.add(new RW_AvoidObs(world, bot, 0.01));
		rewardFunctions.add(new RW_StraightMovement(world, bot, 0.02));
		
		/*old
		controlFunctions.add(new CF_NextOnCollisionAndTimeout(world, bot,this, 10000));
		rewardFunctions.add(new RW_Explorer(world, bot, 1.0,corners,1.0));
		rewardFunctions.add(new RW_PunishOnCollision(world, bot, 20));
		rewardFunctions.add(new RW_ForwardMotion(world, bot, 0.01));
*/		
		
		
		//worldElements.add(new ObstacleStaticBox(new Vec2(-2.4f, 0), 0, 1.0f, world));

		
		float x;
		Vec2 wp = new Vec2();
		Vec2 prevwp = new Vec2();
		Vec2 nextwp = new Vec2();
		Vec2 tan = new Vec2();
		double shift;
		for (int i = 0 ; i < 80; i++)
		{
			prevwp.x = wp.x;
			prevwp.x = wp.y;
			wp.x = nextwp.x;
			wp.x = nextwp.y;
			nextwp.x = (float)(((i+1) *14.0));
			nextwp.y = (float) (Math.sin((double)(i+1)/2.0)*30);
			tan.x = nextwp.x-prevwp.x;
			tan.y = nextwp.y-prevwp.y;

			
			
			worldElements.add(new ObstacleStaticBox(wp, (float)(Math.PI*Math.random()), (float)(2.0*Math.random()+1.0), world));					
	
		
		}
		
	}
	
	@Override
	public void reset()
	{
		super.reset();
		for(RewardFunction r: rewardFunctions)
			r.reset();
	}
	
}
