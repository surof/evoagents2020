/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.experiments;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Explorer;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_PunishOnCollision;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_StraightMovement;

public class EXP_AvoidDriven extends SimulationEnvironment2DSingleBot {

	public EXP_AvoidDriven()
	{
		super();
		this.name = "AvoidDriven";
		hasObstacles = true;
	}
	
	public void init() 
	{
		botStartPos = new Vec2(0.0f,0.0f);		
		makeBot();
		controlFunctions.add(new CF_NextOnTimeout(getBot(),this, 50000));
		rewardFunctions.add(new RW_PunishOnCollision(getBot(), 2));
		rewardFunctions.add(new RW_Explorer(getBot(), 5.0,getCorners(),4.0));
		rewardFunctions.add(new RW_StraightMovement(getBot(), 0.05));
		makeWorld();
		getBot().registerBotToWorld();
	}
}
