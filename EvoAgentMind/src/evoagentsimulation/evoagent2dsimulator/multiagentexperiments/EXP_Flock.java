/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.multiagentexperiments;

import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBotCTF;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLAgentGroupActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLVirtualActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.CF_NextOnTimeout;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_BelowDistanceFromGroup;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_BelowDistanceFromTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ClosingOnTarget;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_PunishOnCollision;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_Speed;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_TeamScores;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.Flag;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

public class EXP_Flock extends SimulationEnvironment2DMultiBotCTF{

	RW_TeamScores rewardTeamScore;

	//Reward Values
	double goingToFlag = 0.1;
	double nearAllyGroup = 0.04;
	double avoidingAllies = -0.02;
	double maxDistFromGroup = 20;
	double minDistFromAllies = 10;
	double speedReward = 0.01;
	double collisionPenalty = 1.0;
	float distanceToFlag = 15;

	int tickLimit = 100000;
	
	public EXP_Flock() {
		super();
		this.name = "Flock";
		hasObstacles = true;
		obstacleSpacing =80;
		minObstacleSize = 5;
		getCorners()[0] = new Vec2(-250.0f, 250.0f);
		getCorners()[1] = new Vec2(250.0f,250.0f);
		getCorners()[2] = new Vec2(250.0f, -250.0f);
		getCorners()[3] = new Vec2(-250.0f, -250.0f);
	}
	
	@Override
	public void init() 
	{
		super.init();
		
		startAngle = new ArrayList<ArrayList<Double>>();
		
		startAngle.add(new ArrayList<Double>());
		startAngle.get(0).add((double) 0);
		startAngle.get(0).add((double) 0);
		startAngle.get(0).add((double) 0);
		startAngle.get(0).add((double) 0);

		
		startPos = new ArrayList<ArrayList<Vec2>>();
		startPos.add(new ArrayList<Vec2>());
		startPos.get(0).add(new Vec2(10,0));
		startPos.get(0).add(new Vec2(-10,0));
		startPos.get(0).add(new Vec2(0,10));
		startPos.get(0).add(new Vec2(0,-10));
		
		bots = new ArrayList<ArrayList<BotBody2D>>();
		bots.add(makeBots(botModel.get(0), 4, SimulationEnvironment2DMultiBot.TEAM_1_LABEL, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		makeBotMinds(mindTemplates);

		teamBasesPositions.add(new Vec2(generatePositionInBoundaries()));
		
		flags.add(new Flag(teamBasesPositions.get(0), 0, 5, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		teamZones.add(new TriggerZone(teamBasesPositions.get(0), 0, 10,SimulationEnvironment2DMultiBot.TEAM_1_LABEL));
		for(Flag f : flags)
			worldElements.add(f);
		for(TriggerZone t : teamZones)
			worldElements.add(t);
	
		
		for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				((S_ConstantSensor)bots.get(i).get(j).sensors.get("ID")).setValue(((double)(j))/((double)(bots.get(i).size() - 1)));
				((S_PLAgentGroupActive)bots.get(i).get(j).sensors.get("NearFriendlyGroupActive")).setIDToTrack(i);
				((S_PLAgentActive)bots.get(i).get(j).sensors.get("NearFriendlyActive")).setIDToTrack(i);
//				((S_Radar)bots.get(i).get(j).sensors.get("FBaseOrient")).setTarget(teamZones.get(i));
//				((S_Distance)bots.get(i).get(j).sensors.get("FBaseDistance")).setTarget(teamZones.get(i));
				((S_PLVirtualActive)bots.get(i).get(j).sensors.get("FFlagActive")).setTarget(flags.get(i));
			}
		}
		
		for(int i=0;i<bots.get(0).size();i++)
		{
			controlFunctions.add(new CF_NextOnTimeout(bots.get(0).get(i),this, tickLimit));
			rewardFunctions.add(new RW_Speed(bots.get(0).get(i),speedReward));
			rewardFunctions.add(new RW_BelowDistanceFromGroup(bots.get(0).get(i),nearAllyGroup,bots.get(0),maxDistFromGroup));
			for(int j=0;j<bots.size();j++)
			{
				if(i!=j)
				rewardFunctions.add(new RW_BelowDistanceFromTarget( bots.get(0).get(i), avoidingAllies, bots.get(0).get(j), minDistFromAllies));
			}
			rewardFunctions.add(new RW_ClosingOnTarget(bots.get(0).get(i),goingToFlag,flags.get(0)));
			rewardFunctions.add(new RW_PunishOnCollision(bots.get(0).get(i),collisionPenalty));
		}
		makeWorld();
		registerBotsToWorld();
	}

	private void posTargetObject(Flag flag) {
		Vec2 pos = new Vec2(generatePositionInBoundaries());
		flag.setWorldPosition(pos.x,pos.y);
	}

	@Override
	public void postStepOps() {
		super.postStepOps();
		for(int i=0 ; i<bots.get(0).size();i++)
		{
			Vec2 v = bots.get(0).get(i).body.getPosition().sub(flags.get(0).worldPosition);
			float d = v.length();
			if(d<distanceToFlag)
				posTargetObject(flags.get(0));
		}
	}
	
	@Override
	public void reset()
	{
		super.reset();
		posTargetObject(flags.get(0));
		for(RewardFunction r: rewardFunctions)
			r.reset();
	}
	
}
