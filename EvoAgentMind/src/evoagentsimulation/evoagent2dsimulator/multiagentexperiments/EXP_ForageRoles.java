/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.multiagentexperiments;

import java.awt.Color;
import java.util.ArrayList;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.bot.elements.A_AutoClaw;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ConstantSensor;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_Manual;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLObjectListActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_PLZoneActive;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_ZonePresence;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RW_ManualReward;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObjectMoving;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;
import evoagentsimulation.evoagent2dsimulator.worldElements.WorldElement;

public class EXP_ForageRoles extends SimulationEnvironment2DMultiBot{

	RW_ManualReward manualReward;
	private double baseReward = 1.0;
	private double collectA = 10;
	private double collectB = 20;
	private TriggerZone dz ;
	private ArrayList<WorldElement> targetObjectListA ;
	private ArrayList<WorldElement> targetObjectListB ;
	private int objectGenerateCount =12;
	private float objectSize = 1.0f;
	private int botCount =9;
	private int formationInitX = -100;
	private int formationInitY = -100;
	private float objectSpacing = 10f;
	//private static long count = 0;
	
	//Reward Values
	public EXP_ForageRoles() {
		super();
		this.name = "ForageMulti";
		hasObstacles = true;
		makeCircleWorld = true;
		obstacleSpacing =60;
		minObstacleSize = 5;
		
		maxObstacleSizeVariability = 5.0;
		maxObstacleSpacingVariability = 30.0;
	    ObstacleProbability = 0.9;
	    
		getCorners()[0] = new Vec2(-250.0f, 250.0f);
		getCorners()[1] = new Vec2(250.0f,250.0f);
		getCorners()[2] = new Vec2(250.0f, -150.0f);
		getCorners()[3] = new Vec2(-250.0f, -150.0f);
	}
	
	@Override
	public void init() 
	{
		super.init();
		
		manualReward = new RW_ManualReward(null, 1);
		rewardFunctions.add(manualReward);
		startAngle = new ArrayList<ArrayList<Double>>();
		startAngle.add(new ArrayList<Double>());
		startPos = new ArrayList<ArrayList<Vec2>>();
		startPos.add(new ArrayList<Vec2>());
		for(int i = 0; i < botCount; i++)
		{
			startAngle.get(0).add((double) 0);
			startPos.get(0).add(new Vec2(formationInitX + (i * 3), formationInitY + (i * 3)));			
		}
				
		bots = new ArrayList<ArrayList<BotBody2D>>();
		bots.add(makeBots(botModel.get(0), botCount, SimulationEnvironment2DMultiBot.TEAM_1_LABEL, SimulationEnvironment2DMultiBot.TEAM_1_ID));
		makeBotMinds(mindTemplates);
		
		dz = new TriggerZone(new Vec2(0,-850), 0,800);
		getWorldElements().add(dz);
		
		targetObjectListA = new ArrayList<WorldElement>();
		targetObjectListB = new ArrayList<WorldElement>();
		
		for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				((S_ConstantSensor)bots.get(i).get(j).sensors.get("ID")).setValue(((double)(j))/((double)(bots.get(i).size() - 1)));
				((S_ZonePresence)bots.get(i).get(j).sensors.get("SENSDZ")).setTarget(dz);
				((S_PLZoneActive)bots.get(i).get(j).sensors.get("ACTDZ")).setTarget(dz);

				((S_PLObjectListActive)bots.get(i).get(j).sensors.get("ACTOBJA")).setList(targetObjectListA);
				((S_PLObjectListActive)bots.get(i).get(j).sensors.get("ACTOBJB")).setList(targetObjectListB);

				((S_PLObjectListActive)bots.get(i).get(j).sensors.get("ACTOBJA")).setDistance(8000.0);
				((S_PLObjectListActive)bots.get(i).get(j).sensors.get("ACTOBJB")).setDistance(8000.0);
			}
		}
		makeWorld();
		registerBotsToWorld();
		generateObjects();
	}

	@Override
	protected Vec2 generatePositionInBoundaries(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - getCorners()[0].x - (2*margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[0].y - margin))
				);
	}
	
	protected Vec2 generatePositionInBoundariesA(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x - (margin)))+getCorners()[3].x+margin,
				(float)(Math.random() * (getCorners()[0].y - margin))
				);
	}
	
	protected Vec2 generatePositionInBoundariesB(float margin) {
		return new Vec2(
				(float)(Math.random() * (getCorners()[1].x + (margin))),
				(float)(Math.random() * (getCorners()[0].y - margin))
				);
	}
	
	protected Vec2 generateLocalPosition(Vec2 ref, float spread) {
		return new Vec2(
				(float)((Math.random() * spread * 2) + ref.x - spread),
				(float)((Math.random() * spread * 2) + ref.y - spread)
				);
	}
	
	private void generateObjectA(float margin) {	
		if(targetObjectListA.size() < objectGenerateCount)
		{
		TargetObject to;
		Vec2 posObj;
		float objSp = objectSpacing;
		posObj = new Vec2(generatePositionInBoundariesA(margin));
		while(!checkElementPositionConficts(posObj,objectSize+objSp+1f) || !checkObstaclePositionConficts(posObj,objectSize+objSp+5f) || !checkInBoundaries(posObj,objectSize+5f+objSp))
		{
			posObj = new Vec2(generatePositionInBoundariesA(margin));
			if(objSp > 0)
				objSp -= 0.1f;
			/*
			else
				System.out.println("spacing reached zero " + targetObjectListA.size() +  "  " + targetObjectListB.size()) ;
			 */
		}
		to = new TargetObjectMoving(posObj, 1.0f, objectSize,"A");
		getWorldElements().add(to);
		targetObjectListA.add(to);	
		to.registerToWorld(getWorld());
		
		}
		else
		{
			//System.out.println("wtf");
		}
	}
	
	private void generateObjectB(float margin) {	
		if(targetObjectListB.size() < objectGenerateCount)
		{
		TargetObject to;
		Vec2 posObj;
		float objSp = objectSpacing;
		posObj = new Vec2(generatePositionInBoundariesB(margin));
		while(!checkElementPositionConficts(posObj,objectSize+objSp+1f) || !checkObstaclePositionConficts(posObj,objectSize+objSp+5f) || !checkInBoundaries(posObj,objectSize+5f+objSp))
		{
			posObj = new Vec2(generatePositionInBoundariesB(margin));
			if(objSp > 0)
				objSp -= 0.1f;
			/*
			else
				System.out.println("spacing reached zero " + targetObjectListA.size() +  "  " + targetObjectListB.size()) ;
				*/
		}
		to = new TargetObjectMoving(posObj, 1.0f, objectSize,"B");
		to.setColor(Color.magenta);
		getWorldElements().add(to);
		targetObjectListB.add(to);	
		to.registerToWorld(getWorld());
	}
	else
	{
		//System.out.println("wtf");
	}

	}
	
	private void generateObjects() {	
		float margin = 20.0f;
		for(int i = 0; i < objectGenerateCount; i++)
		{
			generateObjectA(margin);
		}	
		for(int i = 0; i < objectGenerateCount; i++)
		{
			generateObjectB(margin);
		}	
	}

	@Override
	public void postStepOps() {
		super.postStepOps();
		for(BotBody2D b : bots.get(0))
		{
			if(dz.isPointInZone(b.getPosition()) && ((A_AutoClaw)b.actuators.get("EMAG")).hasObject())
			{
				double payoff = 0.5;
				TargetObject obj = ((A_AutoClaw)b.actuators.get("EMAG")).getObject();
				((A_AutoClaw)b.actuators.get("EMAG")).reset();
				if (obj.getName().equals("A"))
				{
					collectA++;
					targetObjectListA.remove(obj);
					generateObjectA(20);
					payoff += (collectB - (2.0*collectA)) * 0.008;
				}
				else if (obj.getName().equals("B"))
				{
					collectB++;
					targetObjectListB.remove(obj);
					generateObjectB(20);
					payoff += ((2.0*collectA) - collectB) * 0.008;
				}
				//double diff = collectA - collectB;
				/*
				if(diff>0)
					diff = diff*diff;
				else
					diff = -(diff*diff);
				*/
				//System.out.println("payoff : " +collectA + " - " +collectB + " = "+ Math.max(0.0,Math.min(1.0,0.5 + ((diff) * 0.001))));
				((S_Manual)b.sensors.get("PAYOFF")).setValue(Math.max(0.0,Math.min(1.0,payoff)));
				//soft
				//manualReward.addToCurrentValue((baseReward / (1.0 + Math.abs((((2.0*collectA) - collectB))))));
				
				//mid
				manualReward.addToCurrentValue((baseReward / (1.0 + Math.abs((((2.0*collectA) - collectB))))-0.05));
				
				//harsh
				//manualReward.addToCurrentValue((baseReward / (1.0 + Math.abs((((2.0*collectA) - collectB))))-0.1));
				
				obj.removeFromWorld();
				getWorldElements().remove(obj);
				messageString = "ObjA = " + collectA + "  ObjB = " + collectB;
			}
		}
			collectA /= 1.00003;
			collectB /= 1.00003;
		/*
		
		for(WorldElement t : targetObjectListA)
		{
			((TargetObjectMoving)t).randomPush();
		}
		for(WorldElement t : targetObjectListB)
		{
			((TargetObjectMoving)t).randomPush();
		}*/
		
		/*
		int i = 0;
		while(i < targetObjectListA.size())
		{
			if(dz.isPointInDZ(targetObjectListA.get(i).getWorldPosition()) && !((TargetObject)targetObjectListA.get(i)).lock)
			{
				targetObjectListA.get(i).removeFromWorld();
				getWorldElements().remove(targetObjectListA.get(i));
				targetObjectListA.remove(i);
				//rew lad
				manualReward.addReward();
			}
			else
				i++;
		}
		i=0;
		while(i < targetObjectListB.size())
		{
			if(dz.isPointInDZ(targetObjectListB.get(i).getWorldPosition()) && !((TargetObject)targetObjectListB.get(i)).lock)
			{
				targetObjectListB.get(i).removeFromWorld();
				getWorldElements().remove(targetObjectListB.get(i));
				targetObjectListB.remove(i);
				//rew lad
				manualReward.addReward();
			}
			else
				i++;
		}*/
/*
		if(targetObjectList.size() <= 0)
			generateObjects();*/
	}
	
	@Override
	public void reset()
	{
		super.reset();
		clearTargetObjectList();
		collectA = 10;
		collectB = 20;
		for(RewardFunction r: rewardFunctions)
			r.reset();
		generateObjects();
	}
	
	public void clearTargetObjectList()
	{
		for(WorldElement tObj : targetObjectListA)
		{
			tObj.removeFromWorld();
			getWorldElements().remove(tObj);
		}
		targetObjectListA.clear();
		for(WorldElement tObj : targetObjectListB)
		{
			tObj.removeFromWorld();
			getWorldElements().remove(tObj);
		}
		targetObjectListB.clear();
	}
}
