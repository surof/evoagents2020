/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator.worldElements;

import org.jbox2d.common.Vec2;

import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DMultiBot;

public class Flag extends VirtualWorldElement{
	public int id = 0;
	public int carry = SimulationEnvironment2DMultiBot.NOTEAM_ID;
	public boolean home = true;
	Vec2 startPos;
	
	public Flag(Vec2 worldPos, float worldAng, float s, int teamId) {
		super(worldPos, worldAng, s);
		startPos = new Vec2(worldPos);
		id = teamId;
	}
	
	public void returnToBase()
	{
		worldPosition = new Vec2(startPos);
		home = true;
		carry = SimulationEnvironment2DMultiBot.NOTEAM_ID;
	}

	public Vec2 getPosition() {
		return worldPosition;
	}

	public void pickUpFlag(Vec2 position, int teamID) {
		//System.out.println("pickup");
		carry = teamID;
		home = false;
		worldPosition = position;
	}

	public void dropFlag() {
		worldPosition = new Vec2(worldPosition);
		carry = SimulationEnvironment2DMultiBot.NOTEAM_ID;
	}
}
