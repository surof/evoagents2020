/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.contacts.Contact;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentmindelements.EvoAgentMindTemplate;
import evoagentsimulation.evoagent2dsimulator.bot.BotBody2D;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.ControlFunction;
import evoagentsimulation.evoagent2dsimulator.experiments.elements.RewardFunction;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.VirtualWorldElement;
import evoagentsimulation.simulationlearning.ScoreCounter;
import evoagentsimulation.simulationlearning.SimulationInterruptFlag;

public class SimulationEnvironment2DMultiBot extends SimulationEnvironment2D{

	public static final String TEAM_1_LABEL = "TEAM1";
	public static final String TEAM_2_LABEL = "TEAM2";
	public static final String TEAM_3_LABEL = "TEAM3";
	public static final String TEAM_4_LABEL = "TEAM4";

	public static final int NOTEAM_ID = -1;
	public static final int TEAM_1_ID = 0;
	public static final int TEAM_2_ID = 1;
	public static final int TEAM_3_ID = 2;
	public static final int TEAM_4_ID = 3;
	
	protected ArrayList<String> botModel;
	protected ArrayList<ArrayList<BotBody2D>> bots;
	protected ArrayList<ArrayList<EvoAgentMind>> minds;
	protected ArrayList<ArrayList<Vec2>> startPos = new ArrayList<ArrayList<Vec2>>();
	protected ArrayList<ArrayList<Double>> startAngle = new ArrayList<ArrayList<Double>>();
	protected ArrayList<HashMap<String,ArrayList<BotBody2D>>> Signal = new ArrayList<HashMap<String,ArrayList<BotBody2D>>>();
	protected ArrayList<EvoAgentMindTemplate> mindTemplates;
	

	
    public SimulationEnvironment2DMultiBot()
    {
    	super();
		worldContactlistener = new ContactListener() {	
			@Override
			public void beginContact(Contact contact) {
				if(contact.m_fixtureA.getUserData() instanceof BotBody2D)
				{
					if(contact.m_fixtureB.getUserData() instanceof TargetObject)
					{}
					else
					{
						((BotBody2D)contact.m_fixtureA.getUserData()).contactCounter++;
						botContactCounter++;
					}
				}
				if(contact.m_fixtureB.getUserData() instanceof BotBody2D)
				{
					if(contact.m_fixtureA.getUserData() instanceof TargetObject)
					{}
					else
					{
						((BotBody2D)contact.m_fixtureB.getUserData()).contactCounter++;
						botContactCounter++;
					}
				}
			}
			@Override
			public void endContact(Contact contact) {
				if(contact.m_fixtureA.getUserData() instanceof BotBody2D)
				{
					if(contact.m_fixtureB.getUserData() instanceof TargetObject)
					{}
					else
					{
						((BotBody2D)contact.m_fixtureA.getUserData()).contactCounter--;		
						botContactCounter--;				
					}
				}
				if(contact.m_fixtureB.getUserData() instanceof BotBody2D)
				{
					if(contact.m_fixtureA.getUserData() instanceof TargetObject)
					{}
					else
					{
						((BotBody2D)contact.m_fixtureB.getUserData()).contactCounter--;
						botContactCounter--;
					}
				}
			}
			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {}
			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {}
		};
    }
    
    @Override
	public void doWorldStep()
	{
		getWorld().step(timeStep,viterations,piterations);
		tick++;
	}
	
    @Deprecated
	public void doStep(ScoreCounter scoreCounter, SimulationInterruptFlag interruptFlag) {
    	super.doStep(scoreCounter, interruptFlag);
		if(scoreCounter != null)
			for(RewardFunction r : rewardFunctions)
					scoreCounter.addToScore(r.computeRewardValue());
		for(ControlFunction c : controlFunctions)
			interruptFlag.registerInterrupt(c.performCheck());		

    	for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				if(bots.get(i).get(j).isActive())
					minds.get(i).get(j).doStep(bots.get(i).get(j).step(minds.get(i).get(j).getPreviousActuatorValues()));
			}
		}
	}

	public void doAutoStep(ScoreCounter scoreCounter, SimulationInterruptFlag interruptFlag) {
		super.doAutoStep(scoreCounter, interruptFlag);
		if(scoreCounter != null)
			for(RewardFunction r : rewardFunctions)
					scoreCounter.addToScore(r.computeRewardValue());
		for(ControlFunction c : controlFunctions)
			interruptFlag.registerInterrupt(c.performCheck());		

    	for(int i = 0 ; i < bots.size() ; i++)
		{
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				if(bots.get(i).get(j).isActive())
				{
					bots.get(i).get(j).step();	
					minds.get(i).get(j).doStep();	
				}
			}
		}
	}
	
	protected boolean checkElementPositionConficts(Vec2 wp, float s) {

		for(VirtualWorldElement el: getWorldElements())
			if(MathUtils.distance(el.worldPosition,wp)<s+el.size)
				return false;	
		for(ArrayList<BotBody2D> ab: getBots())
			for(BotBody2D b: ab)
				if(MathUtils.distance(b.body.getPosition(),wp)<s+b.size)
					return false;	
		return true;
	}

	public ArrayList<ArrayList<BotBody2D>> getBots() {
		return bots;
	}

	public void init() {
		
	}

	   public void reset() {
	    	super.reset();
			for(ArrayList<EvoAgentMind> am : minds)
				for(EvoAgentMind m : am)
					m.reset();
	    	for(int i = 0 ; i < bots.size() ; i++)
			{
				for(int j = 0 ; j < bots.get(i).size() ; j++)
				{
					bots.get(i).get(j).reset();
					bots.get(i).get(j).setPosition(startPos.get(i).get(j),startAngle.get(i).get(j));			
				}
				Signal.get(i).clear();
			}
		}
	    
		protected ArrayList<BotBody2D> makeBots(String botModel,  int count, String teamLabel, int teamId)
		{
			ArrayList<BotBody2D> ret = new ArrayList<>();
			Class<?> clazz;
			try {
				clazz = Class.forName("evoagentsimulation.evoagent2dsimulator.bot."+botModel);
				Constructor<?> constructor = clazz.getConstructor(SimulationEnvironment2D.class);
				for(int i = 0 ; i < count; i++)
				{
					BotBody2D bot = (BotBody2D) constructor.newInstance(this);
					bot.label = teamLabel;
					bot.ID = teamId;
					bot.setName("BOT " + i + " (" +teamLabel+")");
					ret.add(bot);					
				}
			} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				System.out.println("bot 2D type not found " + botModel );
				return null;
			}
			return ret;
		}
		
	public void makeBotMinds(ArrayList<EvoAgentMindTemplate> mindTemplates) {
		minds = new ArrayList<ArrayList<EvoAgentMind>>();
		for(int i = 0 ; i < bots.size() ; i++)
		{
			minds.add(new ArrayList<EvoAgentMind>());
			Signal.add(new HashMap<>());
			for(int j = 0 ; j < bots.get(i).size() ; j++)
			{
				minds.get(i).add(EvoAgentMindFactory.makeMindFromTemplate(mindTemplates.get(i)));
				minds.get(i).get(j).setAgentName(bots.get(i).get(j).getName());
				bots.get(i).get(j).setMind(minds.get(i).get(j));
			}
		}			
	}

	public void registerBotsToWorld()
	{
		for(ArrayList<BotBody2D> ab : bots)
			for(BotBody2D b : ab)
			{
				b.registerBotToWorld();
			}
	}
	
	public void activateSignal(int teamId, String signal, BotBody2D bot)
	{
		ArrayList<BotBody2D> sig = null;
		sig = Signal.get(teamId).get(signal);
		if(sig == null)
		{
			sig = new ArrayList<>();
			Signal.get(teamId).put(signal, sig);
		}
		if(!sig.contains(bot))
			sig.add(bot);
	}
	
	public void deactivateSignal(int teamId, String signal, BotBody2D bot)
	{
		ArrayList<BotBody2D> sig = Signal.get(teamId).get(signal);
		if(sig != null && sig.contains(bot))
			sig.remove(bot);		
	}

	public BotBody2D GetClosestSignal(int teamId, String signal, BotBody2D me, double dist)
	{
		BotBody2D ret = null;
		double ndist = dist;
		double tmpDist;
		ArrayList<BotBody2D> sig = Signal.get(teamId).get(signal);
		if(sig != null)
			for(BotBody2D b : sig)
				if((tmpDist = MathUtils.distance(b.body.getPosition(), me.body.getPosition()))<= ndist)
				{
					ndist = tmpDist;
					ret = b;
				}
		return ret;
	}
	
	public BotBody2D GetClosestSignal(int teamId, String signal, BotBody2D me)
	{
		BotBody2D ret = null;
		double ndist = -1;
		double tmpDist;
		ArrayList<BotBody2D> sig = Signal.get(teamId).get(signal);
		if(sig != null)
			for(BotBody2D b : sig)
				if((tmpDist = MathUtils.distance(b.body.getPosition(), me.body.getPosition()))<= ndist || ndist < 0)
				{
					ndist = tmpDist;
					ret = b;
				}
		return ret;
	}

	public void setMindTemplates(ArrayList<EvoAgentMindTemplate> mindTempl) {
		mindTemplates = mindTempl;
	}
	
	public ArrayList<EvoAgentMind> getTeamMinds(int id)
	{
		return minds.get(id);
	}
	
	public void setBotModel(ArrayList<String> botModel)
	{
		this.botModel = botModel;  
	}
	
	public ArrayList<ArrayList<EvoAgentMind>> getBotMinds()
	{
		return minds;
	}
}