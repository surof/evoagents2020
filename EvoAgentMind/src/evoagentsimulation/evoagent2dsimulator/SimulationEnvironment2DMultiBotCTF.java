/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation.evoagent2dsimulator;

import java.util.ArrayList;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.contacts.Contact;

import evoagentmindelements.EvoAgentMindTemplate;
import evoagentsimulation.evoagent2dsimulator.bot.WarBot;
import evoagentsimulation.evoagent2dsimulator.bot.elements.S_FlagCarry;
import evoagentsimulation.evoagent2dsimulator.worldElements.Flag;
import evoagentsimulation.evoagent2dsimulator.worldElements.Projectile;
import evoagentsimulation.evoagent2dsimulator.worldElements.TargetObject;
import evoagentsimulation.evoagent2dsimulator.worldElements.TriggerZone;

// Capture the flag environment developed for a student project

public class SimulationEnvironment2DMultiBotCTF extends SimulationEnvironment2DMultiBot{
	protected long penaltyTime = 1000;
	protected long tickCounter = 0;

	protected ArrayList<Projectile> projectileScheduleForRemoval = new ArrayList<>();
	protected ArrayList<ArrayList<Projectile>> projectiles = new ArrayList<ArrayList<Projectile>>();
	protected ArrayList<Vec2> teamBasesPositions = new ArrayList<>();
	protected ArrayList<Flag> flags = new ArrayList<>();
	protected ArrayList<TriggerZone> teamZones = new ArrayList<>();
	protected ArrayList<Integer> teamScores = new ArrayList<>();
	protected ArrayList<WarBot> botsInPenalty = new ArrayList<>();
	protected ArrayList<Long> botsReleaseTime = new ArrayList<>();
	
	public SimulationEnvironment2DMultiBotCTF() {
		super();
		worldContactlistener = new ContactListener() {
			@Override
			public void beginContact(Contact contact) {
				if(contact.m_fixtureA.getUserData() instanceof WarBot)
				{
					if(contact.m_fixtureB.getUserData() instanceof TargetObject)
					{}
					else if(contact.m_fixtureB.getUserData() instanceof Projectile)
					{
						((WarBot)contact.m_fixtureA.getUserData()).registerHit(((Projectile)contact.m_fixtureB.getUserData()).label);
					}
					else
					{
						((WarBot)contact.m_fixtureA.getUserData()).contactCounter++;
						botContactCounter++;
					}
				}
				if(contact.m_fixtureB.getUserData() instanceof WarBot)
				{
					if(contact.m_fixtureA.getUserData() instanceof TargetObject)
					{}
					else if(contact.m_fixtureA.getUserData() instanceof Projectile)
					{
						((WarBot)contact.m_fixtureB.getUserData()).registerHit(((Projectile)contact.m_fixtureA.getUserData()).label);
					}
					else
					{
						((WarBot)contact.m_fixtureB.getUserData()).contactCounter++;
						botContactCounter++;
					}
				}
				if(contact.m_fixtureA.getUserData() instanceof Projectile)
				{
					projectileScheduleForRemoval.add(((Projectile)contact.m_fixtureA.getUserData()));
				}
				if(contact.m_fixtureB.getUserData() instanceof Projectile)
				{
					projectileScheduleForRemoval.add(((Projectile)contact.m_fixtureB.getUserData()));
				}
			}
			@Override
			public void endContact(Contact contact) {
				if(contact.m_fixtureA.getUserData() instanceof WarBot)
				{
					if(contact.m_fixtureB.getUserData() instanceof TargetObject)
					{}
					else if(contact.m_fixtureB.getUserData() instanceof Projectile)
					{}
					else
					{
						((WarBot)contact.m_fixtureA.getUserData()).contactCounter--;
						botContactCounter--;						
					}
				}
				if(contact.m_fixtureB.getUserData() instanceof WarBot)
				{
					if(contact.m_fixtureA.getUserData() instanceof TargetObject)
					{}
					else if(contact.m_fixtureA.getUserData() instanceof Projectile)
					{}
					else
					{
						((WarBot)contact.m_fixtureB.getUserData()).contactCounter--;
						botContactCounter--;
					}
				}
			}
			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {}
			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {}
		};
	}
	
	  @Override
		public void postStepOps() {
		   //checkFlags();
		   checkPenalty();
		   tickCounter++;
		}
	   
	   protected void checkFlags()
	   {
		   for(int i = 0 ; i < flags.size() ; i++)
			{
			   //CAPTURE FLAG
			   if(flags.get(Math.abs(-1 + i)).carry == i && teamZones.get(i).isPointInZone(flags.get(Math.abs(-1 + i)).worldPosition))// "clever" ... don't do that 
			   {
				   teamScores.set(i, teamScores.get(i) + 1);
				   flags.get(Math.abs(-1 + i)).returnToBase();
				   for(int j = 0 ; j < bots.get(i).size() ; j++)
					{
						((S_FlagCarry)bots.get(i).get(j).sensors.get("EFlagCarry")).setValue(0);
					}
				   //scoresToConsole();
			   }
			   //RETURN FLAG
			   if(flags.get(i).carry == i && teamZones.get(i).isPointInZone(flags.get(Math.abs(i)).worldPosition))// "clever" ... don't do that 
			   {
				   flags.get(Math.abs(i)).returnToBase();
				   for(int j = 0 ; j < bots.get(i).size() ; j++)
					{
						((S_FlagCarry)bots.get(i).get(j).sensors.get("FFlagCarry")).setValue(0);
					}
			   }
			}
	   }

	   /*
		private void scoresToConsole() {
			System.out.println("=== SCORE ===");
			System.out.println(SimulationEnvironment2DMultiBot.TEAM_1_LABEL + " : " + teamScores.get(0));
			System.out.println(SimulationEnvironment2DMultiBot.TEAM_2_LABEL + " : " + teamScores.get(1));
		}
		*/
	   
		public ArrayList<ArrayList<Projectile>> getProjectiles()
		{
			return projectiles;
		}

		public void botToPenalty(WarBot warBot) {
			botsInPenalty.add(warBot);
			botsReleaseTime.add(tickCounter + penaltyTime);
		}
		
		 protected void checkPenalty()
		 {
			 int team1 = 0;
			 int team2 = 0;
			 int i = 0;
			 while(i < botsReleaseTime.size())
			 {
				 WarBot b = botsInPenalty.get(i);
				if(botsReleaseTime.get(i)<= tickCounter) 
				{
					
					botsReleaseTime.remove(i);
					botsInPenalty.remove(i);
					if(b.ID == SimulationEnvironment2DMultiBot.TEAM_1_ID)
					{
						b.setPosition(startPos.get(0).get(team1), 0);
						team1++;
					}
					else
					{
						b.setPosition(startPos.get(1).get(team2), 0);
						team2++;
					}
					b.reset();
					b.setActive(true);
				}else
				{
					if(((S_FlagCarry)b.sensors.get("EFlagCarry")).normalizedValue == 1.0)
					{
						((S_FlagCarry)b.sensors.get("EFlagCarry")).setValue(0);
						((S_FlagCarry)b.sensors.get("EFlagCarry")).dropFlag();
					}
					if(((S_FlagCarry)b.sensors.get("FFlagCarry")).normalizedValue == 1.0)
					{
						((S_FlagCarry)b.sensors.get("FFlagCarry")).setValue(0);
						((S_FlagCarry)b.sensors.get("FFlagCarry")).dropFlag();
					}
				
					b.reset();
					b.setPosition(new Vec2(getCorners()[1].x + 50 + (5 * botsInPenalty.size()),getCorners()[1].y + 50 + (5 * botsInPenalty.size())), 0);
					b.setActive(false);
				}
				i++;
			 }
		 }


			@Override
			public void doWorldStep()
			{
				super.doWorldStep();
				projectileScheduleForRemoval.clear();
				for(Projectile p : projectileScheduleForRemoval)
					p.removeProjectileFromWorld();
			}
		 
	@Override
	public void reset() {
		super.reset();
		for (int i = 0; i < bots.size(); i++) {
			for (Projectile p : projectiles.get(i))
				projectileScheduleForRemoval.add(p);
			projectiles.get(i).clear();
		}
	}

	@Override
	public void makeBotMinds(ArrayList<EvoAgentMindTemplate> mindTemplates) {
		super.makeBotMinds(mindTemplates);
		for(int i = 0 ; i < bots.size() ; i++)
		{
			projectiles.add(new ArrayList<Projectile>());
		}			
	}
}
