/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentsimulation;

import java.util.ArrayList;
import java.util.HashMap;

import evoagentsimulation.simulationlearning.ScoreCounter;
import evoagentsimulation.simulationlearning.SimulationInterruptFlag;

public class SimulationEnvironment {

	protected String name = "DefaultWorld";
	protected String botModel;	
	protected ArrayList<String> botModels;	
	protected boolean triggeredInterrupt = false;
	protected boolean hasObstacles = false;
	
    public SimulationEnvironment()
    {
		
    }

	public void doWorldStep()
	{
		
	}

	public void doStep(ScoreCounter scoreCounter, SimulationInterruptFlag interruptFlag) {
		interruptFlag.registerInterrupt(triggeredInterrupt);
	}
	
	public void doAutoStep(ScoreCounter scoreCounter, SimulationInterruptFlag interruptFlag) {
		interruptFlag.registerInterrupt(triggeredInterrupt);
	}

	public HashMap<String,Double> doStep(HashMap<String,Double> actuatorValues, ScoreCounter scoreCounter, SimulationInterruptFlag interruptFlag) {
		interruptFlag.registerInterrupt(triggeredInterrupt);
		return null;
	}
	
    public void reset() {
		triggeredInterrupt = false;
	}
    
    public boolean hasTriggeredInterrupt()
    {
    	return triggeredInterrupt;
    }
    
    public void manualReset()
	{
    	triggeredInterrupt = true;
	}


	public void init() {
		//Override me
	}
	
	public void preStepOps() {
		//Override me
		// called before a simulation step
	}
	
	public void preMindOps() {
		//Override me
		// called after the world step (physics), before the Mind step (agent logic)
	}
	
	public void postStepOps() {
		//Override me
		// called after a simulation step
	}
    
	public void preRepetitionOps() {
		//Override me
		// called before a repetition
	}
	
	public void postRepetitionOps(ScoreCounter scoreCounter, SimulationInterruptFlag simulationInterruptFlag) {
		//Override me		
		// called after a repetition
	}
	
	public void preRunOps() {
		//Override me
		// called after the init, before starting the first repetition
	}
	
	public void postRunOps(ScoreCounter score, SimulationInterruptFlag interruptFlag) {
		//Override me		
		// called after the last repetition
	}

	protected void makeBot() {
		
	}

	public void setBotModel(String botModel)
	{
		
	}

	public void setBotModel(ArrayList<String> botModel)
	{
		
	}
	
	public boolean getHasObstacles()
	{
		return hasObstacles;
	}


}