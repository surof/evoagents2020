/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules;

public class SkillInputHistory implements SkillInput {
	InputSource source;
	String type;
	String name;
	int horizon = 0;

	public SkillInputHistory(String type, String name, int horizon)
	{
		this.type = type;
		this.name = name;
		this.horizon = horizon;
	}

	@Override
	public double get() {
		return source.getHistory(horizon);
	}
	
	@Override
	public void setSource(InputSource s) {
		this.source = s;
	}
	
	@Override
	public InputSource getSource() {
		return source;
	}
	
	@Override
	public boolean isType(String sensorprefix) {
		return type.equals(sensorprefix);
	}

	@Override
	public String getSourceName() {
		return name;
	}
	
	@Override
	public String getType() {
		return type;
	}
	
	public int getHorizon()
	{
		return horizon;
	}
}
