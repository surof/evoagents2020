/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules;

import java.util.ArrayList;

import org.encog.ml.MLRegression;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.prg.EncogProgram;
import org.encog.ml.prg.EncogProgramContext;
import org.encog.util.obj.ObjectCloner;
import evoagentmindelements.EvoAgentMind;

public class ProgramModule extends SkillModule{

	EncogProgramContext prContext = null;
	private BasicMLData networkInputData = null;
	private ArrayList<EncogProgram> programs = null;
	private boolean limitNetworkOutputs = true;
	
	/// WARNING : only works for one output so far.
	/// TODO 	: figure something out with encog (root node ? override GA ?)
	
	
	public ProgramModule(EvoAgentMind inMind,String name, ArrayList<SkillInput> sInput,
			ArrayList<SkillOutput> sOutput, ArrayList<EncogProgram> inPrograms) {
		super(inMind,name, sInput, sOutput);
		this.isNeural = true;
		networkInputData = new BasicMLData(skillInput.size());
		programs = inPrograms;		
		System.out.println(programs.get(0));
		if(programs.get(0) != null)
			prContext = programs.get(0).getContext();	
	}
	
	@SuppressWarnings("unchecked")
	public ProgramModule(EvoAgentMind inMind, ProgramModule prm)
	{
		super(inMind, prm);
		this.prContext = prm.prContext;
		this.isNeural = true;
		networkInputData = new BasicMLData(skillInput.size());
		programs = (ArrayList<EncogProgram>) ObjectCloner.deepCopy(prm.programs);
	}

	public void doStep() {
		if(isMaster)
			currentInfluence = 1.0;
		if(currentInfluence > 0.0)
		{
			loadInput();
			networkInputData.setData(networkinput);
			for(int i = 0; i < 1; i++) {
				networkOutput[i] = programs.get(i).compute(networkInputData).getData(0);
			}
			if(limitNetworkOutputs)
				for(int i = 0; i < getOutputCount(); i++)
				{
					if(networkOutput[i]>1.0)
						networkOutput[i] = 1.0;
					else if(networkOutput[i]<0.0)
						networkOutput[i] = 0.0;
				}
			transmitOutput();
		}
		prevInfluence = currentInfluence;
		currentInfluence = 0.0;
	}
	
	public MLRegression getInternalFunction()
	{
		return programs.get(0);
	}

	public void setInternalFunction(MLRegression f)
	{
		programs.set(0, (EncogProgram) f);
	}
	
	public void setPrograms(ArrayList<EncogProgram> inPrograms)
	{
		programs =  inPrograms;		
	}

	public ArrayList<EncogProgram> getPrograms() {
		
		return programs;
	}

	public EncogProgramContext  getContext()
	{
		return prContext;
	}
	
	public BasicMLData copyInputMLData()
	{
		return new BasicMLData(networkInputData);
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
}
