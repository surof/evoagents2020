/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules;

import java.util.ArrayList;

import org.encog.ml.MLRegression;
import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.util.obj.ObjectCloner;

import evoagentmindelements.EvoAgentMind;
import evoagentsimulation.simulationlearning.EncogNetworkFactory;
import evoagentsimulation.simulationlearning.EncogNetworkTemplate;


public class NeuralNetworkModule extends SkillModule{

	EncogNetworkTemplate networkTemplate = null;
	private BasicMLData networkInputData = null;
	private MLData networkOutputData = null;
	private MLRegression network = null;
	
	double prevReward = 0.0;
	
	private boolean limitNetworkOutputs = true;
	
	//ArrayList<String> hname;
	//ArrayList<Integer> hcount;
	
	public NeuralNetworkModule(EvoAgentMind inMind,String name, ArrayList<SkillInput> sInput, ArrayList<SkillOutput> sOutput, EncogNetworkTemplate networkTempl,MLRegression inNetwork) {

		super(inMind,name, sInput, sOutput);
		this.networkTemplate = networkTempl;
		this.isNeural = true;
		networkInputData = new BasicMLData(skillInput.size());
		if(inNetwork == null)
			network = EncogNetworkFactory.makeFromTemplate(networkTempl);
		else
			network = inNetwork;
	}
	
	public NeuralNetworkModule(EvoAgentMind inMind, NeuralNetworkModule nnm)
	{
		super(inMind, nnm);
		this.networkTemplate = nnm.networkTemplate;
		this.isNeural = true;
		networkInputData = new BasicMLData(skillInput.size());
		network = (MLRegression) ObjectCloner.deepCopy(nnm.network);
	}

	public void doStep() {
		if(isMaster)
			currentInfluence = 1.0;
		if(currentInfluence > 0.0)
		{
			loadInput();
			networkInputData.setData(networkinput);
			networkOutputData = network.compute(networkInputData);
			networkOutput = networkOutputData.getData();
			if(limitNetworkOutputs)
				for(int i = 0; i < networkOutputData.size(); i++)
				{
					if(networkOutput[i]>1.0)
						networkOutput[i] = 1.0;
					else if(networkOutput[i]<0.0)
						networkOutput[i] = 0.0;
					//if(isMaster)
						//System.out.println( i + " = " + Math.max(0.0, Math.min(1.0, networkOutputData.getData(i))));
				}
			transmitOutput();
		}
		prevInfluence = currentInfluence;
		currentInfluence = 0.0;
	}

	
	public MLRegression getInternalFunction()
	{
		return network;
	}

	public void setInternalFunction(MLRegression f)
	{
		network =  f;
	}
	
	/*
	public void setNetwork(MLRegression inNet)
	{
		network =  inNet;		
	}

	public MLRegression getNetwork() {
		
		return network;
	}*/

	public EncogNetworkTemplate getNetwokTemplate()
	{
		return networkTemplate;
	}
	
	public BasicMLData copyInputMLData()
	{
		return new BasicMLData(networkInputData);
	}

	public BasicMLData copyOutputMLData()
	{
		return new BasicMLData(networkOutputData);
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
}
