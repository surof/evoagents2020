/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.drive;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.modules.SkillModule;
import evoagentmindelements.modules.variables.VariableModule;

public class DM_SelectObject extends DriveModule{
	VariableModule role;
	SkillModule collectVar;
	SkillModule select;
	double roleValue = 0.5;
	
	public DM_SelectObject(EvoAgentMind mind)
	{
		super(mind);
	}
	
	public void doStep() {
		role.overrideValue(roleValue);
		collectVar.transmitInfluenceCommand(1.0,1.0);
		select.transmitInfluenceCommand(1.0,1.0);
	}

	public void setRoleValue(double val)
	{
		roleValue = val;
	}
	
	public void checkInputOutputUse() {
		role = mind.getVariable("VAR_ROLE");
		role.setReadOnly(true);
		collectVar = mind.getSkill("CollectVar");
		select = mind.getSkill("SelectObject");
	}
}
