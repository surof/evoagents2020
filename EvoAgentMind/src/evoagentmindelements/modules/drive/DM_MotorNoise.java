/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.drive;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.modules.ActuatorModule;

//possible solution for normalisation of influence ?
//try learning base skills with this on

public class DM_MotorNoise extends DriveModule{
	ActuatorModule LW;
	ActuatorModule RW;
	
	public DM_MotorNoise(EvoAgentMind mind)
	{
		super(mind);
	}
	
	public void doStep() {
		LW.transmitInfluenceCommand(0.5, 0.3);
		RW.transmitInfluenceCommand(0.5, 0.3);
	}

	public void checkInputOutputUse() {
		LW = mind.getActuator("MotL");
		RW = mind.getActuator("MotR");
	}
}
