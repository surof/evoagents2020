/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.hardcodedskills;

public class StrategyLogic extends HardCodedSkill{

	public void doStep(double[ ] in, double[ ] out)
	{
			out[0] = 1.0;

				if(in[1]>0.5) // bring back enemy flag
				{
					out[1] = in[8];
				}
				else
				{
					if(in[2] > 0.4 && in[7] > 0.8) // save friendly flag
						out[1] = in[4];
					else
					{
						if(in[0] < 0.4) // defender
						{
							if(in[12] > 0.5) // pursue
							{
								out[1] = in[14];			
							}
							else
							{
								if(in[10] > 0.9)
									out[1] = in[8];												
								else if(in[10] < 0.2)
									out[1] = in[9];												
								else
									out[1] = 0.4 + (Math.random() * 0.2);
							}
						}else // attacker
						{

							out[1] = in[5];												
						}
					}
							
			}
	}
}

/** memo

0  SE:ID
1  SE:FlagCarry
2  SE:FFlagState
3  SE:EFlagState
4  SE:FFlagOrient
5  SE:EFlagOrient
6  SE:FFlagDistance
7  SE:EFlagDistance
8  SE:FBaseOrient
9  SE:EBaseOrient
10 SE:FBaseDistance
11 SE:EBaseDistance
12 SE:NearEnemyActive
13 SE:NearEnemyDistance
14 SE:NearEnemyOrient

*/