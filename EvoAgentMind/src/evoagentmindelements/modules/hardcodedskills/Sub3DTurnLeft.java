/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.hardcodedskills;

/*
 * 			actuators.put("M1",new A3D_BiDirectionalThruster(0.10f,new Vector3f(1.5f,0,0),new Vector3f(0,-1f,0),this));
			actuators.put("M2",new A3D_BiDirectionalThruster(0.10f,new Vector3f(-1.5f,0,0),new Vector3f(0,-1f,0),this));
			actuators.put("M3",new A3D_BiDirectionalThruster(0.10f,new Vector3f(0,0,1.5f),new Vector3f(0,-1f,0),this));
			
			actuators.put("M4",new A3D_BiDirectionalThruster(0.3f,new Vector3f(2f,0,2f),new Vector3f(-0.6f,0,1f),this));
			actuators.put("M5",new A3D_BiDirectionalThruster(0.3f,new Vector3f(-2f,0,2f),new Vector3f(0.6f,0,1f),this));
			actuators.put("M6",new A3D_BiDirectionalThruster(0.3f,new Vector3f(2f,0,-2f),new Vector3f(-0.6f,0,-1f),this));
			actuators.put("M7",new A3D_BiDirectionalThruster(0.3f,new Vector3f(-2f,0,-2f),new Vector3f(0.6f,0,-1f),this));
 */


public class Sub3DTurnLeft extends HardCodedSkill{

	public void doStep(double[ ] in, double[ ] out)
	{

		out[0] = 1.0;
		out[1] = 0.0;
		out[2] = 0.0;
		out[3] = 1.0;
	}
}
