/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.hardcodedskills;


public class ForageRole extends HardCodedSkill{

	public void doStep(double[ ] in, double[ ] out)
	{
		out[0] = 1.0;
		out[1] = 1.0;
		//out[2] = in[3];
		out[3] = in[2];
		if(in[0] > 0.5 && in[1] > 0.5)			
		{
			if(in[4]>= 0.5)
			{
				//System.out.println(Math.max(0.0,Math.min(1.0,in[3] + (0.1 * (in[1] -0.5)))));
				out[2] = Math.max(0.0,Math.min(1.0,in[4] + (0.01 * (in[2] -0.5))));			
			}
			else
			{
				//System.out.println(Math.max(0.0,Math.min(1.0,in[3] - (0.005 * (in[1] -0.5)))));
				out[2] = Math.max(0.0,Math.min(1.0,in[4] - (0.01 * (in[2] -0.5))));								
			}
		}

	}
}
