/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.hardcodedskills;

public class Gtdza extends HardCodedSkill{

	public void doStep(double[ ] in, double[ ] out)
	{
		out[0] = 0.0;
		out[1] = 1.0;
		
		if(in[0] < 0.10 ||in[1] < 0.10 ||in[7] < 0.10 || in[8] < 0.08 ||in[9] < 0.08||in[2] < 0.05||in[6] < 0.05)
		{
			out[0] = 1.0 - Math.max(Math.min(in[6]-0.02,Math.min(in[2]-0.02,Math.min(in[0]-0.02, Math.min(in[1]-0.02, Math.min(in[7]-0.02, Math.min(in[8]-0.02, in[9]-0.02)))))),0.0);
			out[1] = Math.max(Math.min(in[6]-0.02,Math.min(in[2]-0.02,Math.min(in[0]-0.02, Math.min(in[1]-0.02, Math.min(in[7]-0.02, Math.min(in[8]-0.02, in[9]-0.02)))))),0.0);
		}

	}
}
