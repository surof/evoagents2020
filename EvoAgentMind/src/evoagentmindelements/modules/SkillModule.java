/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules;

import java.util.ArrayList;

import org.encog.ml.MLRegression;
import org.encog.ml.data.basic.BasicMLData;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMind;

public abstract class SkillModule implements MindElement, InfluenceTarget {
	public boolean isMaster = false;
	public boolean isLearning = false;
	protected boolean isBenchmarking = false;
	public boolean isNeural = false;
	private String name;
	protected ArrayList<SkillInput> skillInput;
	protected ArrayList<SkillOutput> skillOutput;
	protected ArrayList<String> skillNamesOutput = null;
	protected double currentInfluence = 0.0;
	protected double prevInfluence = 0.0;
	protected double tickReward = 0.0;
	public String serverComment = null;
	protected EvoAgentMind rootMind = null;
	protected double[ ] networkinput;
	protected double[ ] networkOutput = null;
	
	public SkillModule(EvoAgentMind inMind, String name, ArrayList<SkillInput> sInput, ArrayList<SkillOutput> sOutput) {
		
		this.rootMind = inMind;
		this.setName(name);
		skillInput = sInput;
		skillOutput = sOutput;
		networkOutput = new double[skillOutput.size()];
		networkinput = new double[skillInput.size()];
	}
	
	public SkillModule(EvoAgentMind inMind, SkillModule sm)
	{
		this.rootMind = inMind;
		this.setName(new String(sm.getName()));
		skillInput = sm.cloneSkillInputs();
		skillOutput = sm.cloneSkillOutputs();
		if(skillNamesOutput!=null)
			skillNamesOutput = new ArrayList<>(skillNamesOutput);
		networkOutput = new double[skillOutput.size()];
		networkinput = new double[skillInput.size()];
	}
	
	private ArrayList<SkillInput> cloneSkillInputs() {
		ArrayList<SkillInput> res = new ArrayList<>();
		for(int i = 0; i < skillInput.size(); i++)
		{
			if(skillInput.get(i) instanceof SkillInputInstant)
				res.add(new SkillInputInstant(skillInput.get(i).getType(), skillInput.get(i).getSourceName()));
			else if(skillInput.get(i) instanceof SkillInputDerivative)
				res.add(new SkillInputDerivative(skillInput.get(i).getType(), skillInput.get(i).getSourceName()));
			else if(skillInput.get(i) instanceof SkillInputHistory)
				res.add(new SkillInputHistory(skillInput.get(i).getType(), skillInput.get(i).getSourceName(),((SkillInputHistory) skillInput.get(i)).getHorizon()));
		}
		return res;
	}
	
	private ArrayList<SkillOutput> cloneSkillOutputs() {
		ArrayList<SkillOutput> res = new ArrayList<>();
		for(int i = 0; i < skillOutput.size(); i++)
			res.add(new SkillOutput(skillOutput.get(i).getType(), skillOutput.get(i).getTargetName()));
		return res;
	}

	protected void transmitOutput() {
		for(int i = 0; i < skillOutput.size(); i++)
			skillOutput.get(i).send(networkOutput[i], currentInfluence);
	}

	protected void loadInput() {
		for(int i = 0; i < skillInput.size(); i++)
			networkinput[i] = skillInput.get(i).get();
	}
	
	public void doStep() {
	}

	public void setMaster(boolean b) {
		isMaster = b;
	}

	public void setLearning(boolean b) {
		isLearning = b;
 	}
	
	public boolean getLearning() {
		return isLearning;
 	}

	protected void sendMessages(double[] networkOutput) {

	}
	
	public boolean loadBestGenome() {
		return true;
	}

	public String getSkillName()
	{
		return getName();
	}

	public void setSkillName(String hier) {
		setName(hier);
	}

	public void HandleFeedback(String string) {
		
	}
	
	public void setBenchmarking(boolean b) {
		isBenchmarking = b;
 	}
	
	public boolean getBenchmarking() {
		return isBenchmarking;
 	}
	
	public void addInfluence(double inInfl)
	{
		currentInfluence += inInfl;
	}
	
	public void transmitInfluenceCommand(double skillValue, double influenceValue)
	{
		currentInfluence += (skillValue*influenceValue);
	}

	public int getInputCount() {
		return skillInput.size();
	}

	public int getOutputCount() {
		return skillOutput.size();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public MLRegression getInternalFunction()
	{
		return null;
	}

	public void setInternalFunction(MLRegression f)
	{
	}
	
	public BasicMLData copyInputMLData()
	{
		return null;
	}

	public BasicMLData copyOutputMLData()
	{
		return null;		
	}
	
	public ArrayList<SkillInput> getSkillInputs()
	{
		return skillInput;
	}

	public ArrayList<SkillOutput> getSkillOutputs()
	{
		return skillOutput;
	}

	public double getSkillOutputValueForDisplay(String name) //very slow : do not use for learning
	{
		for(SkillOutput out : skillOutput)
		{
			if(out.isType(EvoAgentAppDefines.skillPrefix))
				if(out.getTargetName().equals(name))
					return networkOutput[skillOutput.indexOf(out)] * prevInfluence;
		}
		return 0.0;
	}
	
	public double getFunctionOutputValueForDisplay(String name) //very slow : do not use for learning
	{
		for(SkillOutput out : skillOutput)
		{
			if(out.isType(EvoAgentAppDefines.skillPrefix))
				if(out.getTargetName().equals(name))
					return networkOutput[skillOutput.indexOf(out)];
		}
		return 0.0;
	}

	public double getCurrentInfluence() {
		return currentInfluence;
	}

	public double getPrevInfluence() {
		return prevInfluence;
	}
	
	public void setInUse(boolean use)
	{
	}

	public void linkSkillToMind(EvoAgentMind evoAgentMind) {
		rootMind = evoAgentMind;
		for(SkillInput in : skillInput)
		{
			//System.out.println(in.getSourceName());
			if(in.isType(EvoAgentAppDefines.sensorPrefix))
				in.setSource(evoAgentMind.getSensor(in.getSourceName()));
			if(in.isType(EvoAgentAppDefines.variablePrefix))
				in.setSource(evoAgentMind.getVariable(in.getSourceName()));
		}
		for(SkillOutput out : skillOutput)
		{
			if(out.isType(EvoAgentAppDefines.motorPrefix))
				out.setTarget(evoAgentMind.getActuator(out.getTargetName()));
			if(out.isType(EvoAgentAppDefines.variablePrefix))
				out.setTarget(evoAgentMind.getVariable(out.getTargetName()));
			if(out.isType(EvoAgentAppDefines.skillPrefix))
				out.setTarget(evoAgentMind.getSkill(out.getTargetName()));
		}
	}

	public ArrayList<String> getSkillOutputNames() {
		if(skillNamesOutput == null)
		{
			skillNamesOutput = new ArrayList<>();
			for(SkillOutput out : skillOutput)
			{
				if(out.isType(EvoAgentAppDefines.skillPrefix))
					skillNamesOutput.add(out.getTargetName());
			}
		}		
		return skillNamesOutput;
	}
}
