/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.variables;

import evoagentmindelements.modules.SensorModule;

// outdated, use drivemodules instead

public class SensorMirrorVariableModule extends VariableModule{
	private static final long serialVersionUID = -5327882372306372644L;
	private SensorModule sensor;
	
	public SensorMirrorVariableModule(String idIn) {
		super(idIn, 0.0);
		inputValue = 0.5;
	}
	
	public SensorMirrorVariableModule(String idIn, Double initVal) {
		super(idIn, initVal);
		inputValue = 0.5;
	}
	
	@Override
	protected void compute()
	{
		inputValue = sensor.getValue();
		computedValue = inputValue;
	}
	
	@Override
	protected void reinitVariable()
	{
	}

	public void setSensor(SensorModule s)
	{
		sensor = s;
	}
	
	public SensorModule getSensor()
	{
		return sensor;
	}
}
