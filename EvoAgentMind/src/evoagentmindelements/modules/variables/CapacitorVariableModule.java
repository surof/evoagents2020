/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.variables;

// WIP

public class CapacitorVariableModule extends VariableModule{
	private static final long serialVersionUID = -5327882372306372644L;
	private double chargeRate = 0.01;
	private double dischargeRate = 0.000001;
	private double prevValue = 0.5;
	
	public CapacitorVariableModule(String idIn) {
		super(idIn, 0.0);
		inputValue = 0.5;
	}
	
	public CapacitorVariableModule(String idIn, Double initVal) {
		super(idIn, initVal);
		inputValue = 0.5;
	}
	
	@Override
	protected void compute()
	{
		computedValue = (prevValue + (inputValue*chargeRate) + (0.5*dischargeRate)) / (1.0 + chargeRate + dischargeRate);
		prevValue = computedValue;
	}
	
	@Override
	protected void reinitVariable()
	{
		prevValue = 0.5;
		computedValue = 0.0;
		inputValue = 0.5;
	}
}
