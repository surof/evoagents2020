/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements.modules.variables;

public class TickWaveSinVariableModule extends VariableModule{

	private static final long serialVersionUID = -926987773673557816L;
	private double variationFreq = 1000;
	private long tick = 0;
	
	public TickWaveSinVariableModule(String idIn) {
		super(idIn,0.0);
		inputValue = 0.880;
		}

	public TickWaveSinVariableModule(String idIn, Double initVal) {
		super(idIn,initVal);
		inputValue = 0.880;
		}

	@Override
	protected void compute()
	{
		tick++;
		computedValue = ((Math.sin(tick/(variationFreq - (variationFreq * inputValue) + 0.33))/2.0)+0.5);	
	}
	
	protected void reinitVariable() {
		computedValue = 0.0;
		tick = 0;
	}
}
