/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentmindelements;

import java.util.ArrayList;

import org.encog.ml.MLEncodable;

import evoagentmindelements.modules.NeuralNetworkModule;
import evoagentmindelements.modules.SkillModule;

// read files only once

public class EvoAgentMindTemplate {
	ArrayList<String> actuators = new ArrayList<String>();
	ArrayList<String> sensors = new ArrayList<String>();
	ArrayList<String> variableName = new ArrayList<String>();
	ArrayList<String> variableType = new ArrayList<String>();
	ArrayList<Double> variableInitValue = new ArrayList<Double>();
	ArrayList<String> variableArguments = new ArrayList<String>();
	ArrayList<SkillModule> skills = new ArrayList<SkillModule>();
	String driveName;
	
	public SkillModule getSkillByName(String name)
	{
		for(SkillModule s : skills)
			if(s.getSkillName().equals(name))
				return s;
		return null;
	}


	public MLEncodable getSkillNetwork(String learningSkill) {
		return (MLEncodable) getSkillByName(learningSkill).getInternalFunction();
	}
	
	public int getSkillInputCount(String learningSkill) {
		return getSkillByName(learningSkill).getInputCount();
	}

	public int getSkillOutputCount(String learningSkill) {
		return getSkillByName(learningSkill).getOutputCount();
	}


	public ArrayList<String> getSkillNames() {
		ArrayList<String> ret = new ArrayList<>();
		for(SkillModule s :skills)
			ret.add(s.getName());
		return ret;
	}


	public ArrayList<String> getNeuralSkillNames() {
		ArrayList<String> ret = new ArrayList<>();
		for(SkillModule s :skills)
			if(s.isNeural)
				ret.add(s.getName());
		return ret;
	}
	
	public boolean hasSkill(String name)
	{
		for(SkillModule s : skills)
			if(s.getSkillName().equals(name))
				return true;
		return false;
	}
	
}
