/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.HashMap;

import javax.swing.JFrame;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentsimulation.evoagent3dsimulator.SimulationEnvironment3D;

import ui.SimEnv3DViewer;

public class EATaskDemoSimSingleBot3D extends EATaskDemoSimSingleBot {
	Thread viewerThread;
	HashMap<String,Double> actuatorValues = null;
	SimEnv3DViewer simPanel;
	JFrame frame;
	
	public EATaskDemoSimSingleBot3D(String root, String botmodel, String botname, String masterS, String driveN,String envirName)
	{
		super(root, botmodel, botname, masterS, driveN, envirName);
		envClasspath = EvoAgentAppDefines.classpath3D;
		mindTemplate = EvoAgentMindFactory.loadMindTemplate(MindRootFolder,botname, masterSkill, mindDriveClassName);
		mind = EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
	}

	protected void viewerStep() {
		//using animation timer
	}

	protected void initViewer() {	
		simPanel = new SimEnv3DViewer((SimulationEnvironment3D) environment, scoreCounter); 
		frame = new JFrame(environmentName);
		frame.add(simPanel);
		frame.setSize(800, 600);
		simPanel.start();
		frame.setVisible(true);
	}
	
	@SuppressWarnings("deprecation")
	protected void closeViewer() {
	    frame.setVisible(false);
	    simPanel.stop();
	    frame.hide();
	    frame = null;
	}
	
	protected boolean checkContinue() {
		return frame.isVisible();
	}
	
	protected boolean checkRepetitionContinue() {
		return frame.isVisible();
	}
	
	protected void initEnvironement()
	{
		System.out.println("env : " + environment);
		environment.setBotModel(botModel);
		((SimulationEnvironment3D)environment).setBotMind(mind);
		environment.init();
	}
}
