/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;

import org.encog.Encog;
import org.encog.ml.CalculateScore;
import org.encog.ml.MLEncodable;
import org.encog.ml.MLMethod;
import org.encog.ml.MethodFactory;
import org.encog.ml.ea.train.basic.BasicEA;
import org.encog.ml.ea.train.basic.TrainEA;
import org.encog.ml.genetic.MLMethodGeneticAlgorithm;
import org.encog.ml.genetic.MLMethodGenome;
import org.encog.ml.prg.EncogProgramContext;
import org.encog.ml.prg.PrgCODEC;
import org.encog.ml.prg.expvalue.ValueType;
import org.encog.ml.prg.extension.StandardExtensions;
import org.encog.ml.prg.generator.RampedHalfAndHalf;
import org.encog.ml.prg.opp.SubtreeCrossover;
import org.encog.ml.prg.opp.SubtreeMutation;
import org.encog.ml.prg.species.PrgSpeciation;
import org.encog.ml.prg.train.PrgPopulation;
import org.encog.ml.prg.train.rewrite.RewriteBoolean;
import org.encog.ml.prg.train.rewrite.RewriteConstants;
import org.encog.ml.train.MLTrain;
import org.encog.neural.neat.NEATPopulation;
import org.encog.neural.neat.NEATUtil;
import org.encog.neural.neat.PersistNEATPopulation;
import org.encog.neural.networks.training.anneal.NeuralSimulatedAnnealing;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.util.concurrency.MultiThreadable;

import evoagentapp.EvoAgentApp;
import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMindTemplate;
import evoagentmindelements.modules.NeuralNetworkModule;
import evoagentmindelements.modules.SkillInput;
import evoagentsimulation.simulationlearning.EncogNetworkFactory;
import evoagentsimulation.simulationlearning.MyNEAT.NEATOPopulation;
import evoagentsimulation.simulationlearning.MyNEAT.PersistNEATOPopulation;
import evoagentsimulation.simulationlearning.geneticprogram.LimitedConstantMutation;

public class EATaskLearningSim extends EATask{
	
	protected String learningAgentLearningSkillFolder = "";
	protected String learningAgentSkillFolder = "";
	protected String learningAgentFolder = "";

	protected String learningSkill = "";
	protected String learningMethod = EvoAgentAppDefines.geneticLearningMethod;
	protected EvoAgentMindTemplate learningMindTemplate = null;		
	
	protected String environmentName = "";
	
	protected int tickLimit = 0;
	protected int generation = 0;
	protected int evaluationRepetition = 1;
	protected int populationSize = 200;
	protected CalculateScore simScore;
	

	protected int saveGeneration = 0;
	protected boolean saveGenAuto = false;
	
	protected boolean useDynamicMetaParameters = false;

	

	public EATaskLearningSim(String rootFolder, String learnSk,  String learnMeth, String envirName, int gen, int tickLim, int evalRep, int saveGen, boolean saveAuto, int sizeGen)
	{
		super(rootFolder);
		learningSkill = learnSk;
		learningMethod = learnMeth;
		environmentName = envirName;
		tickLimit = tickLim;
		generation = gen;
		evaluationRepetition = evalRep;
		saveGeneration = saveGen;
		populationSize = sizeGen;
		saveGenAuto= saveAuto;
	}
	
	public void setLearningMindTemplate() //override me
	{
		
	}
	
	protected CalculateScore makeCalculateScore() //override me
	{
		return null;
	}
	
	public void runTask() 
	{
		learnIteration();
		Encog.getInstance().shutdown();
	}
	
	protected void learnIteration()
	{
		double bestScore = 0.0;
		double lastBestScore = 0.0;
		LocalDateTime startTime; 
		LocalDateTime stopTime; 
		
		setLearningMindTemplate();
		simScore = makeCalculateScore();
		MLTrain train = makeTrainer();
		boolean canThrottle = false;
		try {
			MultiThreadable mt = (MultiThreadable)train;
			System.out.println(mt.getThreadCount() + " threads available");
			canThrottle = true;
		} catch (Exception e) {
			System.out.println("not multithreadable");
		}
		if(train != null)
		{
			System.out.println("doing " + generation + " generations");
			startTime = LocalDateTime.now();
			System.out.println("starting at :" + startTime);
			for(int i=0;i<generation && !endTask;i++) {
				if(EvoAgentApp.frame != null && canThrottle) {
					((MultiThreadable)train).setThreadCount(EvoAgentApp.frame.getAvailableThreads());
					System.out.print(((MultiThreadable)train).getThreadCount() + "th - it :");
				}
				System.out.print(i+"\t");
				train.iteration();
				System.out.println((bestScore=train.getError()));

				if(saveGeneration > 0 && (i+1)%saveGeneration == 0)
				{
					save(train,i,bestScore);
				}
				if(saveGenAuto)
				{
					if(i == 0)
					{
						lastBestScore = bestScore;
					}
					else if(bestScore > lastBestScore)
					{
						save(train,i,bestScore);
						lastBestScore = bestScore;
					}
				}				
			} 
			persistTrainingOperation(train);
			train.pause();
			stopTime = LocalDateTime.now();
			train.finishTraining();
			System.out.println("Save network");
			save(train,-1,bestScore);
			System.out.println("started at :" + startTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + 
					" stopped at :" + stopTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + 
					" runtime :" + String.format("%d:%02d:%02d", Duration.between(startTime, stopTime).getSeconds() / 3600, (Duration.between(startTime, stopTime).getSeconds() % 3600) / 60, (Duration.between(startTime, stopTime).getSeconds() % 60)));
		}
	}

	private MLTrain makeTrainer() {
		System.out.println(learningMethod);
		switch (learningMethod) {
		case EvoAgentAppDefines.geneticLearningMethod:
			return makeGeneticAlgorithmTraining();
		case EvoAgentAppDefines.annealingLearningMethod:
			return makeSimulatedAnnealingTraining();
		case EvoAgentAppDefines.neatLearningMethod:
			return makeNeatTraining();
		case EvoAgentAppDefines.neatoLearningMethod:
			return makeNeatOTraining();
		case EvoAgentAppDefines.geneticProgrammingLearningMethod:
			return makeGeneticProgrammingTraining();
		default:
			return null;
		}
	}

	private MLTrain makeNeatTraining()
	{
		System.out.println("making NEAT training");
		NEATPopulation pop= null;
		Path path = Paths.get(learningAgentLearningSkillFolder+"/TRAINPERSIST_"+learningSkill+"_"+learningMethod+EvoAgentAppDefines.trainingPersistenceFileExtension);
		if (Files.exists(path)) {
			System.out.println("resume NEAT training from file");			
			try {
				byte[] data = Files.readAllBytes(path);
				pop = (NEATPopulation)new PersistNEATPopulation().read(new ByteArrayInputStream(data));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(pop == null)
		{
			System.out.println("start NEAT training from scratch");			
			pop = new NEATPopulation(learningMindTemplate.getSkillInputCount(learningSkill),learningMindTemplate.getSkillOutputCount(learningSkill), populationSize);
			pop.reset();
		}
		return NEATUtil.constructNEATTrainer(pop, simScore);
	}
	
	private MLTrain makeNeatOTraining()
	{
		System.out.println("making NEATO training");
		NEATOPopulation pop= null;
		Path path = Paths.get(learningAgentLearningSkillFolder+"/TRAINPERSIST_"+learningSkill+"_"+learningMethod+EvoAgentAppDefines.trainingPersistenceFileExtension);
		if (Files.exists(path)) {
			System.out.println("resume NEATO training from file");			
			try {
				byte[] data = Files.readAllBytes(path);
				pop = (NEATOPopulation)new PersistNEATOPopulation().read(new ByteArrayInputStream(data));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(pop == null)
		{
			System.out.println("start NEAT training from scratch");			
			pop = new NEATOPopulation(learningMindTemplate.getSkillInputCount(learningSkill),learningMindTemplate.getSkillOutputCount(learningSkill), populationSize);
			pop.reset();
		}
		return NEATUtil.constructNEATTrainer(pop, simScore);
	}
	
	private MLTrain makeSimulatedAnnealingTraining()
	{
		System.out.println("making SimulatedAnnealing training");
		return new NeuralSimulatedAnnealing((MLEncodable) learningMindTemplate.getSkillNetwork(learningSkill),simScore, 10, 2, populationSize);
	}
	
	class ResumeMLMethodGeneticAlgorithm extends MLMethodGeneticAlgorithm {
	public ResumeMLMethodGeneticAlgorithm(MethodFactory phenotypeFactory, CalculateScore calculateScore, int populationSize,File[] files) {
			super(phenotypeFactory, calculateScore, populationSize);
			int i = 0;
			while(i< files.length && i < this.getGenetic().getPopulation().getPopulationSize())
			{		
				MLEncodable nnet = (MLEncodable) EncogDirectoryPersistence.loadObject(files[i]);
				this.getGenetic().getPopulation().getSpecies().get(0).getMembers().get(i).copy(new MLMethodGenome(nnet));	
				System.out.println("resume from : " + files[i]);
				i++;
			}
			System.out.println("end resume load");
		}
	}
	
	private MLTrain makeGeneticAlgorithmTraining()
	{
		MLTrain ret = null;
		System.out.println("making Genetic training");
		Path path = Paths.get(learningAgentLearningSkillFolder+"/");
		File dir = new File(path.toString());
		File[] files = dir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.toLowerCase().endsWith(EvoAgentAppDefines.neuralNetworkFileExtension) && name.contains(learningSkill) && !name.contains("IGNORE");
		    }
		});		
		if (files.length > 0) {
			System.out.println("resume GENETIC training from file (" + files.length + " files found)");		
			
				ret = new  ResumeMLMethodGeneticAlgorithm(new MethodFactory(){
					@Override
					public MLMethod factor() {
						final MLMethod result = EncogNetworkFactory.makeFromTemplate(((NeuralNetworkModule)learningMindTemplate.getSkillByName(learningSkill)).getNetwokTemplate());
						return result;
					}},simScore,populationSize,files);			
		
		}
		else {
			System.out.println("start GENETIC training from scratch");
		ret = new MLMethodGeneticAlgorithm(new MethodFactory(){
			@Override
			public MLMethod factor() {
				final MLMethod result = EncogNetworkFactory.makeFromTemplate(((NeuralNetworkModule)learningMindTemplate.getSkillByName(learningSkill)).getNetwokTemplate());
				return result;
			}},simScore,populationSize);
		}
		System.out.println("The genetic selection operator is : "+((MLMethodGeneticAlgorithm) ret).getGenetic().getSelection().getClass().getName());
		return ret;
	}
	
	private MLTrain makeGeneticProgrammingTraining()
	{
		TrainEA ret = null;
		System.out.println("making Genetic programming");
		System.out.println("start GENETIC programming from scratch");
		
		EncogProgramContext context = new EncogProgramContext();

		ArrayList<SkillInput> in =learningMindTemplate.getSkillByName(learningSkill).getSkillInputs();
		for(int i = 0 ; i < in.size(); i++)
		{
			context.defineVariable(in.get(i).getSourceName(),ValueType.floatingType);			
		}		

		context.getFunctions().addExtension(StandardExtensions.EXTENSION_VAR_SUPPORT);
		context.getFunctions().addExtension(StandardExtensions.EXTENSION_CONST_SUPPORT);
		StandardExtensions.createBooleanOperators(context);
		
		PrgPopulation pop = new PrgPopulation(context, populationSize);

		ret = new TrainEA(pop, simScore);
		ret.setCODEC(new PrgCODEC());
		ret.addOperation(0.8, new SubtreeCrossover());
		ret.addOperation(0.1, new SubtreeMutation(pop.getContext(),4));
		ret.addOperation(0.1, new LimitedConstantMutation(pop.getContext(),0.5,1.0));
		//ret.addScoreAdjuster(new ComplexityAdjustedScore(4,10,0.1,20.0));
		pop.getRules().addRewriteRule(new RewriteConstants());
		pop.getRules().addRewriteRule(new RewriteBoolean());
		ret.setSpeciation(new PrgSpeciation());		
		if( context.getFunctions().size()>0 ) {
			(new RampedHalfAndHalf(context,2,3)).generate(new Random(), pop);
		}
		return ret;
	}
	
	private void save(MLTrain train, int step, double score)
	{
		String stepStr ="BEST";
		if(step != -1)
			stepStr = "GEN_"+String.valueOf(step+1);
		
		if (learningMethod.equals(EvoAgentAppDefines.geneticProgrammingLearningMethod))
		{
			saveBestProgPopulation((TrainEA)train,learningAgentLearningSkillFolder+"/"+stepStr+"_"
					+learningSkill+"_"+learningMethod+"_"+score+EvoAgentAppDefines.genProgramFileExtension);
		}
		else
		{
			saveBestNetwork(train.getMethod(),learningAgentLearningSkillFolder+"/"+stepStr+"_"
					+learningSkill+"_"+learningMethod+"_"+score+EvoAgentAppDefines.neuralNetworkFileExtension);
		}
	}
	
	private void saveBestProgPopulation(TrainEA tr, String path) {
		File f = new File(path);
		EncogDirectoryPersistence.saveObject(f, tr.getPopulation());
	}

	private void saveBestNetwork(MLMethod network, String path) {
		File f = new File(path);
		EncogDirectoryPersistence.saveObject(f, network);
	}

	private void persistTrainingOperation(MLTrain train)
	{
		if(learningMethod.equals(EvoAgentAppDefines.neatLearningMethod))
		{
			final ByteArrayOutputStream serialized = new ByteArrayOutputStream();
		    new PersistNEATPopulation().save(serialized, ((BasicEA) train).getPopulation());
			FileOutputStream fos = null;
			try {
			    fos = new FileOutputStream(new File(learningAgentLearningSkillFolder+"/TRAINPERSIST_"+learningSkill+"_"+learningMethod+EvoAgentAppDefines.trainingPersistenceFileExtension)); 	
			    serialized.writeTo(fos);
			    fos.close();
				} catch(IOException ioe) {
				    ioe.printStackTrace();
				}
		}else if(learningMethod.equals(EvoAgentAppDefines.neatoLearningMethod))
		{
			final ByteArrayOutputStream serialized = new ByteArrayOutputStream();
		    new PersistNEATOPopulation().save(serialized, ((BasicEA) train).getPopulation());
			FileOutputStream fos = null;
			try {
			    fos = new FileOutputStream(new File(learningAgentLearningSkillFolder+"/TRAINPERSIST_"+learningSkill+"_"+learningMethod+EvoAgentAppDefines.trainingPersistenceFileExtension)); 	
			    serialized.writeTo(fos);
			    fos.close();
				} catch(IOException ioe) {
				    ioe.printStackTrace();
				}
		}
	}
}
