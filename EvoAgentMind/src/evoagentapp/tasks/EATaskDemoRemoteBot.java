/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindFactory;
import remote.PiBotCommDefines;

//to be fixed
@Deprecated
public class EATaskDemoRemoteBot extends EATask {

	@SuppressWarnings("unused")
	EvoAgentMind mind;
	
	Socket socket;
	BufferedReader clientIn;
	PrintWriter clientOut;
	
	boolean doRun =true;
	HashMap<String,Double> actuatorValues = null;
	ConcurrentHashMap<String,Double> sensorValues = new ConcurrentHashMap<>();
	HashMap<String,TextDisplay> actuatorDisplay = null;
	HashMap<String,TextDisplay> sensorDisplay = null;
	HashMap<String,TextDisplay> variableDisplay = null;
	
	boolean UDPMode = false;
	DatagramChannel UDPChannel;
	ByteBuffer UDPBuffer,UDPBufferRec;
	InetSocketAddress UDPAdress = new InetSocketAddress("pinkie", PiBotCommDefines.portUDP);
	InetSocketAddress TCPAdress = new InetSocketAddress("pinkie", PiBotCommDefines.port);
	
	boolean receiverFlag = true;

	public EATaskDemoRemoteBot(String root, String botmodel, String botname, String masterS, String driveN)
	{
		super(root, botmodel, botname, masterS);
		mindTemplate = EvoAgentMindFactory.loadMindTemplate(MindRootFolder,botname, masterSkill);
		mind = EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
	}

	public void runTask() 
	{
		makeUI();
		connect();
		if(UDPMode)
			runUDP();
		else
			runTCP();
	}
	
	private void runTCP()
	{
		String sendCommand = PiBotCommDefines.mode_TCP;
		clientOut.println(sendCommand);
		clientOut.flush();
		sendCommand = PiBotCommDefines.ctrl_start;		
		clientOut.println(sendCommand);
		clientOut.flush();
		while(doRun)
		{
			//System.out.println("run tick");
			sendCommand = PiBotCommDefines.ctrl_do;	
			actuatorValues = mind.doStep(getSensorValues(sensorValues,clientIn));
			clientOut.println(sendCommand);
			for(String a : sensorValues.keySet())
			{
				if(sensorDisplay.get(a)!=null)
					sensorDisplay.get(a).update(sensorValues.get(a));				
			}
			for(String a : mind.getVariablesNames())
				variableDisplay.get(a).update(mind.getVariable(a).getValue());
			for(String a : actuatorValues.keySet())
			{
					actuatorDisplay.get(a).update(actuatorValues.get(a));
					clientOut.println(a+PiBotCommDefines.commSeparator+actuatorValues.get(a));				
			}
			clientOut.println(PiBotCommDefines.tag_endmsg);
			clientOut.flush();	
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		System.out.println("piBot demo ended");
		closeTCP();
	}

	private void runUDP()
	{
		String sendCommand = PiBotCommDefines.mode_UDP;
		clientOut.println(sendCommand);
		clientOut.flush();

		try {
			initUDP();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			sendCommand = PiBotCommDefines.ctrl_start+"\n";
			UDPBuffer = ByteBuffer.allocate(8192);
			UDPBuffer.put(sendCommand.getBytes());
			UDPBuffer.flip();
			try{
				UDPChannel.send(UDPBuffer, UDPAdress);
			} catch (IOException e1) {
				System.out.println("send failed");
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			Runnable udpReceiver = new Runnable() {
				@Override
				public void run()
				{
					while(receiverFlag)
					{
						UDPBufferRec = ByteBuffer.allocate(8192);
						System.out.println("red trd");
						try {
							if (UDPChannel.receive(UDPBufferRec) != null) {
								BufferedReader br = new BufferedReader(new StringReader(new String(UDPBufferRec.array())));
								getSensorValues(sensorValues,br);
								System.out.println("red buff");
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			};
			new Thread(udpReceiver).start();
			while(doRun)
			{
				sendCommand = PiBotCommDefines.ctrl_do+"\n";
				
				actuatorValues = mind.doStep(sensorValues);

				System.out.println("do run");
				for(String a : sensorValues.keySet())
				{
					if(sensorDisplay.get(a)!=null)
						sensorDisplay.get(a).update(sensorValues.get(a));				
				}
				for(String a : mind.getVariablesNames())
					variableDisplay.get(a).update(mind.getVariable(a).getValue());
				for(String a : actuatorValues.keySet())
				{
					actuatorDisplay.get(a).update(actuatorValues.get(a));
					sendCommand+=a+PiBotCommDefines.commSeparator+actuatorValues.get(a)+"\n";				
				}
				sendCommand+=PiBotCommDefines.tag_endmsg+"\n";
				UDPBuffer = ByteBuffer.allocate(8192);
				UDPBuffer.put(sendCommand.getBytes());
				UDPBuffer.flip();
				try{
					UDPChannel.send(UDPBuffer, UDPAdress);
				} catch (IOException e1) {
					System.out.println("send failed");
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
			System.out.println("piBot demo ended");
			try {
				receiverFlag = false; 
				closeUDP();
				closeTCP();
			} catch (IOException e) {
				System.out.println("UDP close failed");
			}} catch (IOException e1) {
				System.out.println("UDP init failed");
				e1.printStackTrace();
		}
		receiverFlag = false; 
	}
	
	private void initUDP() throws IOException
	{	
		UDPChannel = DatagramChannel.open();
		DatagramSocket UDPSocket = UDPChannel.socket();
		
		UDPSocket.setReuseAddress(true);
		UDPSocket.bind(new InetSocketAddress (PiBotCommDefines.portUDP));
		// for non blocking
		//		  Selector selector = Selector.open();
		//		  serverChannel.configureBlocking (false);
		//		  serverChannel.register(selector, SelectionKey.OP_READ|SelectionKey.OP_WRITE);

		//UDPChannel.configureBlocking (true);
		//UDPChannel.connect(UDPAdress);
		Selector selector = Selector.open();
		UDPChannel.configureBlocking (false);
		UDPChannel.register(selector, SelectionKey.OP_READ|SelectionKey.OP_WRITE);
		UDPChannel.connect(UDPAdress);   
	}
	
	public void closeUDP() throws IOException {
		UDPBuffer =  ByteBuffer.allocate(8192);
		String msg = PiBotCommDefines.ctrl_do + "\n";
		for(String a : actuatorValues.keySet())
		{
				msg += a+PiBotCommDefines.commSeparator+0.5+"\n";				
		}
		msg+=PiBotCommDefines.tag_endmsg+"\n";
		UDPBuffer =  ByteBuffer.allocate(8192);
		UDPBuffer.put(msg.getBytes());
		UDPBuffer.flip();
		UDPChannel.send(UDPBuffer,UDPAdress);
		UDPBuffer =  ByteBuffer.allocate(8192);
		UDPBuffer.put((PiBotCommDefines.ctrl_stop+"\n").getBytes());
		UDPBuffer.flip();
		UDPChannel.send(UDPBuffer,UDPAdress);
		UDPChannel.close();
	}
	
	public void closeTCP() {
		clientOut.println(PiBotCommDefines.ctrl_do);
		
		for(String a : actuatorValues.keySet())
		{
				clientOut.println(a+PiBotCommDefines.commSeparator+0.5);				
		}
		clientOut.println(PiBotCommDefines.tag_endmsg);
		clientOut.flush();
		clientOut.println(PiBotCommDefines.ctrl_stop);
		clientOut.flush();
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private ConcurrentHashMap<String,Double> getSensorValues(ConcurrentHashMap<String, Double> sensorValues,BufferedReader br)
	{	
		try {
			String line = br.readLine();
			//System.out.println(line);
			if(line != null) {
			while ((line = br.readLine()) != null && !(line).equals(PiBotCommDefines.tag_endmsg)) {
				//System.out.println(line);
				sensorValues.put(line.split(PiBotCommDefines.commSeparator)[0],Double.parseDouble(line.split(PiBotCommDefines.commSeparator)[1]));
			}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sensorValues;
	}

	
	public void connect()
	{
		try {
			socket=new Socket();
			socket.connect(TCPAdress,10000);
			clientIn=new BufferedReader (new InputStreamReader (socket.getInputStream()));
			clientOut=new PrintWriter(socket.getOutputStream());
			System.out.println("Connected");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void makeUI() {
		JFrame frame = new JFrame("RemoteBot");
		frame.setLayout(new FlowLayout());	
		frame.setSize(800, 600);
		JButton stop = new JButton("STOP");
		stop.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {

				System.out.println("stopping pibot");
				doRun = false;
				frame.dispose();
			}
		});
		frame.add(stop,frame.getLayout());
		
		TitledBorder border = new TitledBorder("Sensors");
		border.setTitleJustification(TitledBorder.CENTER);
		border.setTitlePosition(TitledBorder.TOP);

		JPanel snsPan = new JPanel();
		snsPan.setLayout(new GridLayout(25,0));
		sensorDisplay = new HashMap<>();
		snsPan.setBorder(border);
		for(String s : mind.getSensorsNames())
		{
			TextDisplay t = new TextDisplay(s);
			sensorDisplay.put(s,t);
			snsPan.add(t,snsPan.getLayout());
		}
		frame.add(snsPan, frame.getLayout());
		
		border = new TitledBorder("Actuators");
		border.setTitleJustification(TitledBorder.CENTER);
		border.setTitlePosition(TitledBorder.TOP);

		JPanel actPan = new JPanel();
		actPan.setLayout(new BoxLayout(actPan, BoxLayout.PAGE_AXIS));
		actuatorDisplay = new HashMap<>();
		actPan.setBorder(border);
		for(String s : mind.getActuatorsNames())
		{
			TextDisplay t = new TextDisplay(s);
			actuatorDisplay.put(s,t);
			actPan.add(t,actPan.getLayout());
		}
		frame.add(actPan, frame.getLayout());
		
		border = new TitledBorder("Variables");
		border.setTitleJustification(TitledBorder.CENTER);
		border.setTitlePosition(TitledBorder.TOP);		
		
		JPanel varPan = new JPanel();
		varPan.setLayout(new BoxLayout(varPan, BoxLayout.PAGE_AXIS));
		varPan.setBorder(border);
		variableDisplay = new HashMap<>();
		for(String s : mind.getVariablesNames())
		{
			TextDisplay t = new TextDisplay(s);
			variableDisplay.put(s,t);
			varPan.add(t,varPan.getLayout());
		}
		frame.add(varPan, frame.getLayout());
		
		
		frame.setVisible(true);
	}

	@SuppressWarnings("serial")
	public class TextDisplay extends JPanel{
		String sensor = "na";
		JLabel val = new JLabel("0.0");
		
		public TextDisplay(String s) {
			sensor = s;
			this.add(new JLabel(s));
			this.add(val);
		}
		
		public void update(double v)
		{
			val.setText(String.format("%5.4f" , v));
		}
	}
}
