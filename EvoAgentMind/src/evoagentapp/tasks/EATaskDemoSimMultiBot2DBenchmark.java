/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D;

public class EATaskDemoSimMultiBot2DBenchmark extends EATaskDemoSimMultiBot {
	int stepcounter = 0;
	int repcounter = 0;
	protected int tickLimit = 0;
	protected int evaluationRepetition = 1;
	
	
	public EATaskDemoSimMultiBot2DBenchmark(String root, ArrayList<String> botmodel, ArrayList<String> botname, ArrayList<String> masterS,ArrayList<String> driveN,String envirName, int tickLim, int evalRep)
	{
		super(root,botmodel,botname,masterS,driveN,envirName);
		envClasspath = EvoAgentAppDefines.classpath2DMultiExp;
		tickLimit = tickLim;
		evaluationRepetition = evalRep;
		System.out.println(botModels.size() + " " + botNames.size() + " " + masterSkills.size());
		for(int i = 0; i < botModels.size(); i++)
		{
			mindTemplates.add(EvoAgentMindFactory.loadMindTemplate(root, botNames.get(i), masterSkills.get(i), mindDriveClassNames.get(i)));
		}
	}

	protected void viewerStep() {
	}

	protected void initViewer() {		
	}

	protected boolean checkContinue() {
		if(stepcounter >= tickLimit)
		{
			stepcounter = 0;					
			return false;
		}
		if(interruptFlag.interrupted())
		{
			stepcounter = 0;					
			return false;
		}
		stepcounter++;
		return true;
	}
	
	protected boolean checkRepetitionContinue() {
		if(repcounter >= evaluationRepetition)					
			return false;
		repcounter++;
		return true;	
	}
	
	protected void initEnvironement()
	{
		environment.setBotModel(botModels);
		environment.init();
		ArrayList<ArrayList<evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D.ObstaclePos>> obstaclesData = new ArrayList<>();
		if(environment.getHasObstacles())
		{
			((SimulationEnvironment2D)environment).generateAllObstaclesParameters(obstaclesData, 1);
			((SimulationEnvironment2D)environment).loadObstacles(obstaclesData.get(0));
		}
	}
}
