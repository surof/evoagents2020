/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;
import org.encog.ml.CalculateScore;

import evoagentmindelements.EvoAgentMindFactory;
import evoagentsimulation.simulationlearning.SimScoreMultiAgents;

public class EATaskLearnSimMultiBot2D extends EATaskLearnSimMultiBot {

	protected ArrayList<ArrayList<evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D.ObstaclePos>> od = new ArrayList<>();
	
	public EATaskLearnSimMultiBot2D(String root, ArrayList<String> botmodel, ArrayList<String> botname, ArrayList<String> masterS,ArrayList<String> driveN,
			String learnS,  String learnMeth ,String envirName, int gen, int tickLim, int evalRep, int saveGen,boolean saveAuto, int sizeGen)
	{
		super(root,botmodel,botname,masterS,driveN,learnS,learnMeth,envirName,gen, tickLim,evalRep, saveGen,saveAuto,sizeGen);
		for(int i = 0; i < botModels.size(); i++)
		{
			mindTemplates.add(EvoAgentMindFactory.loadMindTemplate(MindRootFolder, botNames.get(i), masterSkills.get(i), mindDriveClassNames.get(i)));
		}
	}
	
	protected CalculateScore makeCalculateScore() //override me
	{
		return  new SimScoreMultiAgents(botModels, od, evaluationRepetition, tickLimit, environmentName, learningSkill, mindTemplates);
	}
}
