/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;

import evoagentmindelements.EvoAgentMindTemplate;

public class EATaskLearnSimMultiBot extends EATaskLearningSim{

	protected ArrayList<String> botModels = new ArrayList<String>();
	protected ArrayList<String> botNames = new ArrayList<String>();
	protected ArrayList<String> masterSkills = new ArrayList<String>();
	protected ArrayList<String> mindDriveClassNames = new ArrayList<String>();
	protected ArrayList<EvoAgentMindTemplate> mindTemplates = new ArrayList<EvoAgentMindTemplate>();

	public EATaskLearnSimMultiBot(String root, ArrayList<String> botmodel, ArrayList<String> botname, 
			ArrayList<String> masterS, ArrayList<String> mindDriveClassN, String learnS,  String learnMeth ,String envirName, int gen, int tickLim, int evalRep, int saveGen,boolean saveAuto, int sizeGen)
	{
		super(root,learnS,learnMeth,envirName,gen, tickLim,evalRep, saveGen,saveAuto, sizeGen);
		botModels = botmodel;
		botNames = botname;
		masterSkills = masterS;
		mindDriveClassNames = mindDriveClassN;
		learningAgentFolder = MindRootFolder + "/"+ botNames.get(0);
		learningAgentSkillFolder = MindRootFolder +"/"+botNames.get(0)+"/Skills";		
		learningAgentLearningSkillFolder = MindRootFolder +"/"+botNames.get(0)+"/Skills/"+learningSkill;		
	}
	
	
	public void setLearningMindTemplate()
	{
		learningMindTemplate = mindTemplates.get(0);
	}
	
}
