/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindTemplate;

public class EATask {
	protected String MindRootFolder = "";
	protected String mindDriveClassName = null;
	protected float throttleDelay = 4;
	protected boolean endTask = false;

	public EATask(String rootFolder)
	{
		MindRootFolder = rootFolder;	
	}
	
	public void runTask() {
		
		
	}
	
	public static EvoAgentMindTemplate getMindTemplate()
	{
		return null;
	}
	
	public  EvoAgentMind getMind()
	{
		return null;
	}

	public  ArrayList<ArrayList<EvoAgentMind>> getMinds()
	{
		return null;
	}
	
	public void setTrottleRender(float delay)
	{
		throttleDelay = delay;
	}

	public float getTrottleRender() {
		return throttleDelay;
	}
	
	public void manualReset()
	{
		
	}
	
	public void stopTask()
	{
		endTask = true;
	}
}
