/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindTemplate;

public class EATaskDemoSimSingleBot extends EATaskDemoSim{
	
	protected String AgentRootFolder = "";
	protected String AgentSkillFolder = "";
	protected String botName = "default";
	protected String botModel = "default";
	protected String masterSkill = "";
		
	protected static EvoAgentMindTemplate mindTemplate = null;
	protected EvoAgentMind mind;
	
	public EATaskDemoSimSingleBot(String root, String botmodel, String botname, String masterskill,String driveName,String envirName)
	{
		super(root,envirName);
		botName = botname;
		botModel = botmodel;
		AgentRootFolder = MindRootFolder + "/"+ botName;
		AgentSkillFolder = MindRootFolder +"/"+botName+"/"+ "/Skills";		
		masterSkill = masterskill;
		mindDriveClassName = driveName;
	}
	
	
	public EvoAgentMind getMind()
	{
		return mind;
	}
}
