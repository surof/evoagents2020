/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;

import evoagentapp.EvoAgentAppDefines;

public class EvoAgentTaskFactory {

	private static EvoAgentTaskFactory.EvoAgentTaskTemplate currentTemplate;
	
	public static void makeNewTemplate()
	{
		currentTemplate = new EvoAgentTaskFactory.EvoAgentTaskTemplate();
	}
	
	public static void parseTemplateString(String in)
	{
		int i = 1, size;
		switch(in.split(EvoAgentAppDefines.separator)[0]) {
		case EvoAgentAppDefines.taskTypeToken:
			currentTemplate.task = in.split(EvoAgentAppDefines.separator)[1];
			break;
		case EvoAgentAppDefines.taskOptionsToken:
			currentTemplate.options  = in.split(EvoAgentAppDefines.separator)[1];
			break;
		case EvoAgentAppDefines.botModelToken:
			size = in.split(EvoAgentAppDefines.separator).length;
			while (i < size)
			{
				currentTemplate.botMod.add(in.split(EvoAgentAppDefines.separator)[i]);				
				i++;
			}
			break;
		case EvoAgentAppDefines.botNameToken:
			size = in.split(EvoAgentAppDefines.separator).length;
			while (i < size)
			{
				currentTemplate.botNam.add(in.split(EvoAgentAppDefines.separator)[i]);				
				i++;
			}
			break;
		case EvoAgentAppDefines.masterSkillToken:
			size = in.split(EvoAgentAppDefines.separator).length;
			while (i < size)
			{
				currentTemplate.masterS.add(in.split(EvoAgentAppDefines.separator)[i]);				
				i++;
			}
			break;
		case EvoAgentAppDefines.driveSkillToken:
			size = in.split(EvoAgentAppDefines.separator).length;
			while (i < size)
			{
				currentTemplate.driveNames.add(in.split(EvoAgentAppDefines.separator)[i]);				
				i++;
			}
			break;
		case EvoAgentAppDefines.learningSkillToken:
			currentTemplate.learnS = in.split(EvoAgentAppDefines.separator)[1];
			break;
		case EvoAgentAppDefines.learningMethodToken:
			currentTemplate.learnMeth = in.split(EvoAgentAppDefines.separator)[1];
			break;
		case EvoAgentAppDefines.simEnvToken:
			currentTemplate.Env = in.split(EvoAgentAppDefines.separator)[1];
			break;
		case EvoAgentAppDefines.taskRootFolderToken:
			currentTemplate.rootFolder = in.split(EvoAgentAppDefines.separator)[1];
			break;
		case EvoAgentAppDefines.generationCountLearningToken:
			currentTemplate.nGenerations = Integer.parseInt(in.split(EvoAgentAppDefines.separator)[1]);
			break;
		case EvoAgentAppDefines.generationSizeLearningToken:
			currentTemplate.sizeGenerations = Integer.parseInt(in.split(EvoAgentAppDefines.separator)[1]);
			break;
		case EvoAgentAppDefines.evalRepetitionCountLearningToken:
			currentTemplate.evalRepetitons = Integer.parseInt(in.split(EvoAgentAppDefines.separator)[1]);
			break;
		case EvoAgentAppDefines.evalTickLimitLearningToken:
			currentTemplate.tickLimit = Integer.parseInt(in.split(EvoAgentAppDefines.separator)[1]);
			break;
		case EvoAgentAppDefines.saveGenerationIntervalLearningToken:
			if(in.split(EvoAgentAppDefines.separator)[1].equals(EvoAgentAppDefines.autoToken))
				currentTemplate.saveGenerationAuto = true;
			else
				currentTemplate.saveGenerationInterval = Integer.parseInt(in.split(EvoAgentAppDefines.separator)[1]);
			break;
		default : 
			System.out.println("unknown task token : " +in.split(EvoAgentAppDefines.separator)[0]);
			break;}
	}
	
	public static void finalizeTemplate() {
		while(currentTemplate.driveNames.size() < currentTemplate.masterS.size())
			currentTemplate.driveNames.add(null);
	}
	
	public static EATask makeTask()
	{
		EATask ret = null;
		System.out.println("Making task : " + currentTemplate.task);
		switch(currentTemplate.task) {
		case EvoAgentAppDefines.learn2dSimSingleTaskToken:
		{
			ret = new EATaskLearnSimSingleBot2D(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
						currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0), currentTemplate.learnS,currentTemplate.learnMeth, currentTemplate.Env,
						currentTemplate.nGenerations, currentTemplate.tickLimit, currentTemplate.evalRepetitons, currentTemplate.saveGenerationInterval, currentTemplate.saveGenerationAuto,currentTemplate.sizeGenerations);
			break;
		}
		case EvoAgentAppDefines.transfer2dSimSingleTaskToken:
		{
			ret = new EATSingleSim2dTransfer(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
						currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0), currentTemplate.learnS, currentTemplate.Env);
			break;
		}
		case EvoAgentAppDefines.learn2dSimMultiTaskToken:
		{
			ret = new EATaskLearnSimMultiBot2D(currentTemplate.rootFolder, currentTemplate.botMod, currentTemplate.botNam,
						currentTemplate.masterS, currentTemplate.driveNames, currentTemplate.learnS,currentTemplate.learnMeth, currentTemplate.Env,
						currentTemplate.nGenerations, currentTemplate.tickLimit, currentTemplate.evalRepetitons, currentTemplate.saveGenerationInterval, currentTemplate.saveGenerationAuto,currentTemplate.sizeGenerations);
			break;
		}
		case EvoAgentAppDefines.learn2dSimContinuousSingleTaskToken:
		{
			ret = new EATaskLearnSimSingleBot2DContinuous(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
						currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0),currentTemplate.learnMeth, currentTemplate.Env,
						currentTemplate.nGenerations, currentTemplate.tickLimit, currentTemplate.evalRepetitons, currentTemplate.saveGenerationInterval, currentTemplate.saveGenerationAuto,currentTemplate.sizeGenerations);
			break;
		}
		case EvoAgentAppDefines.learn3dSimContinuousSingleTaskToken:
		{
			ret = new EATaskLearnSimSingleBot3DContinuous(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0),currentTemplate.learnMeth, currentTemplate.Env,
					currentTemplate.nGenerations, currentTemplate.tickLimit, currentTemplate.evalRepetitons, currentTemplate.saveGenerationInterval, currentTemplate.saveGenerationAuto,currentTemplate.sizeGenerations);
			break;
		}
		case EvoAgentAppDefines.demo2dSimSingleTaskToken:
		{
			ret = new EATaskDemoSimSingleBot2D(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0),currentTemplate.Env);
			break;
		}
		case EvoAgentAppDefines.demo2dSimMultiTaskToken:
		{
			ret = new EATaskDemoSimMultiBot2D(currentTemplate.rootFolder, currentTemplate.botMod, currentTemplate.botNam,
					currentTemplate.masterS, currentTemplate.driveNames,currentTemplate.Env);
			break;
		}
		case EvoAgentAppDefines.demoRemoteBotTaskToken:
		{
			ret = new EATaskDemoRemoteBot(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0));
			break;
		}
		case EvoAgentAppDefines.demoRemoteSimTaskToken:
		{
			ret = new EATaskDemoRemoteSim(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0));
			break;
		}
		case EvoAgentAppDefines.learnRemoteSimTaskToken:
		{
			ret = new EATaskLearnRemoteBot(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0), currentTemplate.learnS,currentTemplate.learnMeth, currentTemplate.nGenerations, currentTemplate.tickLimit, currentTemplate.evalRepetitons,currentTemplate.sizeGenerations);
			break;
		}
		case EvoAgentAppDefines.demoOnboardBotTaskToken:
		{
			ret = new EATaskDemoOnboardBot(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0), currentTemplate.options);
			break;
		}
		case EvoAgentAppDefines.demo3dSimSingleTaskToken:
		{
			ret = new EATaskDemoSimSingleBot3D(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0), currentTemplate.Env);
			break;
		}
		case EvoAgentAppDefines.learn3dSimSingleTaskToken:
		{
			ret = new EATaskLearnSimSingleBot3D(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
					currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0), currentTemplate.learnS,currentTemplate.learnMeth, currentTemplate.Env,
					currentTemplate.nGenerations, currentTemplate.tickLimit, currentTemplate.evalRepetitons, currentTemplate.saveGenerationInterval, currentTemplate.saveGenerationAuto,currentTemplate.sizeGenerations);
			break;
		}
		case EvoAgentAppDefines.benchmark2dSimMultiTaskToken:
		{
			ret = new EATaskDemoSimMultiBot2DBenchmark(currentTemplate.rootFolder, currentTemplate.botMod, currentTemplate.botNam,
						currentTemplate.masterS, currentTemplate.driveNames, currentTemplate.Env, currentTemplate.tickLimit, currentTemplate.evalRepetitons);
			break;
		}
		case EvoAgentAppDefines.benchmark2dSimSingleTaskToken:
		{
			ret = new EATaskDemoSimSingleBot2DBenchmark(currentTemplate.rootFolder, currentTemplate.botMod.get(0), currentTemplate.botNam.get(0),
						currentTemplate.masterS.get(0), currentTemplate.driveNames.get(0), currentTemplate.Env, currentTemplate.tickLimit, currentTemplate.evalRepetitons);
			break;
		}
		case EvoAgentAppDefines.openEndedTaskToken:
		{
			System.out.println("not quite there yet ...");
			break;
		}
		default : 
			System.out.println("invalid task token : " + currentTemplate.task +" (typo perhaps ?)");
			break;}
		return ret;
	}
	
	static class EvoAgentTaskTemplate{
		String options = "";
		int sizeGenerations = 200;
		String task = "";
		String learnS = "";
		String learnMeth = "";
		String Env = "";
		int tickLimit = 1;
		int evalRepetitons = 1;
		int nGenerations = 1;
		String rootFolder = "";
		int saveGenerationInterval = 0;
		boolean saveGenerationAuto = false;
		
		ArrayList<String> botMod = new ArrayList<String>();
		ArrayList<String> botNam = new ArrayList<String>();
		ArrayList<String> masterS = new ArrayList<String>();
		ArrayList<String> driveNames = new ArrayList<String>();
	}
}
