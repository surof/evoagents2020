/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;
import org.encog.Encog;

import evoagentmindelements.EvoAgentMindFactory;
import evoagentmindelements.modules.NeuralNetworkModule;


public class EATaskLearnSimSingleBot3DContinuous extends EATaskLearnSimSingleBot3D {

	public EATaskLearnSimSingleBot3DContinuous(String root, String botmodel, String botname, String masterS, String driveN,
			String learnMeth ,String envirName, int gen, int tickLim, int evalRep, int saveGen,boolean saveAuto, int sizeGen)
	{
		super( root,  botmodel,  botname,  masterS, driveN,"", learnMeth , envirName,  gen,  tickLim,  evalRep, saveGen, saveAuto,  sizeGen);

	}
	
	public void runTask() 
	{
		ArrayList<String> skillNames = learningMindTemplate.getNeuralSkillNames();
		int size = skillNames.size();
		int pos = 0;
		while(!endTask)
		{
			learningSkill = skillNames.get(pos);
			System.out.println("Doing : " + learningSkill);
			pos = (pos + 1) % size;
			learnIteration();
			((NeuralNetworkModule) learningMindTemplate.getSkillByName(learningSkill)).setNetwork(EvoAgentMindFactory.loadNNFromFile(learningAgentSkillFolder,learningSkill));
		}
		Encog.getInstance().shutdown();
	}
}
