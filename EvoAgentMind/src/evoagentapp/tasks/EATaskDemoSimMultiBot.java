/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;
import evoagentmindelements.EvoAgentMindTemplate;

public class EATaskDemoSimMultiBot extends EATaskDemoSim{

	protected ArrayList<String> botModels = new ArrayList<String>();
	protected ArrayList<String> botNames = new ArrayList<String>();
	protected ArrayList<String> masterSkills = new ArrayList<String>();
	protected ArrayList<String> mindDriveClassNames = new ArrayList<String>();

	protected ArrayList<EvoAgentMindTemplate> mindTemplates = new ArrayList<EvoAgentMindTemplate>();

	
	public EATaskDemoSimMultiBot(String root, ArrayList<String> botmodel, ArrayList<String> botname, ArrayList<String> masterS, ArrayList<String> mindDriveClassN, String envirName)
	{
		super(root,envirName);
		botModels = botmodel;
		botNames = botname;
		masterSkills = masterS;
		mindDriveClassNames = mindDriveClassN;
	}
}
