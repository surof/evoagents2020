/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.opencv.core.Core;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMind;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentmindelements.EvoAgentMindTemplate;
import onboard.bot.PhysicalBot;

public class EATaskDemoOnboardBot extends EATask {

	static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
	protected String endMessage ="Demo ended";
	
	protected String AgentRootFolder = "";
	protected String AgentSkillFolder = "";
	protected String botName = "default";
	protected String botModel = "default";
	protected String masterSkill = "";
	protected String options = "";
	
	protected static EvoAgentMindTemplate mindTemplate = null;
	protected EvoAgentMind mind = null;
	
	protected PhysicalBot bot = null;
	boolean diagnostic = false;
	boolean idle = false;
	
	public EATaskDemoOnboardBot(String root, String botmodel, String botname, String masterskill, String driveName, String opts)
	{
		super(root);
		botName = botname;
		botModel = botmodel;
		options = opts.toLowerCase();
		readOptions();
		AgentRootFolder = MindRootFolder + "/"+ botName;
		AgentSkillFolder = MindRootFolder +"/"+botName+"/"+ "/Skills";		
		masterSkill = masterskill;
		mindDriveClassName = driveName;
		if(!idle)
		{
			mindTemplate = EvoAgentMindFactory.loadMindTemplate(MindRootFolder,botname, masterSkill, mindDriveClassName);
			mind = EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
		}
	}

   private void readOptions() {
		if(options.contains("idle"))
		{
			idle = true;
			diagnostic = true;			
		}
		else if (options.contains("diagnostic"))	
		{
			diagnostic = true;
		}
	}

   protected void makeBot()
	{
		Class<?> clazz;
		try {
			clazz = Class.forName("onboard.bot."+botModel);
			Constructor<?> constructor = clazz.getConstructor();
			bot = (PhysicalBot) constructor.newInstance();
			bot.readBotConfigFile(AgentRootFolder+"/"+botName+EvoAgentAppDefines.botdescExtension);
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			System.out.println("Physical bot type not found :" + botModel );System.out.println(e.getCause());System.out.println(e.getMessage());
			bot = null;
		}
	}

	
	public void runTask() 
	{
		initTask();
		if(!idle)
		{
			while(checkRepetitionContinue()&& !endTask)
			{
				preRepetition();
				while(checkContinue())
				{
					bot.updateSensors();
					mind.doStep();
					bot.updateActuators();
					if(diagnostic)
						bot.printAllSensorValues();
				}
				postRepetition();
			}
			postTask();
			if(endMessage!=null)
				System.out.println(endMessage);			
		}
		else
		{
			while(!endTask)
			{
				
				System.out.println("Diganostic mode");
				bot.updateSensors();
				bot.printAllSensorValues();
				bot.testCommand("halt");
				bot.updateActuators();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	protected void initTask() {
		makeBot();
		bot.setMind(mind);
	}
	
	protected void preRepetition() {
		// TODO Auto-generated method stub
		
	}

	protected void postRepetition() {
		// TODO Auto-generated method stub
		
	}

	protected void postTask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void manualReset()
	{
		
	}

	protected void viewerStep() {
		// TODO Auto-generated method stub
		
	}

	protected void initViewer() {
		// TODO Auto-generated method stub
	}

	protected boolean checkContinue() {
		return true;
	}
	
	protected boolean checkRepetitionContinue() {
		return true;
	}
}
