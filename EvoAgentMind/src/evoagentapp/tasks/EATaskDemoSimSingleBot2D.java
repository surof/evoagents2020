/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp.tasks;

import java.util.ArrayList;

import javax.swing.JFrame;

import evoagentapp.EvoAgentAppDefines;
import evoagentmindelements.EvoAgentMindFactory;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D;
import evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2DSingleBot;
import ui.SimEnv2DViewer;

public class EATaskDemoSimSingleBot2D extends EATaskDemoSimSingleBot {

	SimEnv2DViewer simPanel;
	JFrame frame;
	
	public EATaskDemoSimSingleBot2D(String root, String botmodel, String botname, String masterS, String driveN,String envirName)
	{
		super(root,botmodel, botname, masterS, driveN,envirName);
		envClasspath = EvoAgentAppDefines.classpath2DSingleExp;
		mindTemplate = EvoAgentMindFactory.loadMindTemplate(MindRootFolder,botname, masterSkill, mindDriveClassName);
		mind = EvoAgentMindFactory.makeMindFromTemplate(mindTemplate);
	}

	protected void viewerStep() {
		simPanel.repaint();
	}

	protected void initViewer() {		
		simPanel = new SimEnv2DViewer((SimulationEnvironment2D) environment, scoreCounter); 
		frame = new JFrame(environmentName);
		frame.add(simPanel);
		frame.setSize(800, 600);
		frame.setVisible(true);
	}

	protected void closeViewer() {
	    frame.setVisible(false);
	    frame.dispose();
	    frame = null;
	}

	protected boolean checkContinue() {
		return frame.isVisible();
	}
	
	protected boolean checkRepetitionContinue() {
		return frame.isVisible();
	}
	
	protected void initEnvironement()
	{
		environment.setBotModel(botModel);
		((SimulationEnvironment2DSingleBot)environment).setBotMind(mind);
		environment.init();
		ArrayList<ArrayList<evoagentsimulation.evoagent2dsimulator.SimulationEnvironment2D.ObstaclePos>> obstaclesData = new ArrayList<>();
		if(environment.getHasObstacles())
		{
			((SimulationEnvironment2D)environment).generateAllObstaclesParameters(obstaclesData, 1);
			((SimulationEnvironment2D)environment).loadObstacles(obstaclesData.get(0));
		}
	}
}
