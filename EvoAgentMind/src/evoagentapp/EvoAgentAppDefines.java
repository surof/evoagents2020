/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp;


public class EvoAgentAppDefines {
	public static int port = 28001; 

	//folders
	public static String botSavePath = "botBrains";
	public static String rootPath = "";

	public static final String skillFolder = "Skills";
	public static final String taskFolder = "Tasks";
	
	//packages
	public static final String classpath2DMultiExp = "evoagentsimulation.evoagent2dsimulator.multiagentexperiments.";
	public static final String classpath2DSingleExp = "evoagentsimulation.evoagent2dsimulator.experiments.";
	public static final String classpath3D = "evoagentsimulation.evoagent3dsimulator.experiments.";
	
	//file extensions
	public static final String skillDescExtention = ".ades";
	public static final String botdescExtension = ".botdesc";
	public static final String vardescExtension = ".vardesc";
	public static final String neuralNetworkFileExtension = ".nnext";
	public static final String neuralNetworkFileExtensionNoDot = "nnext";
	public static final String genProgramFileExtension = ".prg";	
	public static final String genProgramFileExtensionNoDot = "prg";	
	public static final String trainingPersistenceFileExtension = ".tp";
	public static final String taskFileExtension = ".tsk";
	
	//skill description file
	public static final String skillTypeNeural = "neural";
	public static final String skillTypeHardCoded = "hardCoded";
	public static final String skillTypeGProgram = "geneticProgram";
	public static final String sensorPrefix = "SE";
	public static final String skillPrefix = "SK";
	public static final String variablePrefix = "VA";
	public static final String variableDerivativePrefix = "VD";
	public static final String sensorDerivativePrefix = "SD";
	public static final String sensorHistoryPrefix = "SH";
	public static final String variableHistoryPrefix = "VH";
	public static final String motorPrefix = "MO";
	public static final String inputToken = "input";
	public static final String outputToken = "output";
	public static final String layersToken = "layers";
	public static final String classToken = "class";
	public static final String autoToken = "auto";
	public static final String separator = ":";

	//task description file
	public static final String taskTypeToken = "TYPE";
	public static final String taskRootFolderToken = "ROOTFOLDER";
	public static final String taskOptionsToken = "OPTIONS";
	public static final String botModelToken = "BOTMODEL";
	public static final String botNameToken = "BOTNAME";
	public static final String masterSkillToken = "MASTERSKILL";
	public static final String driveSkillToken = "DRIVEMODULE";
	public static final String learningSkillToken = "LEARNINGSKILL";
	public static final String learningMethodToken = "LEARNINGMETHOD";
	public static final String saveGenerationIntervalLearningToken = "SAVEGENINTER";
	
	//sim
	public static final String simEnvToken = "ENV";
	
	//learning methods
	public static final String geneticLearningMethod = "GENETIC";
	public static final String geneticLearningMethodElman = "GENETICELMAN";
	public static final String geneticLearningMethodRNN = "GENETICRNN";;
	public static final String annealingLearningMethod = "ANNEALING";
	public static final String neatLearningMethod = "NEAT";
	public static final String neatoLearningMethod = "NEATO";
	public static final String geneticProgrammingLearningMethod = "GENPROG";
	
	//learning params
	public static final String generationCountLearningToken = "NUMGEN";
	public static final String generationSizeLearningToken = "SIZEGEN";
	public static final String evalRepetitionCountLearningToken = "EVALREP";
	public static final String evalTickLimitLearningToken = "TICKLIM";

	
	
	//task types
	public static final String benchmark2dSimMultiTaskToken = "BENCHMARK2DSIMMULTI";
	public static final String benchmark2dSimSingleTaskToken = "BENCHMARK2DSIMSINGLE";
	public static final String demo3dSimSingleTaskToken = "DEMO3DSIM";
	public static final String demo2dSimSingleTaskToken = "DEMO2DSIM";
	public static final String demo2dSimMultiTaskToken = "DEMO2DSIMMULTI";
	public static final String learn3dSimSingleTaskToken = "LEARN3DSIM";
	public static final String learn2dSimSingleTaskToken = "LEARN2DSIM";
	public static final String transfer2dSimSingleTaskToken = "TRANSFER2DSIM";
	public static final String learn2dSimMultiTaskToken = "LEARN2DSIMMULTI";
	public static final String learn2dSimContinuousSingleTaskToken = "LEARN2DSIMCONTINUOUS";
	public static final String learn3dSimContinuousSingleTaskToken = "LEARN3DSIMCONTINUOUS";
	public static final String demoRemoteBotTaskToken = "DEMOREMOTE";
	public static final String demoRemoteSimTaskToken = "DEMOREMOTESIM";
	public static final String learnRemoteSimTaskToken = "LEARNREMOTESIM";
	public static final String openEndedTaskToken = "OPENENDED";	
	
	public static final String demoOnboardBotTaskToken = "DEMOONBOARDBOT";
	
	
	//values
	public static final double doubleBooleanFalse = 0.2;	
	public static final double doubleBooleanTrue = 0.8;	
	
	//Physics setting
	
	// calculation per sec (or step length): 60 = fine , 30 = cheaper
	public static float targetFPS2DPhysics = 30.0f;


	
	
	
}
