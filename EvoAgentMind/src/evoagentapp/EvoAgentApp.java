/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package evoagentapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


import org.encog.persist.PersistorRegistry;

//import Utils.SmartScroller;
import evoagentapp.tasks.EATask;
import evoagentapp.tasks.EvoAgentTaskFactory;
import evoagentsimulation.simulationlearning.EncogCNN.PersistCNN;
import evoagentsimulation.simulationlearning.MyNEAT.PersistNEATOPopulation;
import ui.EvoAgentAppFrame;

public class EvoAgentApp{
	static public boolean stop = false;
	static boolean endTask = false;
	static boolean makeGui = true;
	static ArrayList<String> tasks = new ArrayList<>();
	static public EATask currentTask = null;
	static public EvoAgentAppFrame frame = null;
	
	
	private static void createAndShowGUI() {
		frame = new EvoAgentAppFrame();
    }
	
	public static void main(String[] args) {

		LocalDateTime startTime; 
		LocalDateTime stopTime; 
		String s;
		for(int i = 0 ; i < args.length ; i++)
		{
			s = args[i];
			if(s.charAt(0) == '-')
			{
				if(s.equals("-nog"))
					makeGui = false;
			}
			else
			{
				tasks.add(s);
			}
		}
		if(makeGui)
			createAndShowGUI();

		//NEAT-O compat
		PersistorRegistry.getInstance().add(new PersistNEATOPopulation());
		//CNN compat
		PersistorRegistry.getInstance().add(new PersistCNN());
		
		for(int i = 0 ; i < tasks.size() ; i++)
		{
			startTime = LocalDateTime.now();
			
			currentTask = makeTaskFromFile(tasks.get(i));
			if(currentTask != null)
				currentTask.runTask();
			else
				System.out.println("couldn't run task : " + tasks.get(i));

			stopTime = LocalDateTime.now();
			System.out.println("============= TOTAL RUNTIME ===================");
			System.out.println("started at :" + startTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + 
					" stopped at :" + stopTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + 
					" runtime :" + String.format("%d:%02d:%02d", Duration.between(startTime, stopTime).getSeconds() / 3600, (Duration.between(startTime, stopTime).getSeconds() % 3600) / 60, (Duration.between(startTime, stopTime).getSeconds() % 60)));
			currentTask = null;
		}

		long timePrev = 0;
		float Delay = 500;
		while(!stop&& makeGui)
		{
			System.out.println("Drag and drop task file (.simbatch)");
			while(!endTask && makeGui)
			{
				if(System.currentTimeMillis()-timePrev>Delay && currentTask !=null)
				{	
					currentTask.runTask();
					timePrev = System.currentTimeMillis();			
				}
				else
				{
					try {
						Thread.sleep(5);
					} catch (InterruptedException e1) {
						
						e1.printStackTrace();
					}					
				}				
			}
			currentTask = null;
			endTask = false;
		}
	}

	public static EATask makeTaskFromFile(String taskFile) {
		File  f = new File(taskFile);
		System.out.println("task description file : " + taskFile);
		if(f.exists())
		{
			InputStream ips = null;
			try {
				ips = new FileInputStream(f);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.out.println("file not found : " + taskFile);
				return null;
			} 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			EvoAgentTaskFactory.makeNewTemplate();
			try {
				while((line = br.readLine()) != null )
					EvoAgentTaskFactory.parseTemplateString(line);
				br.close();
			}catch (Exception e) {
			}
			EvoAgentTaskFactory.finalizeTemplate();
			return EvoAgentTaskFactory.makeTask();
		}
		else
			System.out.println("not found !");
		return null;
	}
	
	public static void endTask()
	{
		if(currentTask != null)
			currentTask.stopTask();
		endTask = true;
	}
	
	public static EATask getCurrentTask()
	{
		return currentTask;
	}
}


