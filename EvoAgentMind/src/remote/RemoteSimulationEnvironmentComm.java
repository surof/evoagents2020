/*******************************************************************************
 * EvoAgents : A simulation platform for agents using the MIND architecture
 * Copyright (c) 2016, 2020 Suro François (suro@lirmm.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package remote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;

import evoagentsimulation.simulationlearning.ScoreCounter;
import evoagentsimulation.simulationlearning.SimulationInterruptFlag;

//to be fixed
@Deprecated
public class RemoteSimulationEnvironmentComm {
	private ThreadLocal<Socket> socket = new ThreadLocal<Socket>();
	BufferedReader clientIn;
	PrintWriter clientOut;
	InetSocketAddress TCPAdress = new InetSocketAddress("127.0.0.1", RemoteCommDefines.defaultPort);
	
	String sendCommand;
	HashMap<String,Double> sensorValues = new HashMap<>();
	
	public void init() {
		try {
			socket.set(new Socket());
			socket.get().connect(TCPAdress,10000);
			clientIn=new BufferedReader (new InputStreamReader (socket.get().getInputStream()));
			clientOut=new PrintWriter(socket.get().getOutputStream());
			//System.out.println("Connected");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void reset() {
		sendCommand = RemoteCommDefines.c_start;		
		clientOut.println(sendCommand);
		clientOut.flush();
	}

	public HashMap<String, Double> doStep(HashMap<String, Double> actuatorValues, ScoreCounter scoreCounter,SimulationInterruptFlag simulationInterruptFlag) {
		//sendCommand = RemoteCommDefines.c_do;	
		clientOut.println(RemoteCommDefines.c_do);
		//System.out.println("step lol");
		if(actuatorValues != null)
		{
//			System.out.println("step" + actuatorValues.size());
			for(String a : actuatorValues.keySet())
				clientOut.println(a+RemoteCommDefines.commSeparator+actuatorValues.get(a));				
			clientOut.println(RemoteCommDefines.m_end);
			clientOut.flush();				
		}
//	System.out.println("step done");
		return getMessage(sensorValues,clientIn,scoreCounter,simulationInterruptFlag);
	}

	public void quit() {
		clientOut.println(RemoteCommDefines.c_stop);
		clientOut.flush();
		try {
			socket.get().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private HashMap<String,Double> getMessage(HashMap<String, Double> sensorValues,BufferedReader br, ScoreCounter scoreCounter,SimulationInterruptFlag simulationInterruptFlag)
	{	
		try {
			String line = br.readLine();
			//System.out.println(line);
			if(line != null) {
			while ((line = br.readLine()) != null && !(line).equals(RemoteCommDefines.m_end)&& !(line).equals(RemoteCommDefines.t_feedback)) {
				//System.out.println(line);
				sensorValues.put(line.split(RemoteCommDefines.commSeparator)[0],Double.parseDouble(line.split(RemoteCommDefines.commSeparator)[1]));
			}
			if((line).equals(RemoteCommDefines.t_feedback))
				//System.out.println(line);
				scoreCounter.addToScore(Double.parseDouble(br.readLine()));
				//System.out.println(scoreCounter.getCurrentScore());
			}
			while ((line = br.readLine()) != null && !(line).equals(RemoteCommDefines.m_end))
			{
				//System.out.println(line);
				line = br.readLine();

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sensorValues;
	}
}
