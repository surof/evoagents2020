EvoAgents : A simulation platform for agents using the MIND architecture
Copyright (c) 2016, 2020 Suro François (surof@univ-grenoble-alpes.fr)

EvoAgents is a custom framework written in Java designed to carry on 
experiments with agents using the MIND hierarchy as their control system.

EvoAgents_Tutorial.pdf contains extremely outdated informations, 
for your convenience
